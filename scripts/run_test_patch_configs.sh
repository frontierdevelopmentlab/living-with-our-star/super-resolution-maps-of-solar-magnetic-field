#!/bin/bash
_files="/home/gitiaux/super-resolution-maps-of-solar-magnetic-field/configs/test_patch/*.yml"

for f in $_files
do
	echo $f
	python train.py --config_path $f
done
