#!/bin/bash

_files="$@"

for f in $_files
do
	echo $f
	python train.py --config_path $f --upload_results --augment
done
#sudo shutdown -h now
