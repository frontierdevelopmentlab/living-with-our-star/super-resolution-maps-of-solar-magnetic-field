import argparse
import datetime
import os
import shutil
import subprocess
import json
import copy
from time import perf_counter

import numpy as np
import pandas as pd
import torch
import yaml

import torch.nn as nn
from google.cloud import storage
from tensorboardX.writer import SummaryWriter
from torch.utils.data.dataloader import DataLoader

from source.pipeline.dataset import GCSMagDataset, LocalMagDataset
from source.pipeline.model_manager import BaseScaler
from source.pipeline.model_manager import upload_results
from source.utils import disable_warnings, get_logger, is_repo_clean, create_hist_bins
from source.pipeline.metrics import *
from source.report import plot_error_map
from source.pipeline.metrics_gradient import Grad_Sobel
from source.pipeline.metrics_SSIM import SSIM
from source.uncertainty import EpistemicUncertainty
from torch.nn.parallel.data_parallel import DataParallel

from source.pipeline.models import *
from source.pipeline.losses import *

INDEX_FILENAME = 'index.csv'

BUCKET = 'fdl-mag-experiments'

disable_warnings()

logger = get_logger(__name__)

index_filename = 'index.csv'

def enable_dropout(m):
  for each_module in m.modules():
    if each_module.__class__.__name__.startswith('Dropout'):
      each_module.train()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--run', required=False)
    parser.add_argument('--epoch', required=False)
    parser.add_argument('--config_path', required=True)
    parser.add_argument('--upload_results', action='store_true')
    parser.add_argument('--iterations', required=True)
    parser.add_argument('--type', required=True)
    parser.add_argument('--assess_dropout', action='store_true')

    args = parser.parse_args()
    type = args.type

    if shutil.which('gsutil') is None:
        raise EnvironmentError(f'gsutil not found on path')

    repo = is_repo_clean('.')

    logger.info(f'Starting evaluation run with {args}')
    tstart = perf_counter()

    gcs_storage_client = storage.Client()

    with open(args.config_path, 'r') as stream:
        config_data = yaml.load(stream, Loader=yaml.SafeLoader)

    data_config = config_data['data']
    rootdir = data_config['data_bucket']
    data_folder = data_config['data_folder']

    iterations = int(args.iterations)

    filename_source = None
    if 'filename_source' in data_config.keys():
        filename_source = data_config['filename_source']

    norm = None
    if 'normalisation' in data_config.keys():
         norm = data_config['normalisation']

    if 'n_frames' in config_data['net'].keys():
        nframe = int(config_data['net']['n_frames'])
    else:
        nframe = 1


    index_range = None
    if 'index_range' in data_config.keys():
        start, stop = data_config['index_range']
        index_range = np.arange(start, stop)

    if rootdir.startswith('gs://') or rootdir.startswith('gcs://'):
        gcs_storage_client = storage.Client()
        buckname = rootdir.split('//')[-1]
        dset_validation = GCSMagDataset(INDEX_FILENAME, buckname, '{}/{}'.format(data_folder, type),
                                  gcs_storage_client, norm_factor=norm, nframe=nframe,
                                  transform=None, filename_source=filename_source, range_data=index_range)
    else:
        dset_validation = LocalMagDataset(INDEX_FILENAME, rootdir, '{}/{}'.format(data_folder, type),
                                    filename_source=filename_source, norm_factor=norm, nframe=nframe,
                                    transform=None, range_data=index_range)

    validation_loader = DataLoader(dset_validation, batch_size=config_data['batch_size'],
                             num_workers=0)

    results = copy.deepcopy(config_data)
    results_df = dset_validation.index_target

    logger.info("Building Model")
    model = BaseScaler.from_dict(config_data)

    tstamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

    comment = config_data.get('comment', '')
    comment = comment.replace(' ', '_')

    logging_dir = f'summaries/inference/{data_folder}_{tstamp}_{model.net.name}_' \
                  f'{model.loss.name}_{model.batch_size}_' \
                  f'{model.learning_rate}_{comment}'
    os.makedirs(logging_dir, exist_ok=True)

    if args.epoch is not None and args.run is not None and config_data['net'] != 'BiCubic':
        bucket = gcs_storage_client.bucket('fdl-mag-experiments')
        blob = bucket.blob(f'checkpoints/{args.run}/epoch_{args.epoch}')

        if not os.path.exists(f'checkpoints/{args.run}'):
            logger.info(f'Creating checkpoint folder: checkpoints/{args.run}')
            os.makedirs(f'checkpoints/{args.run}')
        if not os.path.exists(f'checkpoints/{args.run}/epoch_{args.epoch}'):
            logger.info(f'Downloading checkpoint: {args.epoch}')
            file = blob.download_to_filename(f'checkpoints/{args.run}/epoch_{args.epoch}')

        checkpoint = torch.load(f'checkpoints/{args.run}/epoch_{args.epoch}', map_location='cpu')
        logger.info(f'Loading Model: fdl-mag-experiments/checkpoints/{args.run}/epoch_{args.epoch}')

        if list(checkpoint['model_state_dict'].keys())[0].split('.')[0] == 'module':
            state_dict = {}
            for key, value in checkpoint['model_state_dict'].items():
                state_dict['.'.join(key.split('.')[1:])] = value
                #state_dict[key] = value

            model.net.load_state_dict(state_dict)
        else:
            model.net.load_state_dict(checkpoint['model_state_dict'])

    model.net.to(model.device)
    enable_dropout(model.net)
    epistemic_estimator = EpistemicUncertainty(model.net, model.net.p)

    bins_var = np.linspace(0, 400, num=100)
    bins_mean = create_hist_bins()
    if args.assess_dropout:
        hist_var = np.zeros((bins_var.shape[0] - 1, bins_var.shape[0] -1))
        hist_mean = np.zeros((bins_mean.shape[0] - 1, bins_mean.shape[0] - 1))
        hist_error_mc = np.zeros((bins_var.shape[0] - 1, bins_var.shape[0] - 1))

    hist_error = np.zeros((bins_var.shape[0] - 1, bins_var.shape[0] -1))

    for iteration, batch in enumerate(validation_loader, config_data['batch_size']):
        input = batch['input'].to(model.device)
        target = batch['target'].to(model.device)

        out_mean, out_var = epistemic_estimator.forward(input)

        output = model.net.forward(input)

        out = torch.zeros((*output.shape, iterations))

        if args.assess_dropout:
            for i in range(iterations):
                output_drop = model.net.forward(input)
                out[..., i] = output_drop * norm

            mc_mean = torch.mean(out, -1).detach().cpu().numpy()
            mc_var = torch.sqrt(torch.var(out, -1)).detach().cpu().numpy()

        target = target.cpu().numpy() * norm

        out_var = np.sqrt(out_var.detach().cpu().numpy() * norm ** 2)
        out_mean = out_mean.detach().cpu().numpy() * norm

        error = np.sqrt((target - out_mean) ** 2)
        herror, _, _ = np.histogram2d(error.ravel(), out_var.ravel(), bins=bins_var)
        hist_error += herror


        if args.assess_dropout:
            hmean, _, _ = np.histogram2d(mc_mean.ravel(), out_mean.ravel(), bins=bins_mean)
            hist_mean += hmean
            hvar, _, _ = np.histogram2d(mc_var.ravel(), out_var.ravel(), bins=bins_var)
            hist_var += hvar

            error_mc = np.sqrt((target - mc_mean) ** 2)
            herror_mc, _, _ = np.histogram2d(error_mc.ravel(), mc_var.ravel(), bins=bins_var)
            hist_error_mc += herror_mc

    tend = perf_counter()
    logger.info(f'Finished evaluation took: {(tend - tstart)/60.0}m')

    # dump results in json file
    json_results = json.dumps(results)
    outfile = f'{logging_dir}/results_{type}.json'
    f = open(outfile, "w")
    f.write(json_results)
    f.close()

    # save index with corresponding metrics
    results_df.to_csv(f'{logging_dir}/results_mag_{type}.csv')

    # save histograam of mean and variance between mc and direct inference
    if args.assess_dropout:
        np.save(f'{logging_dir}/histogram_var.npy', hist_var)
        np.save(f'{logging_dir}/histogram_mean.npy', hist_mean)
        np.save(f'{logging_dir}/histogram_error_mc.npy', hist_error_mc)
    np.save(f'{logging_dir}/histogram_error.npy', hist_error)

    # Add git info
    with open(f'{logging_dir}/gitinfo', mode='w') as file:
        git_data = f'branch: {repo.active_branch.name} \ncommit: {repo.head.commit}'
        file.write(git_data)

    if args.upload_results:
        # Copy to bucket
        res = subprocess.call(['gsutil', '-m', 'cp', '-r', logging_dir,
                               f'gs://{BUCKET}/inference'])
