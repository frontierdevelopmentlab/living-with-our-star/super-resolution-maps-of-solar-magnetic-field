import numpy as np
import matplotlib.pylab as plt
import pandas as pd
from matplotlib.dates import DateFormatter
from matplotlib.lines import Line2D


# Color Axis limits
vmin = -2000
vmax = 2000

# Color definitions
ClrS = (0.74, 0.00, 0.00)
ClrN = (0.20, 0.56, 1.00)

Clr = [(0.00, 0.00, 0.00),
      (0.31, 0.24, 0.00),
      (0.43, 0.16, 0.49),
      (0.32, 0.70, 0.30),
      (0.45, 0.70, 0.90),
      (1.00, 0.82, 0.67)]

# Colormap Definition
current_cmap = plt.cm.get_cmap('hmimag').copy()
current_cmap.set_bad(color='black')

grad_cmap = plt.cm.get_cmap('seismic').copy()
grad_cmap.set_bad(color='black')

grad_cmap_abs = plt.cm.get_cmap('magma').copy()

linestyles = ['--','-',':','.-']
markers = ['o','s', 'v', '*']



class sharpGradUflux:
    def __init__(self, path):
        """Class to read and process a CSV created in the batch SHARP processing

        Parameters
        ----------
        path : string
            path to CSV file
        """

        self.df = pd.read_csv(path, parse_dates=['date']).set_index(['sharpId', 'instrument', 'dataType', 'resolution'])
        if 'Unnamed: 0' in self.df.keys().to_list():
            self.df = self.df.drop('Unnamed: 0', axis=1)
        self.sharpIds = np.unique(self.df.index.get_level_values(0))
        self.instruments = np.unique(self.df.index.get_level_values(1))
        self.dataTypes = np.unique(self.df.index.get_level_values(2))
        self.resolutions = np.unique(self.df.index.get_level_values(3))
        self.variables = self.df.keys().to_list()
        self.variables.remove('date')



    def plotSharpEvolution(self, sharpId, savePath=None, variables=None, dataTypes=None, instruments=None, resolutions=None, lw=1, norm=True):
        """Method to plot the time evolution of the gradient or unsigned flux of a sharp.0

        Parameters
        ----------
        sharpId : string
            Identifier of the sharp region that we want to plot
        savePath : string
            Path to save the figure.  It includes the file format in the form of the extension
        variables : list of string
            Which variables to add to the plots, 'fullUflux', 'maskedUflux', 'fullGrad', 'maskedGrad'
            default is None, in which case it plots all
        dataTypes : list of strings
            Which data type to add to the plots, 'base', 'reproject', 'ML'
            default is None, in which case it plots all
        instruments : list of strings
            Which instrument to add to the plots, 'MDI', 'GONG'
            default is None, in which case it plots all
        resolutions : list of strings
            Which resolutions to add to the plots, '1X', '4X'
            default is None, in which case it plots all
        lw : float
            line width of Bmask contour
        norm : bool
            Whether to normalize GONG and MDI
        """

        if variables is None:
            variables = self.variables.copy()

        if instruments is None:
            instruments = self.instruments.copy()

        if dataTypes is None:
            dataTypes = self.dataTypes.copy()

        if resolutions is None:
            resolutions = self.resolutions.copy()

        nInst = len(instruments)
        nDat = len(variables)

        # Size definitions
        dpi = 400
        pxx = 600   # Horizontal size of each panel
        pxy = 600   # Vertical size of each panel

        nph = nInst     # Number of horizontal panels
        npv = nDat     # Number of vertical panels 

        # Padding
        padv  = 0  #Vertical padding in pixels
        padv2 = 0  #Vertical padding in pixels between panels
        padh  = 0 #Horizontal padding in pixels at the edge of the figure
        padh2 = 0  #Horizontal padding in pixels between panels

        # Figure sizes in pixels
        fszv = (npv*pxy + 2*padv + (npv-1)*padv2 )      #Vertical size of figure in pixels
        fszh = (nph*pxx + 2*padh + (nph-1)*padh2 )      #Horizontal size of figure in pixels

        # Conversion to relative units
        ppxx   = pxx/fszh      # Horizontal size of each panel in relative units
        ppxy   = pxy/fszv      # Vertical size of each panel in relative units
        ppadv  = padv/fszv     #Vertical padding in relative units
        ppadv2 = padv2/fszv    #Vertical padding in relative units
        ppadh  = padh/fszh     #Horizontal padding the edge of the figure in relative units
        ppadh2 = padh2/fszh    #Horizontal padding between panels in relative units


        ## Start Figure
        fig = plt.figure(figsize=(fszh/dpi,fszv/dpi), dpi = dpi, facecolor='w')
        gs = fig.add_gridspec(nDat, nInst, hspace=0, wspace=0, left=ppadh, right=ppadh+nph*ppxx, bottom=ppadv, top = ppadv+npv*ppxy)
        axs = gs.subplots(sharex=True, sharey='row')

        legend_dt = []

        for j, variable in enumerate(variables):

            for i, instrument in enumerate(instruments):

                y1MDI = None
                y2MDI = None
                y1GONG = None
                y2GONG = None

                for k, dataType in enumerate(dataTypes):

                    factor = 1
                    if dataType == 'base' and instrument == 'MDI' and norm:
                        factor = 1/1.3
                    if dataType == 'base' and instrument == 'GONG' and norm:
                        factor = 1/0.67

                    if dataType == 'reproject':
                        color = 'r'
                    else:
                        color = Clr[(k+2)%len(Clr)]

                    mk = markers[k%len(markers)]

                    if i==0 and j==0:
                        legend_dt.append(Line2D([0], [0], color=color, lw=lw, marker=mk, label=dataType))                    

                    for l, resolution in enumerate(resolutions):
                        
                        ls = linestyles[l%len(linestyles)]
                        mec = '0.8'
                        if resolution == '4X':
                            mec = 'k'


                        if not(dataType == 'reproject' and resolution == '1X'):


                            try:
                                x = self.df.loc[(sharpId,instrument,dataType,resolution),'date'].values
                                y = self.df.loc[(sharpId,instrument,dataType,resolution),variable].values


                                if instrument == 'GONG':
                                    if y1GONG is None:
                                        y1GONG = np.nanmin(y)
                                    else:
                                        y1GONG = np.nanmin([y1GONG, np.nanmin(y)])

                                    if y2GONG is None:
                                        y2GONG = np.nanmax(y)
                                    else:
                                        y2GONG = np.nanmax([y2GONG, np.nanmax(y)])


                                if instrument == 'MDI':
                                    if y1MDI is None:
                                        y1MDI = np.nanmin(y)
                                    else:
                                        y1MDI = np.nanmin([y1MDI, np.nanmin(y)])

                                    if y2MDI is None:
                                        y2MDI = np.nanmax(y)
                                    else:
                                        y2MDI = np.nanmax([y2MDI, np.nanmax(y)])


                                axs[j,i].plot(x,y*factor, ls=ls, color=color, lw=lw, marker=mk, mec=mec)

                                if j!=0 and j!=len(variables)-1:
                                    # axs[j,i].set_xticklabels([])
                                    a=1
                                else:
                                    axs[j,i].set_xlabel('date')
                                    # axs[j,i].xaxis.set_tick_params(rotation=45)
                                    date_form = DateFormatter("%m-%d")
                                    axs[j,i].xaxis.set_major_formatter(date_form)

                                if j==0:
                                    axs[j,i].xaxis.set_label_position('top')
                                    axs[j,i].xaxis.tick_top()
                                    axs[j,i].set_title(instrument.upper())
                                    

                                if i!=0 and i!=len(instruments)-1:
                                    axs[j,i].set_yticklabels([])
                                else:
                                    axs[j,i].set_ylabel(variable)

                                if i==len(instruments)-1:
                                    axs[j,i].yaxis.set_label_position('right')
                                    axs[j,i].yaxis.tick_right()

                            except:
                                pass

        lgn = axs[0, len(instruments)-1].legend(handles=legend_dt, ncol=len(legend_dt), loc='lower center', bbox_to_anchor=(0, 1.1), frameon=False)

        # lgn.set_bbox_to_anchor(lgn.get_bbox_to_anchor(), transform=axs[npv//2, len(instruments)-1].transAxes)

        fig.autofmt_xdate(rotation=45)
        if savePath is not None:
            fig.savefig(savePath, bbox_inches='tight', dpi = dpi, pad_inches=0)
        plt.close(fig)




    def plotScatter(self, savePath=None, variables=None, dataTypes=None, instruments=None, resolutions=None, ms=4, alpha=1, norm=True, fontsize=8):
        """Method to plot the time evolution of the gradient or unsigned flux of a sharp.0

        Parameters
        ----------
        savePath : string
            Path to save the figure.  It includes the file format in the form of the extension
        variables : list of string
            Which variables to add to the plots, 'fullUflux', 'maskedUflux', 'fullGrad', 'maskedGrad'
            default is None, in which case it plots all
        dataTypes : list of strings
            Which data type to add to the plots, 'base', 'reproject', 'ML'
            default is None, in which case it plots all
        instruments : list of strings
            Which instrument to add to the plots, 'MDI', 'GONG'
            default is None, in which case it plots all
        resolutions : list of strings
            Which resolutions to add to the plots, '1X', '4X'
            default is None, in which case it plots all
        ms : 4
            Marker size
        alpha: 1
            Alpha
        norm : bool
            Whether to normalize GONG and MDI
        fontsize : int
            Size of font to use for text plot
        """

        if variables is None:
            variables = self.variables.copy()

        if instruments is None:
            instruments = self.instruments.copy()

        if dataTypes is None:
            dataTypes = self.dataTypes.copy()
        dataTypes = dataTypes[dataTypes != 'reproject']

        if resolutions is None:
            resolutions = self.resolutions.copy()

        idx = pd.IndexSlice

        nRes = len(resolutions)
        nDat = len(dataTypes)

        # Size definitions
        dpi = 400
        pxx = 600   # Horizontal size of each panel
        pxy = 600   # Vertical size of each panel

        nph = nDat     # Number of horizontal panels
        npv = nRes     # Number of vertical panels 

        # Padding
        padv  = 0  #Vertical padding in pixels
        padv2 = 0  #Vertical padding in pixels between panels
        padh  = 0 #Horizontal padding in pixels at the edge of the figure
        padh2 = 0  #Horizontal padding in pixels between panels

        # Figure sizes in pixels
        fszv = (npv*pxy + 2*padv + (npv-1)*padv2 )      #Vertical size of figure in pixels
        fszh = (nph*pxx + 2*padh + (nph-1)*padh2 )      #Horizontal size of figure in pixels

        # Conversion to relative units
        ppxx   = pxx/fszh      # Horizontal size of each panel in relative units
        ppxy   = pxy/fszv      # Vertical size of each panel in relative units
        ppadv  = padv/fszv     #Vertical padding in relative units
        ppadv2 = padv2/fszv    #Vertical padding in relative units
        ppadh  = padh/fszh     #Horizontal padding the edge of the figure in relative units
        ppadh2 = padh2/fszh    #Horizontal padding between panels in relative units



        for k, variable in enumerate(variables):

            unitFactor = 1
            if 'flux' in variable.lower():
                unitFactor = 1e-22

            for l, instrument in enumerate(instruments):            


                ## Start Figure
                fig = plt.figure(figsize=(fszh/dpi,fszv/dpi), dpi = dpi, facecolor='w')
                gs = fig.add_gridspec(nRes, nDat, hspace=0, wspace=0, left=ppadh, right=ppadh+nph*ppxx, bottom=ppadv, top = ppadv+npv*ppxy)
                axs = gs.subplots(sharex=True, sharey=True)

                n = 0

                for i, dataType in enumerate(dataTypes):

                    factor = 1
                    if dataType == 'base' and instrument == 'MDI' and norm:
                        factor = 1/1.3
                    if dataType == 'base' and instrument == 'GONG' and norm:
                        factor = 1/0.67
                        
                    

                    for j, resolution in enumerate(resolutions):

                        if not(dataType == 'reproject'):

                            try:

                                y = self.df.loc[idx[:,instrument,dataType,resolution],variable].values*factor*unitFactor
                                x = self.df.loc[idx[:,instrument,'reproject','4X'],variable].values*unitFactor

                                x = x[np.isfinite(y)]
                                y = y[np.isfinite(y)]
                                y = y[np.isfinite(x)]
                                x = x[np.isfinite(x)]

                                slope = np.sum(x*y)/np.sum(x*x)

                                axs[j,i].scatter(x,y, s=ms, alpha=alpha, edgecolors='None', zorder=2)
                                axs[j,i].text(0.01, 0.99, f'{self.change_annotation_text(dataType)} {resolution}\n{instrument.upper()} = {np.round(slope,2)} HMI', horizontalalignment='left', verticalalignment='top', color = 'k', transform=axs[j,i].transAxes, fontsize=fontsize)
                                letter = chr(ord('a') + n)
                                axs[j,i].text(0.99, 0.01, f'({letter})', horizontalalignment='right', verticalalignment='bottom', color = 'k', transform=axs[j,i].transAxes, fontsize=fontsize)
                                n += 1


                                x1 = 0
                                x2 = np.nanmax(x)
                                axs[j,i].plot([x1,x2], [x1,x2], ls=':', color='k', zorder=1)   
                                axs[j,i].set_xlim([x1,x2])
                                axs[j,i].set_ylim([x1,x2])


                                if j==0:
                                    axs[j,i].xaxis.set_label_position('top')
                                    axs[j,i].xaxis.tick_top()
                                    # axs[j,i].set_title(instrument.upper())

                                if j==len(resolutions)-1:
                                    axs[j,i].set_xlabel(f'HMI target') 

                                
                                if i==0:
                                    axs[j,i].set_ylabel(f'{instrument.upper()} output')   

                                    
                                if i==len(instruments)-1:
                                    axs[j,i].yaxis.set_label_position('right')
                                    axs[j,i].yaxis.tick_right()



                                

                            except:
                                pass

                     
                fig.suptitle(f'{instrument} {self.change_title_text(variable)}', y=1.025, va='bottom')
                if savePath is not None:
                    savePath = savePath.split('.png')[0]
                    fig.savefig(f'{savePath}_{instrument}_{self.change_file_text(variable)}.png', bbox_inches='tight', dpi = dpi, pad_inches=0)
                plt.close(fig)

        
    def change_file_text(self, variable):
        """Method to change the text used to name files so that the files are arranged more sensibly in alphabetical order

        Parameters
        ----------
        variable : string
            The string to be converted

        Returns
        -------
        variable : string
            Transformed screen if match found
        """

        if variable == 'fullUflux':
            return 'ufluxFull'
        if variable == 'maskedUflux':
            return 'ufluxMasked'
        if variable == 'fullGrad':
            return 'gradFull'
        if variable == 'maskedGrad':
            return 'gradMasked'
        return variable



    def change_title_text(self, variable):
        """Method to change the text used to for the titles to use Latex expressions

        Parameters
        ----------
        variable : string
            The string to be converted

        Returns
        -------
        variable : string
            Transformed screen if match found
        """

        if variable == 'fullUflux':
            return '$\iint \left|B_{LOS}\\right| \,dA$ ($10^{22}$ Mx)'
        if variable == 'maskedUflux':
            return 'ufluxMasked'
        if variable == 'fullGrad':
            return '$\overline{\left|\\nabla B_{LOS}\\right|}$ ($10^{-8}$ G/cm)'
        if variable == 'maskedGrad':
            return 'gradMasked'
        return variable



    def change_annotation_text(self, dataType):
        """Method to change the text used in the internal plot text to make it easier to understand by a general public

        Parameters
        ----------
        variable : string
            The string to be converted

        Returns
        -------
        variable : string
            Transformed screen if match found
        """

        if dataType == 'base':
            return 'Proportional'
        if dataType == 'ml':
            return 'ML'

        return dataType


