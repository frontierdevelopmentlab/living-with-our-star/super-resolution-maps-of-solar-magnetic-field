import numpy as np
from scipy.interpolate import griddata
from astropy.io import fits
import matplotlib.pylab as plt
from sunpy.map import Map
import astropy.units as u

from sunpy.coordinates import propagate_with_solar_surface
from .utils import latLonRemap, mapCrop, mapPixelArea, makeBMask, sphericalGrad

# Initialize Python Logger
import logging
logging.basicConfig(format='%(levelname)-4s '
                           '[%(module)s:%(funcName)s:%(lineno)d]'
                           ' %(message)s')
LOG = logging.getLogger()
LOG.setLevel(logging.INFO)


# Color Axis limits
vmin = -2000
vmax = 2000

# Color definitions
ClrS = (0.74, 0.00, 0.00)
ClrN = (0.20, 0.56, 1.00)

Clr = [(0.00, 0.00, 0.00),
      (0.31, 0.24, 0.00),
      (0.43, 0.16, 0.49),
      (0.32, 0.70, 0.30),
      (0.45, 0.70, 0.90),
      (1.00, 0.82, 0.67)]

# Colormap Definition
current_cmap = plt.cm.get_cmap('hmimag').copy()
current_cmap.set_bad(color='black')

grad_cmap = plt.cm.get_cmap('seismic').copy()
grad_cmap.set_bad(color='black')

grad_cmap_abs = plt.cm.get_cmap('magma').copy()
grad_cmap_abs.set_bad(color='black')


class magnetogram:
    def __init__(self, path, instrument, recenter=True, rmask=1.0):
        """Class to store sunpy maps of magnetograms as well as related transformations.

        Parameters
        ----------
        path : string
            path to magnetogram fits file
        instrument : string
            name of the instrument
        recenter : bool
            Whether to recenter the image on the disk center
        rmaxk : float
            Solar radius beyond which values are set to 0
        """
        LOG.info(f'Loading {instrument.upper()} magnetogram...')

        self.path = path.lower()
        self.instrument = instrument
        self.rmask = rmask

        # Magnetogram Dictionaries
        self.ml = {}   # Dictionary to store ML inference maps
        self.mscl = {}  # Dictionary to store upscaled maps
        self.mrep = {}  # Dictionary to store reprojected maps

        # Auxiliary Data Dictionaries
        self.rSun = {}  # Dictionary to store solar radius arrays
        self.area = {}  # Dictionary to store area arrays

        # Sharp Dictionaries
        self.sharps = {}

        # Read magnetogram
        fitsFile = fits.open(path, cache=False)
        fitsFile.verify('fix')

        if instrument.lower() == 'gong':
            data = fitsFile[0].data
            header = fitsFile[0].header
        else:
            data = fitsFile[1].data
            header = fitsFile[1].header

        fitsFile.close()

        # Process header
        if instrument.lower() == 'gong':
            header['RSUN_OBS'] = (header['RADIUS'] * u.rad).to(u.arcsec).value
            header['RSUN_REF'] = 696000000
            header['CROTA2'] = 0
            header['CUNIT1'] = 'arcsec'
            header['CUNIT2'] = 'arcsec'
            header['DSUN_REF'] = 149597870691
            header['DSUN_OBS'] = header['DSUN_REF'] * header['DISTANCE']
            header['cdelt1'] = 2.5310 + 0.0005 + 0.015
            header['cdelt2'] = 2.5310 + 0.0005 + 0.015
            header['crpix1'] = header['FNDLMBXC']
            header['crpix2'] = header['FNDLMBYC']
            header['R_SUN'] = header['RSUN_OBS'] / header['cdelt2']
            header['CTYPE1'] = 'HPLN-TAN'
            header['CTYPE2'] = 'HPLT-TAN'
            date = header['DATE-OBS']
            header['DATE-OBS'] = date[0:4] + '-' + date[5:7] + '-' + date[8:10] + 'T' + header['TIME-OBS'][0:11] + '0'

        # Remove NaNs
        data[np.isnan(data)] = 0

        # Assemble map
        map = Map(data, header)

        # Rotate and recenter, if necessary
        target_shape = data.shape
        map = map.rotate(recenter=recenter)

        # Crop image to desired shape
        sz_x_diff = (map.data.shape[0]-target_shape[0])//2
        sz_y_diff = (map.data.shape[1]-target_shape[1])//2

        map.meta['crpix1'] = map.meta['crpix1']-sz_x_diff
        map.meta['crpix2'] = map.meta['crpix2']-sz_y_diff

        self.map = Map(map.data[sz_x_diff:sz_x_diff+target_shape[0], sz_y_diff:sz_y_diff+target_shape[1]].copy(), map.meta)

        x, y = np.meshgrid(*[np.arange(v.value) for v in self.map.dimensions]) * u.pixel
        hpc_coords = self.map.pixel_to_world(x, y)
        self.rSun['1X'] = np.sqrt(hpc_coords.Tx ** 2 + hpc_coords.Ty ** 2) / self.map.rsun_obs
        self.map.data[self.rSun['1X']>rmask] = 0 



    def pixelArea(self, scaleFactor, reprojected=False, ML=False):
        """Method to calculate each pixel's area.  It assumes that you have already created
           maps of the right scalefactor.

        Parameters
        ----------
        scaleFactor : float
            Scale factor to use.
        reprojected : bool
            Whether to also project the sharp region on reprojected maps.
        ML : bool
            Whether to also project the sharp region on ML inference maps.
        """
        

        resolution = str(scaleFactor)+'X'
        LOG.info(f'Calculating pixel Area for {self.instrument.upper()} at {resolution} resolution...')

        map = self.map
        if scaleFactor != 1:
            if resolution not in self.mscl.keys():
                LOG.info(f'{resolution} Scaled map is not available, please run resize() method first')
                return
            else:
                map = self.mscl[resolution]

        area = mapPixelArea(map)
        if 'base' not in self.area.keys():
            self.area['base'] = {}
        self.area['base'][resolution] = area

        if ML:
            if resolution not in self.ml.keys():
                LOG.info(f'{resolution}-reprojected scaled map is not available, please run resize() method first')
                return
            else:
                map = self.ml[resolution]            
                area = mapPixelArea(map)
                if 'ml' not in self.area.keys():
                    self.area['ml'] = {}
                self.area['ml'][resolution] = area                

        if reprojected:
            if resolution not in self.mrep.keys():
                LOG.info(f'{resolution}-reprojected scaled map is not available, please run resize() method first')
                return
            else:
                map = self.mrep[resolution]            
                area = mapPixelArea(map)
                if 'reproject' not in self.area.keys():
                    self.area['reproject'] = {}
                self.area['reproject'][resolution] = area  


    def reproject(self, inputMap, scaleFactor):
        """Method to reproject another instrument into the current instrument.
           It assumes that a map at the target resolution already exists.

        Parameters
        ----------
        hmiMap : Sunpy map
            Input map to be reprojected into instrument FOV
        scaleFactor : float
            Scale factor to use
        """
        resolution = str(scaleFactor)+'X'
        LOG.info(f'Reprojecting input map into {self.instrument.upper()} at {resolution} resolution...')

        map = self.map
        if scaleFactor != 1:
            if resolution not in self.mscl.keys():
                LOG.info(f'{resolution} Scaled map is not available, please run resize() method first')
                return
            else:
                map = self.mscl[resolution]


        # output, footprint = reproject_interp(inputMap, map.wcs, map.data.shape)
        # self.mrep[resolution] = Map(output, map.wcs)
        with propagate_with_solar_surface():
            self.mrep[resolution] = inputMap.reproject_to(map.wcs)

        self.mrep[resolution].data[self.rSun[resolution]>self.rmask] = 0
        



    def resize(self, scaleFactor, recenter=True):
        """Method to create a rescaled version of the map.

        Parameters
        ----------
        scaleFactor : float
            Scale factor to use
        recenter : bool
            Whether to recenter the image on the disk center
        """        

        resolution = str(scaleFactor)+'X'
        LOG.info(f'Upscaling {self.instrument.upper()} to {resolution} resolution...')
        

        target_shape = list(self.map.data.shape)
        target_shape[0] = target_shape[0]*scaleFactor
        target_shape[1] = target_shape[1]*scaleFactor

        mapRsz = self.map
        # Reform map to new size if original shape is too small
        if scaleFactor>0:
            new_fov = np.zeros((target_shape[0], target_shape[0]))
            new_meta = self.map.meta.copy()

            new_meta['crpix1'] = new_meta['crpix1'] - self.map.data.shape[0] / 2 + new_fov.shape[0] / 2
            new_meta['crpix2'] = new_meta['crpix2'] - self.map.data.shape[1] / 2 + new_fov.shape[1] / 2


            # Identify the indices for appending the map original FoV
            i1 = int(new_fov.shape[0] / 2 - self.map.data.shape[0] / 2)
            i2 = int(new_fov.shape[0] / 2 + self.map.data.shape[0] / 2)

            # Insert original image in new field of view
            new_fov[i1:i2, i1:i2] = self.map.data[:, :]

            # Assemble Sunpy map
            mapRsz = Map(new_fov, new_meta)

        mapRsz = mapRsz.rotate(scale=scaleFactor, recenter=True)

        # Crop image to desired shape
        sz_x_diff = (mapRsz.data.shape[0]-target_shape[0])//2
        sz_y_diff = (mapRsz.data.shape[1]-target_shape[1])//2

        mapRsz.meta['crpix1'] = mapRsz.meta['crpix1']-sz_x_diff
        mapRsz.meta['crpix2'] = mapRsz.meta['crpix2']-sz_y_diff

        self.mscl[resolution] = Map(mapRsz.data[sz_x_diff:sz_x_diff+target_shape[0], sz_y_diff:sz_y_diff+target_shape[1]].copy(), mapRsz.meta)

        x, y = np.meshgrid(*[np.arange(v.value) for v in self.mscl[resolution].dimensions]) * u.pixel
        hpc_coords = self.mscl[resolution].pixel_to_world(x, y)
        self.rSun[resolution] = np.sqrt(hpc_coords.Tx ** 2 + hpc_coords.Ty ** 2) / self.mscl[resolution].rsun_obs
        self.mscl[resolution].data[self.rSun[resolution]>self.rmask] = 0


    ##------------------------------------------------------------------------------------------------------------------------------------
    ## Sharp processing functions
    ##------------------------------------------------------------------------------------------------------------------------------------


    def sharpAverageGradient(self, BdataType='base', Bmask=False):
        """Method to calculate flux for all patches and their masks.

        Parameters
        ----------
        BdataType : string
            string indicating which datatype was used to define the Bmask
        BMask : bool
            Whether to also calculate the unsigned flux using the Bfield mask
        """

        LOG.info(f'Calculating average gradient magnitude for all SHARPS...')  

        if len(self.sharps) == 0:
            LOG.info('No SHARP regions available')
            return

        for sharpId, sharp in self.sharps.items():
            LOG.info(f'SHARP {sharpId}...')

            if len(sharp) == 0:
                LOG.info(f'SHARP {sharpId} has no projections.')
            else:
                for typeId, mapType in sharp.items():
                    if typeId != 'area':
                        if 'LL' not in mapType.keys():
                            LOG.info(f'no LL projections in SHARP {sharpId}-{typeId}.')
                        else:
                            if len(mapType['LL']) == 0:
                                LOG.info(f'no LL projections in SHARP {sharpId}-{typeId}.')
                            else:
                                if 'gradient' not in mapType['LL'].keys():
                                    self.sharps[sharpId][typeId]['LL']['gradient'] = {}
                                for resolution, Bfield in mapType['LL']['map'].items():
                                    if resolution not in self.sharps[sharpId][typeId]['LL']['gradient'].keys():
                                        self.sharps[sharpId][typeId]['LL']['gradient'][resolution] = {}
                                    colat = self.sharps[sharpId][typeId]['LL']['map'][resolution]['colat']
                                    lon = self.sharps[sharpId][typeId]['LL']['map'][resolution]['lon']
                                    data = self.sharps[sharpId][typeId]['LL']['map'][resolution]['data']
                                    dBdT, dBdP, gradMag = sphericalGrad(colat, lon, data)
                                    self.sharps[sharpId][typeId]['LL']['gradient'][resolution]['dBdT'] = dBdT
                                    self.sharps[sharpId][typeId]['LL']['gradient'][resolution]['dBdP'] = dBdP
                                    self.sharps[sharpId][typeId]['LL']['gradient'][resolution]['gradMag'] = gradMag
                                    self.sharps[sharpId][typeId]['LL']['gradient'][resolution]['full'] = np.nanmean(gradMag)  

                                    if Bmask:
                                        if 'Bmask' not in self.sharps[sharpId][BdataType]['LL'].keys():
                                            LOG.info(f'No Bmask for {sharpId}-{BdataType}-LL-{resolution} resolution.')
                                        else:
                                            self.sharps[sharpId][typeId]['LL']['gradient'][resolution]['masked'] = np.nanmean(gradMag[(self.sharps[sharpId][BdataType]['LL']['Bmask'][resolution])>0])
                                    



    def sharpDataProject(self, sharpMap, sharpId, reprojected=False, ML=False, area=True):
        """Method that projects a given SHARP map into the current magnetogram.
           Sharps are projected to all available resolutions and, if the user desires,
           also to all reprojected and/or ML inference maps

        Parameters
        ----------
        sharpMap : sunpy map
            Map containing a SHARP region.  In reality it could be any submap of interest.
        sharpID : string
            A string containing the identifier of the region to use in the creation of dictionaries
        reprojected : bool
            Whether to also project the sharp region on reprojected maps.
        ML : bool
            Whether to also project the sharp region on ML inference maps.
        area : bool
            Whether to also project pixel areas.
        """

        LOG.info(f'Projecting SHARP {sharpId} to {self.instrument.upper()}...')

        # Project onto main map
        if sharpId not in self.sharps.keys(): self.sharps[sharpId] = {}
        if 'base' not in self.sharps[sharpId].keys(): self.sharps[sharpId]['base'] = {}
        if 'pos' not in self.sharps[sharpId]['base'].keys(): self.sharps[sharpId]['base']['pos'] = {}
        if 'map' not in self.sharps[sharpId]['base']['pos'].keys(): self.sharps[sharpId]['base']['pos']['map'] = {}
        self.sharps[sharpId]['base']['pos']['map']['1X'] = mapCrop(self.map, sharpMap)

        # Project unto resized maps
        if len(self.mscl) > 0:
            for resolution, map in self.mscl.items():
                self.sharps[sharpId]['base']['pos']['map'][resolution] = mapCrop(map, sharpMap)

        # Project unto reprojected maps
        if reprojected:
            if 'reproject' not in self.sharps[sharpId].keys(): self.sharps[sharpId]['reproject'] = {}
            if 'pos' not in self.sharps[sharpId]['reproject'].keys(): self.sharps[sharpId]['reproject']['pos'] = {}
            if 'map' not in self.sharps[sharpId]['reproject']['pos'].keys(): self.sharps[sharpId]['reproject']['pos']['map'] = {}
            if len(self.mrep) > 0:
                for resolution, map in self.mrep.items():
                    self.sharps[sharpId]['reproject']['pos']['map'][resolution] = mapCrop(map, sharpMap)

        # Project unto ML inference maps
        if ML:
            if 'ml' not in self.sharps[sharpId].keys(): self.sharps[sharpId]['ml'] = {}
            if 'pos' not in self.sharps[sharpId]['ml'].keys(): self.sharps[sharpId]['ml']['pos'] = {}
            if 'map' not in self.sharps[sharpId]['ml']['pos']: self.sharps[sharpId]['ml']['pos']['map'] = {}
            if len(self.ml) > 0:
                for resolution, map in self.ml.items():
                    self.sharps[sharpId]['ml']['pos']['map'][resolution] = mapCrop(map, sharpMap)
                    
        if area:
            if 'area' not in self.sharps[sharpId].keys(): self.sharps[sharpId]['area'] = {}
            for dataType in self.area.keys():
                if dataType not in self.sharps[sharpId]['area'].keys():
                    self.sharps[sharpId]['area'][dataType] = {}
                for resolution, map in self.area[dataType].items():
                    self.sharps[sharpId]['area'][dataType][resolution] = mapCrop(map, sharpMap)  



    def sharpUnsignedFlux(self, Bmask=False):
        """Method to calculate flux for all patches and their masks.

        Parameters
        ----------
        BMask : bool
            Whether to also calculate the unsigned flux using the Bfield mask
        """

        LOG.info(f'Calculating unsigned magnetic flux for all SHARPS...')  

        if len(self.sharps) == 0:
            LOG.info('No SHARP regions available')
            return

        for sharpId, sharp in self.sharps.items():
            LOG.info(f'SHARP {sharpId}...')

            if len(sharp) == 0:
                LOG.info(f'SHARP {sharpId} has no projections.')
            else:
                if 'area' not in sharp.keys():
                    LOG.info(f'No pixel area estimation for {sharpId}.')
                else:
                    for typeId, mapType in sharp.items():
                        if typeId != 'area':
                            if 'pos' not in mapType.keys():
                                LOG.info(f'no POS projections in SHARP {sharpId}-{typeId}.')
                            else:
                                if len(mapType['pos']) == 0:
                                    LOG.info(f'no POS projections in SHARP {sharpId}-{typeId}.')
                                else:
                                    if 'Uflux' not in mapType['pos'].keys():
                                        self.sharps[sharpId][typeId]['pos']['Uflux'] = {}
                                    for resolution, Bfield in mapType['pos']['map'].items():
                                        if resolution not in sharp['area'][typeId].keys():
                                            LOG.info(f'No pixel area estimation for {sharpId}-{resolution} resolution.')
                                        else:
                                            if resolution not in self.sharps[sharpId][typeId]['pos']['Uflux'].keys():
                                                self.sharps[sharpId][typeId]['pos']['Uflux'][resolution] = {}
                                            Uflux = np.abs(Bfield.data*sharp['area'][typeId][resolution].data)
                                            self.sharps[sharpId][typeId]['pos']['Uflux'][resolution]['full'] = np.nansum(Uflux)

                                            if Bmask:
                                                if 'Bmask' not in self.sharps[sharpId][typeId]['pos'].keys():
                                                    LOG.info(f'No Bmask for {sharpId}-{typeId}-pos-{resolution} resolution.')
                                                else:
                                                    mask = self.sharps[sharpId][typeId]['pos']['Bmask'][resolution]
                                                    self.sharps[sharpId][typeId]['pos']['Uflux'][resolution]['masked'] = np.nansum(Uflux*mask)



    def sharpMakeBMask(self, 
                 scaleFactor, 
                 Blim=30, 
                 area_threshold=128,
                 connectivity=2,
                 dilationR=8,
                 dataType='pos',
                 translateTo1X=False,
                 method='linear'):
        """Method to greate mask surrounding strong fields for sharp patches.

        Parameters
        ----------
        scaleFactor : float
            Scale factor to use.
        Blim : float
            Magnetic field theshold used to determine the mask kernels
        area_threshold : int
            area_threshold passed to area_opening operation
        connectivity : int
            (1) for using only immediate neighbors (vertical and horizontal).
            (2) for using also diagonals
        dilationR : 8
            Radius of dilation disk
        dataType : string
            Which data type to use for calculation: 'data'(regular), 'dataLL'(Lat-Lon projection)
        translateTo1X : bool
            Whether to interpolate the calculated mask to base resolution
        method : string
            Method to use in the griddata interpolation: 'linear', 'cubic'
        """

        resolution = str(scaleFactor)+'X'
        instrumentString = self.instrument.upper()
        LOG.info(f'Calculating magnetic field mask for {instrumentString} at {resolution} resolution...')  

        if len(self.sharps) == 0:
            LOG.info('No SHARP regions available')
            return

        for sharpId, sharp in self.sharps.items():
            LOG.info(f'SHARP {sharpId}...')
            for mapType in sharp.keys():
                if mapType != 'area':
                    if len(sharp[mapType]) == 0:
                        LOG.info(f'SHARP {sharpId} has no {instrumentString}-{mapType} projection.')
                    else:
                        if resolution not in sharp[mapType][dataType]['map'].keys():
                            LOG.info(f'SHARP {sharpId} has no {instrumentString}-{mapType} projection at {resolution} resolution.')
                        else:
                            if 'Bmask' not in self.sharps[sharpId][mapType][dataType].keys(): 
                                self.sharps[sharpId][mapType][dataType]['Bmask'] = {}

                            if dataType == 'pos':
                                map = sharp[mapType][dataType]['map'][resolution]
                                data = map.data
                            else:
                                data = sharp[mapType][dataType]['map'][resolution]['data']

                            self.sharps[sharpId][mapType][dataType]['Bmask'][resolution] = makeBMask(data, 
                                                                                        Blim=Blim, 
                                                                                        area_threshold=area_threshold, 
                                                                                        connectivity=connectivity, 
                                                                                        dilationR=dilationR)

                            if translateTo1X and resolution != 1:
                                if dataType == 'pos':
                                    y, x = np.meshgrid(*[np.arange(v) for v in sharp[mapType][dataType]['map'][resolution].data.shape])* u.pixel
                                    xy = sharp[mapType][dataType]['map'][resolution].pixel_to_world(x, y)
                                    xxh = (xy.Tx).value
                                    yyh = (xy.Ty).value

                                    y, x = np.meshgrid(*[np.arange(v) for v in sharp[mapType][dataType]['map']['1X'].data.shape])* u.pixel
                                    xy = sharp[mapType][dataType]['map']['1X'].pixel_to_world(x, y)
                                    xxl = xy.Tx.value
                                    yyl = xy.Ty.value
                                    
                                    datah  = self.sharps[sharpId][mapType][dataType]['Bmask'][resolution]

                                    mask1x = griddata((xxh.reshape(-1), yyh.reshape(-1)), datah.T.reshape(-1), (xxl, yyl), method=method)
                                    mask1x[np.isnan(mask1x)] = 0
                                    mask1x = np.clip(mask1x, 0, 1)
                                    self.sharps[sharpId][mapType][dataType]['Bmask']['1X'] = mask1x.T

                                else:
                                    colath = self.sharps[sharpId][mapType][dataType]['map'][resolution]['colat'].value
                                    lonh = self.sharps[sharpId][mapType][dataType]['map'][resolution]['lon'].value
                                    datah = self.sharps[sharpId][mapType][dataType]['Bmask'][resolution]

                                    colatl = self.sharps[sharpId][mapType][dataType]['map']['1X']['colat'].value
                                    lonl = self.sharps[sharpId][mapType][dataType]['map']['1X']['lon'].value

                                    mask1x = griddata((colath.reshape(-1), lonh.reshape(-1)), datah.reshape(-1), (colatl, lonl), method=method)
                                    mask1x[np.isnan(mask1x)] = 0
                                    mask1x = np.clip(mask1x, 0, 1)
                                    self.sharps[sharpId][mapType][dataType]['Bmask']['1X'] = mask1x



    def sharpLatLonRemap(self, dlat=None, dlon=None, method='linear'):
        """Method to project all sharp magnetograms into a lat-lon grid

        Parameters
        ----------
        dlat : u.deg
            Latitudinal grid size in u.deg (degrees)
        dlon : u.deg
            Longitudinal grid size in u.deg (degrees)
        method : string
            Method to use in the griddata interpolation: 'linear', 'cubic'
        """

        instrumentString = self.instrument.upper()
        LOG.info(f'Projecting all sharps into lat-lon grids for {instrumentString}...')  

        if len(self.sharps) == 0:
            LOG.info('No SHARP regions available')
            return

        for sharpId, sharp in self.sharps.items():
            for typeId, mapType in sharp.items():
                if typeId != 'area':
                    if 'LL' not in self.sharps[sharpId][typeId].keys(): self.sharps[sharpId][typeId]['LL'] = {}
                    if 'map' not in self.sharps[sharpId][typeId]['LL'].keys(): self.sharps[sharpId][typeId]['LL']['map'] = {}
                    for resolution, map in self.sharps[sharpId][typeId]['pos']['map'].items():
                        LOG.info(f'Projecting SHARP {sharpId}-{typeId} to lat-lon at {resolution} resolution...')
                        if resolution not in self.sharps[sharpId][typeId]['LL']['map'].keys(): 
                            self.sharps[sharpId][typeId]['LL']['map'][resolution] = {}
                        lon, colat, latlonData = latLonRemap(self.sharps[sharpId][typeId]['pos']['map'][resolution], dlat=dlat, dlon=dlon, method=method)
                        self.sharps[sharpId][typeId]['LL']['map'][resolution]['lon'] = lon
                        self.sharps[sharpId][typeId]['LL']['map'][resolution]['colat'] = colat
                        self.sharps[sharpId][typeId]['LL']['map'][resolution]['data'] = latlonData


    ##------------------------------------------------------------------------------------------------------------------------------------
    ## Plotting functions
    ##------------------------------------------------------------------------------------------------------------------------------------

    def plotBnUflux(self, 
                    axis, 
                    sharpId, 
                    resolution='1X', 
                    dataType='base', 
                    fontsize=8, 
                    ccolor='r', 
                    lw=1, 
                    Bmask=True, 
                    calibration=1,
                    title=None):
        """Method to make a single plot, for a combination of datatype and resolution, 
           showing the magnetic field and the calculated unsigned flux.

        Parameters
        ----------
        axis : plt.axis
            Axis used to place the plot
        sharpId : string
            Identifier of the sharp region that we want to plot
        resolution : string
            Resolutions to plot
        dataType : string
            Datatype to plot (i.e. base, ML, reproject)
        fontsize : int
            Size of font to plot
        ccolor : matplotlib color
            color to use in plotting the Bmask
        lw : float
            line width of Bmask contour
        BMask : bool
            Whether to also plot the Bfield mask (it won't be plotted if it doesn't exist) 
        calibration : float
            Calibration multiplication factor to use in the plot
        title : string
            Title to use instead of a generic combination of instrument,datatype, and resolution      
        """


        if title is None:
            title = f'{self.instrument.upper()}-{dataType}-{resolution}'

        map = self.sharps[sharpId][dataType]['pos']['map'][resolution]

        if 'Bmask' in self.sharps[sharpId][dataType]['pos'].items():
            mask = self.sharps[sharpId][dataType]['pos']['Bmask'][resolution]

        y, x = np.meshgrid(*[np.arange(v) for v in map.data.shape])* u.pixel
        xy = map.pixel_to_world(x, y)
        xxh = (xy.Tx).value
        yyh = (xy.Ty).value

        flux = (self.sharps[sharpId][dataType]['pos']['Uflux'][resolution]['full']*u.Mm*u.Mm*u.gauss).to(u.cm*u.cm*u.gauss)*calibration
        exponent = np.floor(np.abs(np.log10(flux.value)))
        number = np.format_float_positional(flux.value/(10**exponent), precision=3)
        if np.isfinite(exponent) and np.isfinite(flux):                    
            plotString = f'$|\phi|_{{all}}={number}x10^{{{int(exponent)}}}$Mx\n'
        else:
            plotString = f'$|\phi|_{{all}}=$NaN\n'

        if 'Bmask' in self.sharps[sharpId][dataType]['pos'].items():
            flux = (self.sharps[sharpId][dataType]['pos']['Uflux'][resolution]['masked']*u.Mm*u.Mm*u.gauss).to(u.cm*u.cm*u.gauss)*calibration
            exponent = np.floor(np.abs(np.log10(flux.value)))
            number = np.format_float_positional(flux.value/(10**exponent), precision=3)

            if np.isfinite(exponent) and np.isfinite(flux):
                plotString2 = f'$|\phi|_{{mask}}={number}x10^{{{int(exponent)}}}$Mx'
            else:
                plotString2 = f'$|\phi|_{{mask}}=$NaN'

        axis.pcolormesh(xxh, yyh, map.data.T*calibration, vmin=vmin, vmax=vmax, cmap = current_cmap)
        if 'Bmask' in self.sharps[sharpId][dataType]['pos'].items() and Bmask:
            axis.contour(xxh, yyh, mask.T, [0.5], colors=ccolor, linewidths=lw)
            axis.text(0.01, 0.99, f'{title}\n' + plotString + plotString2, horizontalalignment='left', verticalalignment='top', color = 'k', transform=axis.transAxes, fontsize=fontsize)
        else:
            axis.text(0.01, 0.99, f'{title}\n' + plotString, horizontalalignment='left', verticalalignment='top', color = 'k', transform=axis.transAxes, fontsize=fontsize)
        axis.set_axis_off()



    def multiplotBnUflux(self, sharpId, savePath=None, resolutions=None, fontsize=8, ccolor='r', lw=1, Bmask=True):
        """Method to plot all datatypes and resolutions showing the magnetic field and the calculated unsigned flux.
           It's not very robust. It will assume you have calculated everything in plane-of-the-sky (pos) and
           resolutions are consistent across quantities.

        Parameters
        ----------
        sharpId : string
            Identifier of the sharp region that we want to plot
        savePath : string
            Path to save the figure.  It includes the file format in the form of the extension
        resolutions : list
            List of resolutions to plot, if None, then all available are plotted
        fontsize : int
            Size of font to plot
        ccolor : matplotlib color
            color to use in plotting the Bmask
        lw : float
            line width of Bmask contour
        BMask : bool
            Whether to also plot the Bfield mask (it won't be plotted if it doesn't exist)
        """

        # TODO: Put the necessary conditionals that will prevent plot from crashing if there is a missing calculation

        nDtype = len(self.sharps[sharpId])
        nRes = len(self.sharps[sharpId]['base']['pos']['map'])

        # Size definitions
        dpi = 400
        pxx = self.sharps[sharpId]['base']['pos']['map']['1X'].data.shape[1]*4   # Horizontal size of each panel
        pxy = self.sharps[sharpId]['base']['pos']['map']['1X'].data.shape[0]*4   # Vertical size of each panel

        nph = nDtype     # Number of horizontal panels
        npv = nRes     # Number of vertical panels 

        # Padding
        padv  = 0  #Vertical padding in pixels
        padv2 = 0  #Vertical padding in pixels between panels
        padh  = 0 #Horizontal padding in pixels at the edge of the figure
        padh2 = 50  #Horizontal padding in pixels between panels

        # Figure sizes in pixels
        fszv = (npv*pxy + 2*padv + (npv-1)*padv2 )      #Vertical size of figure in pixels
        fszh = (nph*pxx + 2*padh + (nph-1)*padh2 )      #Horizontal size of figure in pixels

        # Conversion to relative units
        ppxx   = pxx/fszh      # Horizontal size of each panel in relative units
        ppxy   = pxy/fszv      # Vertical size of each panel in relative units
        ppadv  = padv/fszv     #Vertical padding in relative units
        ppadv2 = padv2/fszv    #Vertical padding in relative units
        ppadh  = padh/fszh     #Horizontal padding the edge of the figure in relative units
        ppadh2 = padh2/fszh    #Horizontal padding between panels in relative units


        ## Start Figure
        fig = plt.figure(figsize=(fszh/dpi,fszv/dpi), dpi = dpi)


        for i, dataType in enumerate(self.sharps[sharpId].keys()):
            if dataType != 'area':

                if resolutions is None:
                    resolutions = self.sharps[sharpId][dataType]['pos']['Uflux'].keys()

                for j, resolution in enumerate(resolutions):


                    if resolution in self.sharps[sharpId][dataType]['pos']['map'].items():                    
                        ax1 = fig.add_axes([ppadh+i*ppxx, ppadv+j*ppxy, ppxx, ppxy])
                        self.plotBnUflux(ax1, sharpId, resolution=resolution, dataType=dataType, fontsize=fontsize, ccolor=ccolor, lw=lw, Bmask=Bmask)


        if savePath is not None:
            fig.savefig(savePath, bbox_inches='tight', dpi = dpi, pad_inches=0)
        plt.close(fig)


    def plotMeanGrad(self, 
                     axis, 
                     sharpId, 
                     variable='gradMag', 
                     resolution='1X', 
                     dataType='base', 
                     fontsize=8, 
                     ccolor='r', 
                     lw=1, 
                     Bmask=True, 
                     calibration=1,
                     vmax=None,
                     title=None):
        """Method to make a single plot, for a combination of datatype and resolution, 
           showing the magnetic field and the calculated average gradient.

        Parameters
        ----------
        axis : plt.axis
            Axis used to place the plot
        sharpId : string
            Identifier of the sharp region that we want to plot
        variable : string
            Variable to plot: 'dBdT', 'dBdP', and 'gradMag'
        resolution : string
            Resolution to plot
        dataType : string
            Datatype to plot (i.e. base, ML, reproject)
        fontsize : int
            Size of font to plot
        ccolor : matplotlib color
            color to use in plotting the Bmask
        lw : float
            line width of Bmask contour
        BMask : bool
            Whether to also plot the Bfield mask (it won't be plotted if it doesn't exist) 
        calibration : float
            Calibration multiplication factor to use in the plot    
        title : string
            Title to use instead of a generic combination of instrument,datatype, and resolution      
        """


        if title is None:
            title = f'{self.instrument.upper()}-{dataType}-{resolution}'

        cmap = grad_cmap
        txtc = 'k'
        if variable == 'gradMag':
            cmap = grad_cmap_abs
            txtc = 'w'   


        data = self.sharps[sharpId][dataType]['LL']['gradient'][resolution][variable]
        LOG.info(f'Max gradient {np.nanmax(data)}')

        if 'Bmask' in self.sharps[sharpId][dataType]['pos'].items():
            mask = self.sharps[sharpId][dataType]['LL']['Bmask'][resolution]

        colat = self.sharps[sharpId][dataType]['LL']['map'][resolution]['colat'].value
        lon = self.sharps[sharpId][dataType]['LL']['map'][resolution]['lon'].value

        vlim = np.nanmax(np.abs(data.value))
        vhigh = vlim
        vlow = -vlim
        if variable == 'gradMag':
            vlow=0

        if vmax is not None:
            vhigh = vmax

        LOG.info(f'Vhigh = {vhigh} - vlow = {vlow}')
        
        gradient = np.round(self.sharps[sharpId][dataType]['LL']['gradient'][resolution]['full'].value*calibration, 3)
        if np.isfinite(gradient):
            plotString = f'$mean(|\\nabla B_{{LOS}}|)_{{all}}={gradient}$G/Mm\n'
        else:
            plotString = f'$mean(|\\nabla B_{{LOS}}|)_{{all}}=$NaN\n'

        if 'Bmask' in self.sharps[sharpId][dataType]['pos'].items() and Bmask:
            gradient = np.round(self.sharps[sharpId][dataType]['LL']['gradient'][resolution]['masked'].value*calibration, 3)
            if np.isfinite(gradient):
                plotString2 = f'$mean(|\\nabla B_{{LOS}}|)_{{mask}}={gradient}$G/Mm'
            else:
                plotString2 = f'$mean(|\\nabla B_{{LOS}}|)_{{mask}}=$NaN'

        axis.pcolormesh(lon, colat, data.value*calibration, vmin=vlow, vmax=vhigh, cmap = cmap)
        if 'Bmask' in self.sharps[sharpId][dataType]['pos'].items():
            axis.contour(lon, colat, mask, [0.5], colors=ccolor, linewidths=lw)
            axis.text(0.01, 0.99, f'{title}\n' + plotString + plotString2, horizontalalignment='left', verticalalignment='top', transform=axis.transAxes, fontsize=fontsize, color=txtc)
        else:
            axis.text(0.01, 0.99, f'{title}\n' + plotString, horizontalalignment='left', verticalalignment='top', transform=axis.transAxes, fontsize=fontsize, color=txtc)

        axis.set_axis_off()
        axis.invert_yaxis()



    def multiplotMeanGrad(self, sharpId, variable='gradMag', resolutions=None, savePath=None, fontsize=8, ccolor='r', lw=1, Bmask=True):
        """Method to plot all datatypes and resolutions showing the calculated gradient.
           It's not very robust. It will assume you have calculated everything already in a colat-lon grid and that
           resolutions are consistent across quantities.

        Parameters
        ----------
        sharpId : string
            Identifier of the sharp region that we want to plot
        variable : string
            Variable to plot: 'dBdT', 'dBdP', and 'gradMag'
        resolutions : list
            List of resolutions to plot, if None, then all available are plotted
        savePath : string
            Path to save the figure.  It includes the file format in the form of the extension
        fontsize : int
            Size of font to plot
        ccolor : matplotlib color
            color to use in plotting the Bmask
        lw : float
            line width of Bmask contour
        BMask : bool
            Whether to also plot the Bfield mask (it won't be plotted if it doesn't exist)
        """

        # TODO: Put the necessary conditionals that will prevent plot from crashing if there is a missing calculation

        nDtype = len(self.sharps[sharpId])
        nRes = len(self.sharps[sharpId]['base']['LL']['map'])

        # Size definitions
        dpi = 400
        pxx = self.sharps[sharpId]['base']['LL']['map']['1X']['data'].shape[1]*4   # Horizontal size of each panel
        pxy = self.sharps[sharpId]['base']['LL']['map']['1X']['data'].shape[0]*4   # Vertical size of each panel

        nph = nDtype     # Number of horizontal panels
        npv = nRes     # Number of vertical panels 

        # Padding
        padv  = 0  #Vertical padding in pixels
        padv2 = 0  #Vertical padding in pixels between panels
        padh  = 0 #Horizontal padding in pixels at the edge of the figure
        padh2 = 50  #Horizontal padding in pixels between panels

        # Figure sizes in pixels
        fszv = (npv*pxy + 2*padv + (npv-1)*padv2 )      #Vertical size of figure in pixels
        fszh = (nph*pxx + 2*padh + (nph-1)*padh2 )      #Horizontal size of figure in pixels

        # Conversion to relative units
        ppxx   = pxx/fszh      # Horizontal size of each panel in relative units
        ppxy   = pxy/fszv      # Vertical size of each panel in relative units
        ppadv  = padv/fszv     #Vertical padding in relative units
        ppadv2 = padv2/fszv    #Vertical padding in relative units
        ppadh  = padh/fszh     #Horizontal padding the edge of the figure in relative units
        ppadh2 = padh2/fszh    #Horizontal padding between panels in relative units


        ## Start Figure
        fig = plt.figure(figsize=(fszh/dpi,fszv/dpi), dpi = dpi)

        for i, dataType in enumerate(self.sharps[sharpId].keys()):
            if dataType != 'area':

                if resolutions is None:
                    resolutions = self.sharps[sharpId][dataType]['LL']['gradient'].keys()

                for j, resolution in enumerate(resolutions):

                    if resolution in self.sharps[sharpId][dataType]['LL']['gradient'].items():

                        ax1 = fig.add_axes([ppadh+i*ppxx, ppadv+j*ppxy, ppxx, ppxy])
                        self.plotMeanGrad(ax1, sharpId, variable=variable, resolution=resolution, dataType=dataType, fontsize=fontsize, ccolor=ccolor, lw=lw, Bmask=Bmask)

        if savePath is not None:
            fig.savefig(savePath, bbox_inches='tight', dpi = dpi, pad_inches=0)
        plt.close(fig) 
                     