import numpy as np
from sunpy.map import Map

import torch
import yaml

import sys
sys.path.append("../../../")
from source.pipeline.model_manager import BaseScaler
from source.pipeline.models.highresnet_rprcdo import HighResNet_RPRCDO

from source.pipeline.metrics_denoised_hist import DenoisedHistogram
from source.pipeline.losses.ssim_grad_hist_loss import SSIMGradHistLoss


from sharp_modules.magnetogram import magnetogram

# Initialize Python Logger
import logging
logging.basicConfig(format='%(levelname)-4s '
                           '[%(module)s:%(funcName)s:%(lineno)d]'
                           ' %(message)s')
LOG = logging.getLogger()
LOG.setLevel(logging.INFO)

class fdl2019_magnetogram(magnetogram):
    def __init__(self, path, instrument, recenter=True, rmask=1.0):
        """Class to store sunpy maps of magnetograms as well as related transformations.
           This child class of the parent class magnetogram includes the necessary method to run
           ML inference based on the results of the 2019 FDL program.  If using your own model,
           you would need to create your own child class.

        Parameters
        ----------
        path : string
            path to magnetogram fits file
        instrument : string
            name of the instrument
        recenter : bool
            Whether to recenter the image on the disk center
        rmaxk : float
            Solar radius beyond which values are set to 0
        """
        super().__init__(path, instrument, recenter, rmask)


    def runML(self, scaleFactor):
        """Method to run ML inference.  This function is based on the results of the 2019 FDL program.  If using your own model,
           you would need to create your own child class.

        Parameters
        ----------
        scaleFactor : int
            Scale factor to use.
        """

        MDIML = {}
        MDIML['1X'] = 'D:/Mis Documentos/AAResearch/FDL/2019/Converter/checkpoints/mdi/20210207214138_HighResNet_RPRCDO_SSIMGradHistLoss_jsoc_mdi_RP_D1_19'
        MDIML['4X'] = 'D:/Mis Documentos/AAResearch/FDL/2019/Converter/checkpoints/mdi/20201020035600_HighResNet_RPRCDO_SSIMGradHistLoss_jsoc_mdi_RP_19'

        GONGML = {}
        GONGML['1X'] = 'D:/Mis Documentos/AAResearch/FDL/2019/Converter/checkpoints/gong/20210129203433_HighResNet_RPRCDO_SSIMGradHistLoss_gong_RP_D1_19'
        GONGML['4X'] = 'D:/Mis Documentos/AAResearch/FDL/2019/Converter/checkpoints/gong/20201214200251_HighResNet_RPRCDO_SSIMGradHistLoss_gong_RP_19'

        resolution = str(scaleFactor)+'X'
        LOG.info(f'Running {resolution} ML inference on {self.instrument.upper()}...')


        if self.instrument=='hmi':
            LOG.info('No ML model for HMI')
            return
        if self.instrument=='mdi':
            mlModels = MDIML
        if self.instrument=='gong':
            mlModels = GONGML

        if resolution not in mlModels.keys():
            LOG.info('No available ML model for that scale factor')
            return
        else:
            mlModel = mlModels[resolution]

        with open(mlModel + '.yml', 'r') as stream:
            config_data = yaml.load(stream, Loader=yaml.SafeLoader)

        data_config = config_data['data']
        norm = 3500
        if 'normalisation' in data_config.keys():
            norm = data_config['normalisation']

        net_config = config_data['net']
        model_name = net_config['name']

        model = BaseScaler.from_dict(config_data)

        device = torch.device("cpu")
        model = model.net.to(device)

        checkpoint = torch.load(mlModel, map_location='cpu')
        try:

            try:
                model.load_state_dict(checkpoint['model_state_dict'])

            except:
                state_dict = {}
                for key, value in checkpoint['model_state_dict'].items():
                    state_dict['.'.join(key.split('.')[1:])] = value
                model.load_state_dict(state_dict)
        except:
            state_dict = {}
            for key, value in checkpoint['model_state_dict'].items():
                state_dict['.'.join(np.append(['module'], key.split('.')[0:]))] = value
            model.load_state_dict(state_dict)

        in_fd = np.stack([self.map.data/norm, self.rSun['1X']], axis=0)
        inferred = (model.forward(torch.from_numpy(in_fd[None]).to(device).float()).detach().numpy()[0,...]*norm).squeeze()
        inferred[self.rSun[resolution]>self.rmask] = 0
        
        if scaleFactor == 1:
            self.ml[resolution] = Map(inferred, self.map.meta)
        else:
            new_meta = self.map.meta.copy()
            new_meta['cdelt1'] = new_meta['cdelt1']/scaleFactor
            new_meta['cdelt2'] = new_meta['cdelt2']/scaleFactor
            new_meta['r_sun'] = 2*new_meta['rsun_obs']/(new_meta['cdelt1'] + new_meta['cdelt2'])
            new_meta['crpix1'] = (new_meta['crpix1']-0.5)*scaleFactor + 0.5
            new_meta['crpix2'] = (new_meta['crpix2']-0.5)*scaleFactor + 0.5
            
            self.ml[resolution] = Map(inferred, new_meta)


    