"""
Main training class
"""
import argparse
import datetime
import os
import shutil
import subprocess
import json
import copy
from time import perf_counter

import numpy as np
import pandas as pd
import torch
import yaml
from google.cloud import storage
from tensorboardX.writer import SummaryWriter
from torch.utils.data.dataloader import DataLoader
from sklearn.feature_extraction import image

from source.pipeline.dataset import GCSMagDataset, LocalMagDataset
from source.pipeline.model_manager import BaseScaler
from source.pipeline.model_manager import upload_results
from source.utils import disable_warnings, get_logger, is_repo_clean, create_hist_bins
from source.pipeline.metrics import *
from source.pipeline.metrics_gradient import Grad_Sobel
from source.pipeline.metrics_SSIM import SSIM

from torch.nn import functional as F
import torch

from source.pipeline.models import *
from source.pipeline.losses import *

INDEX_FILENAME = 'index.csv'

BUCKET = 'fdl-mag-experiments'

disable_warnings()

logger = get_logger(__name__)

index_filename = 'index.csv'

# histogram parameters
bins = create_hist_bins()
binsp = np.linspace(0,1,bins.shape[0])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--run', required=False)
    parser.add_argument('--epoch', required=False)
    parser.add_argument('--ups_run', required=False)
    parser.add_argument('--ups_epoch', required=False)
    parser.add_argument('--config_path', required=True)
    parser.add_argument('--ups_config_path', required=False)
    parser.add_argument('--upload_results', action='store_true')
    parser.add_argument('--target_align', action='store_true')
    parser.add_argument('--favor_neg', action='store_true')
    parser.add_argument('--favor_pos', action='store_true')
    parser.add_argument('--type', required=True)
    parser.add_argument('--downscale', required=False)
    parser.add_argument('--hmi2factor', required=False)
    parser.add_argument('--nradii', required=False)

    args = parser.parse_args()
    type = args.type

    if shutil.which('gsutil') is None:
        raise EnvironmentError(f'gsutil not found on path')

    repo = is_repo_clean('.')

    logger.info(f'Starting evaluation run with {args}')
    tstart = perf_counter()

    gcs_storage_client = storage.Client()

    with open(args.config_path, 'r') as stream:
        config_data = yaml.load(stream, Loader=yaml.SafeLoader)

    data_config = config_data['data']
    rootdir = data_config['data_bucket']
    data_folder = data_config['data_folder']

    index_range = None
    if 'index_range' in data_config.keys():
        start, stop = data_config['index_range']
        index_range = np.arange(start, stop)
    #
    norm = None
    if 'normalisation' in data_config.keys():
         norm = data_config['normalisation']

    hmi2f = 1
    if args.hmi2factor:
         hmi2f = float(args.hmi2factor)

    nradii = 10
    if args.nradii:
        nradii = int(args.nradii)
    binsr = np.sqrt(np.arange(0,nradii+1)/nradii)

    target_downscale = 1
    if 'target_downscale' in config_data['net']:
        target_downscale = int(config_data['net']['n_frames'])

    if 'n_frames' in config_data['net'].keys():
        nframe = int(config_data['net']['n_frames'])
    else:
        nframe = 1

    upscale_factor = config_data['net']['upscale_factor']
    # if downscale is not None:
    #     upscale_factor = downscale

    logger.info("Building Model")
    model = BaseScaler.from_dict(config_data)

    tstamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

    comment = config_data.get('comment', '')
    comment = comment.replace(' ', '_')

    logging_dir = f'summaries/inference/{data_folder}_{tstamp}_{model.net.name}_' \
                  f'{model.loss.name}_{model.batch_size}_' \
                  f'{model.learning_rate}_{comment}'
    os.makedirs(logging_dir, exist_ok=True)

    if args.epoch is not None and args.run is not None and config_data['net'] != 'BiCubic':
        bucket = gcs_storage_client.bucket('fdl-mag-experiments')
        blob = bucket.blob(f'checkpoints/{args.run}/epoch_{args.epoch}')

        if not os.path.exists(f'checkpoints/{args.run}'):
            logger.info(f'Creating checkpoint folder: checkpoints/{args.run}')
            os.makedirs(f'checkpoints/{args.run}')
        if not os.path.exists(f'checkpoints/{args.run}/epoch_{args.epoch}'):
            logger.info(f'Downloading checkpoint: {args.epoch}')
            file = blob.download_to_filename(f'checkpoints/{args.run}/epoch_{args.epoch}')

        checkpoint = torch.load(f'checkpoints/{args.run}/epoch_{args.epoch}', map_location='cpu')
        logger.info(f'Loading Model: fdl-mag-experiments/checkpoints/{args.run}/epoch_{args.epoch}')

        try:

            try:
                model.net.load_state_dict(checkpoint['model_state_dict'])

            except:
                state_dict = {}
                for key, value in checkpoint['model_state_dict'].items():
                    state_dict['.'.join(key.split('.')[1:])] = value
                model.net.load_state_dict(state_dict)

        except:
            state_dict = {}
            for key, value in checkpoint['model_state_dict'].items():
                state_dict['.'.join(np.append(['module'], key.split('.')[0:]))] = value
            model.net.load_state_dict(state_dict)


    counter = 0
    model.net.to(model.device)
    model.metrics = [MeanMetric(), MaxMetric(), MinMetric(), StdevMetric(), SignedMetric(), UnsignedMetric()]

    grad_sobel = Grad_Sobel(size=11, sigma=2, device=model.device)
    model.metrics.append(grad_sobel)

    ssim = SSIM(size_average=False)
    model.metrics.append(ssim)

    pnsr = PNSR()
    model.metrics.append(pnsr)

    pearson = CorrPearson()
    model.metrics.append(pearson)

    pearson_10 = CorrPearson(clamp=15)
    model.metrics.append(pearson_10)


    model.metrics.append(Quantile(0.01))
    model.metrics.append(Quantile(0.99))

    val_metrics = {}

    for metric in model.metrics:
        val_metrics[metric.name] = 0

    model.net.eval()

    ups_upscale_factor = copy.deepcopy(upscale_factor)
    double_sw = False
    if args.ups_epoch is not None and args.ups_run is not None and args.ups_config_path is not None and config_data['net'] != 'BiCubic':

        double_sw = True
        with open(args.ups_config_path, 'r') as stream:
            ups_config_data = yaml.load(stream, Loader=yaml.SafeLoader)

        ups_data_config = ups_config_data['data']
        ups_rootdir = ups_data_config['data_bucket']
        ups_data_folder = ups_data_config['data_folder']

        ups_norm = None
        if 'normalisation' in ups_data_config.keys():
            ups_norm = ups_data_config['normalisation']

        if 'n_frames' in ups_config_data['net'].keys():
            ups_nframe = int(ups_config_data['net']['n_frames'])
        else:
            ups_nframe = 1

        ups_upscale_factor = ups_config_data['net']['upscale_factor']

        bucket = gcs_storage_client.bucket('fdl-mag-experiments')
        blob = bucket.blob(f'checkpoints/{args.ups_run}/epoch_{args.ups_epoch}')

        if not os.path.exists(f'checkpoints/{args.ups_run}'):
            logger.info(f'Creating checkpoint folder: checkpoints/{args.ups_run}')
            os.makedirs(f'checkpoints/{args.ups_run}')
        if not os.path.exists(f'checkpoints/{args.ups_run}/epoch_{args.ups_epoch}'):
            logger.info(f'Downloading checkpoint: {args.ups_epoch}')
            file = blob.download_to_filename(f'checkpoints/{args.ups_run}/epoch_{args.ups_epoch}')

        ups_checkpoint = torch.load(f'checkpoints/{args.ups_run}/epoch_{args.ups_epoch}', map_location='cpu')
        logger.info(f'Loading Model: fdl-mag-experiments/checkpoints/{args.ups_run}/epoch_{args.ups_epoch}')

        logger.info("Building upscaling Model")
        ups_model = BaseScaler.from_dict(ups_config_data)

        try:

            try:
                ups_model.net.load_state_dict(ups_checkpoint['model_state_dict'])

            except:
                state_dict = {}
                for key, value in ups_checkpoint['model_state_dict'].items():
                    state_dict['.'.join(key.split('.')[1:])] = value
                ups_model.net.load_state_dict(state_dict)

        except:
            state_dict = {}
            for key, value in ups_checkpoint['model_state_dict'].items():
                state_dict['.'.join(np.append(['module'], key.split('.')[0:]))] = value
            ups_model.net.load_state_dict(state_dict)

        ups_model.net.to(model.device)
        ups_model.metrics = [MeanMetric(), MaxMetric(), MinMetric(), StdevMetric(), SignedMetric(), UnsignedMetric()]

        ups_model.metrics.append(grad_sobel)
        ups_model.metrics.append(ssim)
        ups_model.metrics.append(pnsr)
        ups_model.metrics.append(pearson)
        ups_model.metrics.append(pearson_10)

        ups_model.metrics.append(Quantile(0.01))
        ups_model.metrics.append(Quantile(0.99))

        ups_val_metrics = {}

        for metric in ups_model.metrics:
            ups_val_metrics[metric.name] = 0

        ups_model.net.eval()
        ups_hist_target_output = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1))



    if rootdir.startswith('gs://') or rootdir.startswith('gcs://'):
        gcs_storage_client = storage.Client()
        buckname = rootdir.split('//')[-1]
        dset_validation = GCSMagDataset(INDEX_FILENAME, buckname, '{}/{}'.format(data_folder, type),
                                  gcs_storage_client, index_range, norm_factor=norm, nframe=nframe,
                                  transform=None, target_align=args.target_align,
                                  upscale_factor=np.max([upscale_factor, ups_upscale_factor]))
    else:
        dset_valP = LocalMagDataset(INDEX_FILENAME, rootdir, '{}/{}'.format(data_folder, type),
                                   index_range, norm_factor=norm, nframe=nframe,
                                   transform=None, target_align=args.target_align,
                                   upscale_factor=np.max([upscale_factor, ups_upscale_factor]), target_downscale=target_downscale)
        dset_valN = LocalMagDataset(INDEX_FILENAME, rootdir, '{}/{}'.format(data_folder, type),
                                   index_range, norm_factor=-norm, nframe=nframe,
                                   transform=None, target_align=args.target_align,
                                   upscale_factor=np.max([upscale_factor, ups_upscale_factor]), target_downscale=target_downscale)
        dset_validation = torch.utils.data.ConcatDataset([dset_valP, dset_valN])

    validation_loader = DataLoader(dset_validation, batch_size=config_data['batch_size'],
                             num_workers=0)

    results = copy.deepcopy(config_data)
    results_df = pd.concat([dset_valP.index_target, dset_valN.index_target]).reset_index()

    if double_sw:
        ups_results = copy.deepcopy(ups_config_data)
        ups_results_df = dset_validation.index_target

    # Histograms for ML output
    hist_target_output = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))
    hist_target_output_Er = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))
    hist_target_output_Flux = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))
    hist_target_output_Flux2 = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))
    hist_target_output_GradA = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))

    hist_target_output_Sh = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1))
    hist_target_output_SR = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1))

    # Histograms for bicubic upsampling
    hist_target_input = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))
    hist_target_input_Er = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))
    hist_target_input_Flux = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))
    hist_target_input_Flux2 = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))
    hist_target_input_GradA = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1, nradii))

    hist_target_input_Sh = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1))
    hist_target_input_SR = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1))

    uncertainty_list = []

    # uncertainty
    if model.loss.name == 'SSIMGradHistLossUnc':
        bins_var = np.linspace(0, 400, num=100)
        hist_mean = np.zeros((bins.shape[0] - 1, bins.shape[0] - 1))
        hist_error = np.zeros((bins_var.shape[0] - 1, bins_var.shape[0] - 1))

    for iteration, batch in enumerate(validation_loader):
        logger.info(f'Iteration:{iteration}')
        input = batch['input'].to(model.device)
        target = batch['target'].to(model.device)

        if double_sw:
            target_HR = copy.deepcopy(target)
            target = F.avg_pool2d(target, int(np.max([upscale_factor, ups_upscale_factor])))

        input_r = input[:, 1, ...].view(input.shape[0], -1).cpu().numpy()
        mean_r = np.mean(input_r)
        unsigned_t = torch.mean(torch.abs(target), dim = (-2,-1)).cpu().numpy()*norm

        target_cpu = norm * target.clone().detach().cpu().numpy()

        # Bicubic interpolation
        if upscale_factor > 1:
            upsampled_input = torch.nn.Upsample(scale_factor=upscale_factor,
                                                mode='bicubic', align_corners=False)(input)
            upsampled_r = upsampled_input[:, 1, ...].clone().detach().cpu().numpy()
            upsampled_input = upsampled_input[:, 0, ...].clone().detach().cpu().numpy() * norm / hmi2f
        else:
            upsampled_r = input[:, 1, ...].clone().detach().cpu().numpy()
            upsampled_input = input[:, 0, ...].clone().detach().cpu().numpy() * norm / hmi2f

        output = model.net.forward(input)

        if model.loss.name == 'SSIMGradHistLossUnc':
            uncertainty = output[:, 1, ...]
            output = output[:, 0, ...]

            error = ((output - target) * norm) ** 2
            error = error.detach().cpu().numpy()

            uncertainty = torch.exp(uncertainty).detach().cpu().numpy() * norm ** 2

            herror, _, _ = np.histogram2d(error.ravel(), uncertainty.ravel(), bins=bins_var)
            hist_error += herror

        if args.favor_neg or args.favor_pos:
            output_n = -model.net.forward(-input)
            if args.favor_neg:
                output[upsampled_input > 0] = output_n[upsampled_input > 0]
            else:
                output[upsampled_input < 0] = output_n[upsampled_input < 0]

        output_cpu = output.clone().detach().cpu().numpy() * norm
        grad_sobel_out_x, grad_sobel_out_y, grad_sobel_out = grad_sobel.forward(output * norm)
        grad_sobel_tar_x, grad_sobel_tar_y, grad_sobel_tar = grad_sobel.forward(target * norm)
        grad_sobel_ups_x, grad_sobel_ups_y, grad_sobel_ups = grad_sobel.forward(torch.nn.Upsample(scale_factor=upscale_factor,
                                                mode='bicubic', align_corners=False)(input) * norm)

        grad_mag_diff_out = torch.sqrt( (grad_sobel_out_x-grad_sobel_tar_x)**2 + (grad_sobel_out_y-grad_sobel_tar_y)**2)
        grad_mag_diff_ups = torch.sqrt( (grad_sobel_ups_x-grad_sobel_tar_x)**2 + (grad_sobel_ups_y-grad_sobel_tar_y)**2)


        # Holder variable for shifted difference
        diff_shift_out = np.zeros((output_cpu.shape[0], output_cpu.shape[1]-2, output_cpu.shape[2]-2, 9))
        diff_shift_in = np.zeros((output_cpu.shape[0], output_cpu.shape[1]-2, output_cpu.shape[2]-2, 9))
        reduced_output = output_cpu[:,1:-1,1:-1]
        reduced_input = upsampled_input[:,1:-1,1:-1]
        for dx in np.arange(-1,2):
            for dy in np.arange(-1,2):
                n = dx*3+dy+4
                tmp_target = target_cpu[:,1+dx:output_cpu.shape[1]-1+dx,1+dy:output_cpu.shape[2]-1+dy]
                diff_shift_out[:,:,:,n] = tmp_target-reduced_output
                diff_shift_in[:,:,:,n] = tmp_target-reduced_input

        # Find optimal and add to histogram
        indices = np.argmin(np.abs(diff_shift_out), axis=3)
        I, J, K = np.indices(indices.shape)
        H, _, _ = np.histogram2d(diff_shift_out[I,J,K,indices].reshape(-1), reduced_output.reshape(-1), bins=bins)
        hist_target_output_Sh += H

        indices = np.argmin(np.abs(diff_shift_in), axis=3)
        I, J, K = np.indices(indices.shape)
        H, _, _ = np.histogram2d(diff_shift_in[I,J,K,indices].reshape(-1), reduced_input.reshape(-1), bins=bins)
        hist_target_input_Sh += H

        ###################
        # Calculating histogram for different radii
        for i in np.arange(0, nradii):

            mask = np.logical_and(upsampled_r<binsr[i+1], upsampled_r>=binsr[i])
            mask = np.logical_and(mask, target_cpu!=0)

            cos_theta = np.cos(np.arcsin(upsampled_r.copy()))
            cos_theta = cos_theta[mask]

            r_target = target_cpu.copy()
            r_target = r_target[mask]
            r_out = output_cpu.copy()
            r_out = r_out[mask]
            r_ups = upsampled_input.copy()
            r_ups = r_ups[mask]

            g_out = grad_mag_diff_out.clone().detach().cpu().numpy()
            g_out = g_out[mask]
            g_ups = grad_mag_diff_ups.clone().detach().cpu().numpy()
            g_ups = g_ups[mask]

            # Correlation plot
            H, _, _ = np.histogram2d(r_target, r_ups, bins=bins)
            hist_target_input[:,:,i] += H
            H = H*0

            H, _, _ = np.histogram2d(r_target, r_out, bins=bins)
            hist_target_output[:,:,i] += H
            H = H*0

            # Uncertainty Estimation
            H, _, _ = np.histogram2d(r_target-r_ups, r_ups, bins=bins)
            hist_target_input_Er[:,:,i] += H
            H = H*0

            H, _, _ = np.histogram2d(r_target-r_out, r_out, bins=bins)
            hist_target_output_Er[:,:,i] += H
            H = H*0

            # Magnitude of gradient difference
            H, _, _ = np.histogram2d(g_out, r_target, bins=bins)
            hist_target_output_GradA[:,:,i] += H
            H = H*0

            H, _, _ = np.histogram2d(g_ups, r_target, bins=bins)
            hist_target_input_GradA[:,:,i] += H
            H = H*0

            # Flux calculation
            H, _, _ = np.histogram2d((r_target-r_ups)/cos_theta, r_ups, bins=bins)
            hist_target_input_Flux[:,:,i] += H
            H = H*0
            H, _, _ = np.histogram2d((r_target-r_ups)/cos_theta/cos_theta, r_ups, bins=bins)
            hist_target_input_Flux2[:,:,i] += H
            H = H*0

            H, _, _ = np.histogram2d((r_target-r_out)/cos_theta, r_out, bins=bins)
            hist_target_output_Flux[:,:,i] += H
            H = H*0
            H, _, _ = np.histogram2d((r_target-r_out)/cos_theta/cos_theta, r_out, bins=bins)
            hist_target_output_Flux2[:,:,i] += H
            H = H*0


        if upscale_factor > 1:
            ###################
            # Calculating Super-res metrics
            ##################
            ## Mean absolute error given input
            target4b4 = np.moveaxis(target_cpu, 0, -1)
            target4b4 = image.extract_patches(target4b4, (upscale_factor, upscale_factor, target.shape[0]), extraction_step=(upscale_factor, upscale_factor, 1))[:, :, 0, :, :, :]

            out4b4 = np.moveaxis(output_cpu, 0, -1)
            out4b4 = image.extract_patches(out4b4, (upscale_factor, upscale_factor, target.shape[0]), extraction_step=(upscale_factor, upscale_factor, 1))[:, :, 0, :, :, :]

            ups4b4 = np.moveaxis(upsampled_input, 0, -1)
            ups4b4 = image.extract_patches(ups4b4, (upscale_factor, upscale_factor, target.shape[0]), extraction_step=(upscale_factor, upscale_factor, 1))[:, :, 0, :, :, :]

            input_shft = np.moveaxis(input.clone().detach().cpu().numpy()[:,0,:,:], 0, -1)* norm / hmi2f

            H, _, _ = np.histogram2d(np.mean(np.abs(out4b4-target4b4), axis=(2,3)).reshape(-1),
                                     input_shft.reshape(-1), bins=bins)
            hist_target_output_SR[:, :] += H

            H, _, _ = np.histogram2d(np.mean(np.abs(ups4b4-target4b4), axis=(2,3)).reshape(-1),
                                     input_shft.reshape(-1), bins=bins)
            hist_target_input_SR[:, :] += H


        #######################
        # Other metrics
        output_metrics, target_metrics = model.calculate_metrics(output * norm, target * norm)

        _, _, grad_sobel_out = grad_sobel.forward(output * norm)
        _, _, grad_sobel_tar = grad_sobel.forward(target * norm)
        grad_norm_out = torch.mean(torch.abs(grad_sobel_out), dim=(1, 2))
        grad_norm_tar = torch.mean(torch.abs(grad_sobel_tar), dim=(1, 2))
        output_metrics[grad_sobel.name] = grad_norm_out
        target_metrics[grad_sobel.name] = grad_norm_tar

        if len(target.shape) == 3:
            target = target[:, None, ...]
        if len(output.shape) == 3:
            output = output[:, None, ...]
        ssim_diff = ssim.forward(target, output)
        pnsr_diff = pnsr.forward(target, output)
        pearson_diff = pearson.forward(target, output)
        pearson_diff_10 = pearson_10.forward(target, output)

        for metric_name in val_metrics.keys():
            if metric_name == "SSIM":
                current_metric_diff = ssim_diff.mean()
                results_df.loc[np.arange(counter, counter + input.shape[0]), f'{metric_name}'] = ssim_diff.detach().cpu().numpy()

            elif metric_name == 'PNSR':
                current_metric_diff = pnsr_diff

            elif metric_name == 'PearsonCorrelation_15':
                current_metric_diff = pearson_diff_10.mean()
                results_df.loc[np.arange(counter, counter + input.shape[0]), f'{metric_name}'] = pearson_diff_10.detach().cpu().numpy()

            elif metric_name == 'PearsonCorrelation':
                current_metric_diff = pearson_diff.mean()
                results_df.loc[np.arange(counter, counter + input.shape[0]), f'{metric_name}'] = pearson_diff.detach().cpu().numpy()

            else:
                current_metric_diff = torch.mean(torch.abs(target_metrics[metric_name] - output_metrics[metric_name]))

                results_df.loc[np.arange(counter, counter + input.shape[0]), f'{metric_name}_target'] = target_metrics[metric_name].cpu().numpy()
                results_df.loc[np.arange(counter, counter + input.shape[0]), f'{metric_name}_output'] = output_metrics[metric_name].detach().cpu().numpy()

            val_metrics[metric_name] += (current_metric_diff * input.shape[0] / len(validation_loader.dataset)).item()

        results_df.loc[np.arange(counter, counter + input.shape[0]), 'average_radius_input'] = mean_r


        if double_sw:

            ups_output = ups_model.net.forward(torch.cat((output/ups_norm, input[:,1,:,:][:,None,:,:]),1))
            target_HR = norm * target_HR
            ups_output = ups_output * ups_norm

            H, _, _ = np.histogram2d(target_HR.view(-1).detach().cpu().numpy(),
                                     ups_output.view(-1).detach().cpu().numpy(), bins=bins)
            ups_hist_target_output += H

            ups_output_metrics, ups_target_metrics = ups_model.calculate_metrics(ups_output, target_HR)

            _, _, grad_sobel_out = grad_sobel.forward(ups_output)
            _, _, grad_sobel_tar = grad_sobel.forward(target_HR)
            grad_norm_out = torch.mean(torch.abs(grad_sobel_out), dim=(1, 2))
            grad_norm_tar = torch.mean(torch.abs(grad_sobel_tar), dim=(1, 2))
            ups_output_metrics[grad_sobel.name] = grad_norm_out
            ups_target_metrics[grad_sobel.name] = grad_norm_tar

            if len(target_HR.shape) == 3:
                target_HR = target_HR[:, None, ...]
            if len(ups_output.shape) == 3:
                ups_output = ups_output[:, None, ...]
            ssim_diff = ssim.forward(target_HR, ups_output)
            pnsr_diff = pnsr.forward(target_HR, ups_output)
            pearson_diff = pearson.forward(target_HR, ups_output)
            pearson_diff_10 = pearson_10.forward(target_HR, ups_output)

            for metric_name in val_metrics.keys():
                if metric_name == "SSIM":
                    current_metric_diff = ssim_diff.mean()
                    ups_results_df.loc[np.arange(counter, counter + output.shape[
                        0]), f'{metric_name}'] = ssim_diff.detach().cpu().numpy()

                elif metric_name == 'PNSR':
                    current_metric_diff = pnsr_diff

                elif metric_name == 'PearsonCorrelation_10':
                    current_metric_diff = pearson_diff_10.mean()
                    ups_results_df.loc[np.arange(counter, counter + ups_output.shape[
                        0]), f'{metric_name}'] = pearson_diff_10.detach().cpu().numpy()

                elif metric_name == 'PearsonCorrelation':
                    current_metric_diff = pearson_diff.mean()
                    ups_results_df.loc[np.arange(counter, counter + ups_output.shape[
                        0]), f'{metric_name}'] = pearson_diff.detach().cpu().numpy()

                else:
                    current_metric_diff = torch.mean(
                        torch.abs(ups_target_metrics[metric_name] - ups_output_metrics[metric_name]))

                    ups_results_df.loc[np.arange(counter, counter + ups_output.shape[0]), f'{metric_name}_target'] = \
                    ups_target_metrics[metric_name].cpu().numpy()
                    ups_results_df.loc[np.arange(counter, counter + ups_output.shape[0]), f'{metric_name}_output'] = \
                    ups_output_metrics[metric_name].detach().cpu().numpy()

                ups_val_metrics[metric_name] += (
                            current_metric_diff * ups_output.shape[0] / len(validation_loader.dataset)).item()

            ups_results_df.loc[np.arange(counter, counter + ups_output.shape[0]), 'average_radius_input'] = mean_r


        counter += len(input)



    results['metrics'] = val_metrics
    tend = perf_counter()
    logger.info(f'Finished evaluation took: {(tend - tstart)/60.0}m')

    # dump results in json file
    json_results = json.dumps(results)
    outfile = f'{logging_dir}/results_{type}.json'
    f = open(outfile, "w")
    f.write(json_results)
    f.close()

    # save index with corresponding metrics
    results_df.to_csv(f'{logging_dir}/results_mag_{type}.csv')

    # save histograms as numpy array
    np.save(f'{logging_dir}/histogram_target_input_{type}.npy', hist_target_input)
    np.save(f'{logging_dir}/histogram_target_output_{type}.npy', hist_target_output)

    np.save(f'{logging_dir}/histogram_target_input_Err_{type}.npy', hist_target_input_Er)
    np.save(f'{logging_dir}/histogram_target_output_Err_{type}.npy', hist_target_output_Er)

    np.save(f'{logging_dir}/histogram_target_input_Sh_{type}.npy', hist_target_input_Sh)
    np.save(f'{logging_dir}/histogram_target_output_Sh_{type}.npy', hist_target_output_Sh)

    np.save(f'{logging_dir}/histogram_target_input_Grad_{type}.npy', hist_target_input_GradA)
    np.save(f'{logging_dir}/histogram_target_output_Grad_{type}.npy', hist_target_output_GradA)

    np.save(f'{logging_dir}/histogram_target_input_Flux_{type}.npy', hist_target_input_Flux)
    np.save(f'{logging_dir}/histogram_target_input_Flux2_{type}.npy', hist_target_input_Flux2)
    np.save(f'{logging_dir}/histogram_target_output_Flux_{type}.npy', hist_target_output_Flux)
    np.save(f'{logging_dir}/histogram_target_output_Flux2_{type}.npy', hist_target_output_Flux2)

    if model.loss.name == 'SSIMGradHistLossUnc':
        np.save(f'{logging_dir}/histogram_uncertainty.npy', hist_error)


    if upscale_factor > 1:
        np.save(f'{logging_dir}/histogram_target_input_SR_{type}.npy', hist_target_input_SR)
        np.save(f'{logging_dir}/histogram_target_output_SR_{type}.npy', hist_target_output_SR)

    if double_sw:
        ups_results['metrics'] = ups_val_metrics
        np.save(f'{logging_dir}/ups_histogram_target_output_{type}.npy', ups_hist_target_output)

        # dump results in json file
        json_results = json.dumps(ups_results)
        outfile = f'{logging_dir}/ups_results_{type}.json'
        f = open(outfile, "w")
        f.write(json_results)
        f.close()

        # save index with corresponding metrics
        ups_results_df.to_csv(f'{logging_dir}/results_ups_mag_{type}.csv')

    # Add git info
    try:
        with open(f'{logging_dir}/gitinfo', mode='w') as file:
            git_data = f'branch: {repo.active_branch.name} \ncommit: {repo.head.commit}'
            file.write(git_data)
    except:
        pass

    if args.upload_results:
        # Copy to bucket
        res = subprocess.call(['gsutil', '-m', 'cp', '-r', logging_dir,
                               f'gs://{BUCKET}/inference'])
