"""
Main training module
"""
import argparse
import shutil
import os
import subprocess

import yaml
import datetime
import getpass
import torch
from time import perf_counter

import numpy as np
from google.cloud import storage
from tensorboardX.writer import SummaryWriter
from torch.utils.data.dataloader import DataLoader
from torchvision import transforms

from source.pipeline.dataset import GCSMagDataset, LocalMagDataset, RandomInversion,\
    RandomHorizontalFlip, RandomVerticalFlip
from source.pipeline.model_manager import BaseScaler
from source.utils import disable_warnings, get_logger, is_repo_clean, set_random_seed

from source.pipeline.models import BiCubic
from source.pipeline.losses import MSELoss, GradientLoss

INDEX_FILENAME = 'index.csv'

BUCKET = 'fdl-mag-experiments'

disable_warnings()

def _init_worker(worker):
    np.random.seed(seed)

if __name__ == "__main__":
    logger = get_logger(__name__)
    parser = argparse.ArgumentParser()
    parser.add_argument('--upload_results', action='store_true')
    parser.add_argument('--augment', action='store_true')
    parser.add_argument('--target_align', action='store_true')
    parser.add_argument('--config_path', required=True)
    parser.add_argument('--run', required=False)
    parser.add_argument('--epoch', required=False)
    parser.add_argument('--seed', required=False)
    parser.add_argument('--self', action='store_true')

    args = parser.parse_args()

    if shutil.which('gsutil') is None:
        raise EnvironmentError(f'gsutil not found on path')

    repo = is_repo_clean('.')

    logger.info(f'Starting training run with {args}')
    tstart = perf_counter()

    if args.seed is not None:
        seed = set_random_seed(int(args.seed))
        logger.info(f'Initializing with random seed {seed}')
    else:
        seed = set_random_seed()

    with open(args.config_path, 'r') as stream:
        config_data = yaml.load(stream, Loader=yaml.SafeLoader)

    data_config = config_data['data']
    rootdir = data_config['data_bucket']
    data_folder = data_config['data_folder']

    if 'n_frames' in config_data['net'].keys():
        nframe = int(config_data['net']['n_frames'])
    else:
        nframe =1

    index_range = None
    if 'index_range' in data_config.keys():
        start, stop = data_config['index_range']
        index_range = np.arange(start, stop)

    norm = None
    if 'normalisation' in data_config.keys():
        norm = data_config['normalisation']

    upscale_factor = None
    if 'upscale_factor' in config_data['net'].keys():
        upscale_factor = config_data['net']['upscale_factor']

    self_downscale = None
    if 'self_downscale' in config_data['net'].keys():
        self_downscale = config_data['net']['self_downscale']

    asinh = None
    if 'asinh' in data_config.keys():
        asinh = data_config['asinh']

    data_transform = None
    if args.augment:
        data_transform = transforms.Compose([
            RandomHorizontalFlip(),
            RandomVerticalFlip(),
            # RandomInversion()
        ])

    num_workers = 0
    if rootdir.startswith('gs://') or rootdir.startswith('gcs://'):
        gcs_storage_client = storage.Client()
        buckname = rootdir.split('//')[-1]
        dset_train = GCSMagDataset(INDEX_FILENAME, buckname, '{}/train'.format(data_folder),
                                   gcs_storage_client, index_range, norm_factor=norm, nframe=nframe,
                                   transform=data_transform, target_align=args.target_align, asinh=asinh,
                                   upscale_factor=upscale_factor)
        dset_test = GCSMagDataset(INDEX_FILENAME, buckname, '{}/test'.format(data_folder),
                                  gcs_storage_client, index_range, norm_factor=norm, nframe=nframe,
                                  transform=data_transform, target_align=args.target_align, asinh=asinh,
                                  upscale_factor=upscale_factor)
        dset_val = GCSMagDataset(INDEX_FILENAME, buckname, '{}/validate'.format(data_folder),
                                 gcs_storage_client, index_range, norm_factor=norm, nframe=nframe,
                                 transform=data_transform, target_align=args.target_align, asinh=asinh,
                                 upscale_factor=upscale_factor)
    else:
        num_workers = os.cpu_count() - 1

        dset_trainP = LocalMagDataset(INDEX_FILENAME, rootdir, '{}/train'.format(data_folder),
                                     index_range, norm_factor=norm, nframe=nframe,
                                     transform=data_transform, target_align=args.target_align, asinh=asinh,
                                     upscale_factor=upscale_factor, target_downscale=self_downscale)
        dset_trainN = LocalMagDataset(INDEX_FILENAME, rootdir, '{}/train'.format(data_folder),
                                     index_range, norm_factor=-norm, nframe=nframe,
                                     transform=data_transform, target_align=args.target_align, asinh=asinh,
                                     upscale_factor=upscale_factor, target_downscale=self_downscale)
        dset_train = torch.utils.data.ConcatDataset([dset_trainP, dset_trainN])

        dset_testP = LocalMagDataset(INDEX_FILENAME, rootdir, '{}/test'.format(data_folder),
                                    index_range, norm_factor=norm, nframe=nframe,
                                    transform=data_transform, target_align=args.target_align, asinh=asinh,
                                    upscale_factor=upscale_factor, target_downscale=self_downscale)
        dset_testN = LocalMagDataset(INDEX_FILENAME, rootdir, '{}/test'.format(data_folder),
                                    index_range, norm_factor=-norm, nframe=nframe,
                                    transform=data_transform, target_align=args.target_align, asinh=asinh,
                                    upscale_factor=upscale_factor, target_downscale=self_downscale)
        dset_test = torch.utils.data.ConcatDataset([dset_testP, dset_testN])

        dset_valP = LocalMagDataset(INDEX_FILENAME, rootdir, '{}/validate'.format(data_folder),
                                   index_range, norm_factor=norm, nframe=nframe,
                                   transform=data_transform, target_align=args.target_align, asinh=asinh,
                                   upscale_factor=upscale_factor, target_downscale=self_downscale)
        dset_valN = LocalMagDataset(INDEX_FILENAME, rootdir, '{}/validate'.format(data_folder),
                                   index_range, norm_factor=-norm, nframe=nframe,
                                   transform=data_transform, target_align=args.target_align, asinh=asinh,
                                   upscale_factor=upscale_factor, target_downscale=self_downscale)
        dset_val = torch.utils.data.ConcatDataset([dset_valP, dset_valN])

    train_loader = DataLoader(dset_train, batch_size=config_data['batch_size'], shuffle=True,
                              num_workers=num_workers, worker_init_fn=_init_worker)
    test_loader = DataLoader(dset_test, batch_size=config_data['batch_size'],
                             num_workers=num_workers)
    val_loader = DataLoader(dset_val, batch_size=config_data['batch_size'],
                            num_workers=num_workers)

    logger.info("Building Model")
    model = BaseScaler.from_dict(config_data)

    logger.info(f'Number of model parameters: '
                f'{np.sum([p.numel() for p in model.net.parameters()])}')

    tstamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

    user = getpass.getuser()

    comment = config_data.get('comment', '')
    comment = comment.replace(' ', '_')

    logging_dir = f'summaries/{data_folder}_{tstamp}_{model.net.name}_' \
                  f'{model.loss.name}_{model.batch_size}_' \
                  f'{model.learning_rate}_{comment}'

    logger.info(f'Summary logging dir: {logging_dir}')

    chkpt_dir = f'checkpoints/{data_folder}_{tstamp}_{model.net.name}_' \
                f'{model.loss.name}_{model.batch_size}_' \
                f'{model.learning_rate}_{comment}'

    if args.epoch is not None and args.run is not None:
        gcs_storage_client = storage.Client()
        bucket = gcs_storage_client.bucket('fdl-mag-experiments')
        blob = bucket.blob(f'checkpoints/{args.run}/{args.epoch}')

        if not os.path.exists(f'checkpoints/{args.run}'):
            logger.info(f'Creating checkpoint folder: checkpoints/{args.run}')
            os.makedirs(f'checkpoints/{args.run}')
        if not os.path.exists(f'checkpoints/{args.run}/{args.epoch}'):
            logger.info(f'Downloading checkpoint: {args.epoch}')
            file = blob.download_to_filename(f'checkpoints/{args.run}/{args.epoch}')

        checkpoint = torch.load(f'checkpoints/{args.run}/{args.epoch}', map_location='cuda')
        logger.info(f'Loading Model: fdl-mag-experiments/checkpoints/{args.run}/{args.epoch}')

        try:

            try:
                model.net.load_state_dict(checkpoint['model_state_dict'])

            except:
                state_dict = {}
                for key, value in checkpoint['model_state_dict'].items():
                    state_dict['.'.join(key.split('.')[1:])] = value
                model.net.load_state_dict(state_dict)

        except:
            state_dict = {}
            for key, value in checkpoint['model_state_dict'].items():
                state_dict['.'.join(np.append(['module'], key.split('.')[0:]))] = value
            model.net.load_state_dict(state_dict)


    if ~os.path.exists(chkpt_dir):
        os.makedirs(chkpt_dir)

    logger.info(f'Checkpoint dir: {chkpt_dir}')

    writer = SummaryWriter(logging_dir)
    model.train(train_loader, val_loader, writer, chkpt_dir)

    tend = perf_counter()
    logger.info(f'Finished training took: {(tend - tstart)/60.0}m')

    mean_eval = model.evaluation(test_loader, writer, logging_dir)

    writer.close()

    logger.info(f'Mean eval {mean_eval}')

    # Add git info
    with open(f'{logging_dir}/gitinfo', mode='w') as file:
        git_data = f'branch: {repo.active_branch.name} \ncommit: {repo.head.commit}'
        file.write(git_data)

    if args.upload_results:
        # Copy to bucket
        log = subprocess.call(['gsutil', '-m', 'cp', '-r', 'run.log',
                               f'gs://{BUCKET}/{logging_dir}/'])
        res = subprocess.call(['gsutil', '-m', 'cp', '-r', logging_dir,
                               f'gs://{BUCKET}/summaries'])
        chkpt = subprocess.call(['gsutil', '-m', 'cp', '-r', chkpt_dir,
                                 f'gs://{BUCKET}/{chkpt_dir}'])
