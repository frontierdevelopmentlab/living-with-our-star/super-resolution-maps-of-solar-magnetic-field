"""
Common low level utility functions
"""

import logging
import os
import random
import sys
import time
import warnings

import numpy as np
import pandas as pd
import torch
from git import Repo
from google.cloud import storage

from astropy.coordinates import SkyCoord
from sunpy.map import Map
from skimage.transform import resize
import astropy.units as u


def disable_warnings():
    """
    Disable printing of warnings

    Returns
    -------
    None
    """
    warnings.simplefilter("ignore")


def get_logger(name):
    """
    Return a logger for current module
    Returns
    -------

    logger : logger instance

    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter(fmt="%(asctime)s %(levelname)s %(name)s: %(message)s",
                                  datefmt="%Y-%m-%d - %H:%M:%S")
    console = logging.StreamHandler(sys.stdout)
    console.setLevel(logging.DEBUG)
    console.setFormatter(formatter)

    logfile = logging.FileHandler('run.log', 'w')
    logfile.setLevel(logging.DEBUG)
    logfile.setFormatter(formatter)

    logger.addHandler(console)
    logger.addHandler(logfile)

    return logger


def is_repo_clean(path):
    """
    Return a git repo object if clean otherwise raise error
    Parameters
    ----------
    path : str
        Path to repo
    Returns
    -------
    `git.Repo` : Git repo object
    """
    repo = Repo(path)

    # if repo.active_branch.name != 'master':
    #     raise EnvironmentError(f'Git repo on {repo.active_branch.name} not master')
    #
    # if repo.is_dirty():
    #     modified_files = [item.a_path for item in repo.index.diff(None)]
    #     raise EnvironmentError(f'The git repo is not clean: {modified_files}')
    #
    # if repo.untracked_files:
    #     raise EnvironmentError(f'The git repo has un-tracked files: {repo.untracked_files}')
    #
    # if list(repo.iter_commits('origin/master..master')):
    #     raise EnvironmentError(
    #         f'The repo has unpushed commits: '
    #         f'{[c.name_rev for c in repo.iter_commits("origin/master..master")]}')

    return repo

def file_exist(bucketname, filename):

    """
    Check whether a file exists in a bucket

    Parameters
    ----------
    bucketname: string
        name of bucket or file to check file existence

    filename: string
        name of file to check existence

    client:
        google.storage.Client()

    Returns
    -------
    bool: True iff the file exist

    """

    if 'gs://' in bucketname:
        client = storage.Client()
        bucket = client.bucket(bucketname)
        blob = bucket.blob(filename)

        # this is check if file exist or not
        stats = blob.exists(client)

    else:
        stats = os.path.exists(os.path.join(bucketname, filename))

    return stats

def upload_index(data, destination, filename):

    """
    Upload a pandas dataframe with metrics and an
    index for each file in destination

    Parameters
    ----------
    data: pandas dataframe
        metrics, date and filename
    destination: string
        name of the folder destination to load index file

    Returns
    -------

    """

    if file_exist(destination, '{}.csv'.format(filename)):
        index_df = pd.read_csv('{}/{}.csv'.format(destination, filename),
                               index_col='index')
        index_df['dateobs'] = pd.to_datetime(index_df['dateobs'])
        data = pd.concat([data, index_df]).drop_duplicates('dateobs')

    data.sort_values(by='dateobs', inplace=True)
    data.set_index(np.arange(len(data)), inplace=True)
    data.index.name = 'index'
    data.dropna(inplace=True)

    data.to_csv('{}/{}.csv'.format(destination, filename))


def set_random_seed(seed=None):
    if seed is None:
        seed = int((time.time()*1e6) % 1e8)

    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)

    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)  # if you are using multi-GPU.
        torch.backends.cudnn.benchmark = False
        torch.backends.cudnn.deterministic = True

    return seed


def crop(amap, size, target_plate=0.504273):
    """
    Crop map to from disk center by arcsecond matching dim

    Parameters
    ----------
    amap: sunpy map

    size: float
        width of the cropping patch

    Returns
    -------
    crop_map: cropped map to dimension x dimension from disk center

    """

    width_arcsec = size * u.arcsec * target_plate
    height_arcsec = size * u.arcsec * target_plate

    bottom_left = SkyCoord(- width_arcsec / 2, - height_arcsec / 2, frame=amap.coordinate_frame)
    top_right = SkyCoord(width_arcsec / 2, height_arcsec / 2, frame=amap.coordinate_frame)

    # crop the center of the sun (check the projection)
    crop_map = amap.submap(bottom_left, top_right)

    return crop_map

def downsample(amap, scale):
    """
    Downsample map by scale factor

    Parameters
    ----------
    amap: sunpy map

    scale: integer scale factor

    Returns
    -------
    amap_downscaled: downscaled map

    """

    amap_data = amap.data
    amap_downscaled = resize(amap_data, (amap_data.shape[0] / scale, amap_data.shape[1] / scale),
                             anti_aliasing=True)

    # update headers image scale and number of pixels
    new_meta = amap.meta.copy()

    scale_factor_x = scale
    scale_factor_y = scale


    # Update metadata
    new_meta['cdelt1'] *= scale_factor_x
    new_meta['cdelt2'] *= scale_factor_y

    if 'CD1_1' in new_meta:
        new_meta['CD1_1'] *= scale_factor_x
        new_meta['CD2_1'] *= scale_factor_x
        new_meta['CD1_2'] *= scale_factor_y
        new_meta['CD2_2'] *= scale_factor_y

    new_meta['crpix1'] = (amap_data.shape[0] / scale + 1)/ 2.
    new_meta['crpix2'] = (amap_data.shape[1] / scale + 1)/ 2.

    center = amap.center

    new_meta['crval1'] = center.Tx.value
    new_meta['crval2'] = center.Ty.value

    return Map(amap_downscaled, new_meta)


def create_hist_bins(dl=0.02, ax_lim=3000, noise_level=0.2):
    """
    Generate bins for 2d histograms
    :param dl:
    :param ax_lim:
    :param noise_level:
    :return:
    """

    lim = np.log10(ax_lim)
    bins = np.round(np.power(10, np.arange(1, lim + dl, dl)), 2)
    bins = bins - 10 + noise_level
    bins = np.append(np.flip(-(bins)), bins)

    return bins

