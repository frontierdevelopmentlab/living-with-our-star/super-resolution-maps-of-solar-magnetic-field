import numpy as np
import torch
import torch.nn as nn

from source.utils import get_logger
from source.utils_uncertainty import *

logger = get_logger(__name__)

class EpistemicUncertainty(nn.Module):
    """
    Implement methods to compute epistemic uncertainty for a model with last
    layer conv layer and dropout
    """
    def __init__(self, model, p):
        super().__init__()

        self.p = p
        self.model = model

        self.activation = {}
        model.decode.deconv[1].register_forward_hook(self.get_layer('pre_activation'))
        model.decode.deconv[3].register_forward_hook(self.get_layer('post_conv'))

    def forward(self, x):
        """
        Compute an approximation of the variance and mean of the
        posterior distribution obtained by mc dropout on the last
        deconvolution layer of the model
        :param x: (B, C, W, H)
        :return: (B), (B)
        """

        weights_conv = self.model.decode.deconv[3].weight
        bias_conv = self.model.decode.deconv[3].bias
        prelu_param = self.model.decode.deconv[4].weight
        weights_final = self.model.decode.final[1].weight
        bias_final = self.model.decode.final[1].bias

        self.model.forward(x)
        pre_activation = self.activation['pre_activation']

        variance = pre_activation ** 2 * self.p / (1 - self.p)
        conv2_mean = nn.functional.conv2d(pre_activation, weights_conv, bias=bias_conv)
        conv2_var = nn.functional.conv2d(variance, weights_conv ** 2)

        prelu_var = self.prelu_derivative(conv2_mean, prelu_param) ** 2 * conv2_var
        prelu_mean = nn.functional.prelu(conv2_mean, prelu_param)

        prelu_var = prelu_var / (1 - self.p) + self.p / (1 - self.p) * prelu_mean ** 2
        out_var = nn.functional.conv2d(prelu_var, weights_final ** 2)

        out_mean = nn.functional.conv2d( prelu_mean, weights_final, bias=bias_final)

        return out_mean[:, 0, ...], out_var[:, 0, ...]


    def get_layer(self, name):
        """
        Create a hook to find the value of a particular layer
        after a forward pass
        :param name: string
        :return: hook function
        """
        def hook(model, input, output):
            self.activation[name] = output.detach()

        return hook

    def prelu_derivative(self, x, param):
        """
        Compute the jacobian of a prelu activation
        :param x:
        :return:
        """
        mask_smaller_than_zero = (x < 0).float()
        mask_greater_than_zero = (x>= 0).float()

        return param * mask_smaller_than_zero + mask_greater_than_zero


class SubspaceUncertainty(object):
    """
    Implement a cyclical stochastic gradient descent to collect
    SGD iterates and compute a subspace of the resulting variance-covariance
    matrix for each basin of attraction.
    Weights are then sampled from a mixture of normal distributions where
    each gaussian corresponds to a quadratic approximation of the basin of attraction
    obtained within a cycle
    """

    def __init__(self, net, loss, learning_rate, device='cpu', t=20000, m=5, beta=0.6):
        self.net = net.to(device)
        self.loss = loss
        self.cycle = np.ceil(t / m)
        self.t = t
        self.beta = beta
        self.lr_0 = learning_rate
        self.device = device

        self.optimizer = torch.optim.SGD(list(self.net.parameters()), lr=learning_rate,
                                         momentum=0.0, weight_decay=0.00001)

    def optimize(self, x, y):
        self.optimizer.zero_grad()
        output = self.net.forward(x)
        loss = self.loss(y, output)

        loss.backward()
        self.optimizer.step()

        return loss

    def adjust_learning_rate(self, iteration):
        iter_mod = iteration % self.cycle
        lr = self.lr_0 / 2 * (np.cos(np.pi * iter_mod / self.cycle) + 1)

        for param_group in self.optimizer.param_groups:
            param_group['lr'] = lr

        self.learning_schedule.append(lr)

    def get_projections_pca(self, params, components=20):

        U, S, V = torch.svd(params, some=False)
        projection = S[:components, None] * V[:components, ...] / V.shape[0]

        return projection.detach()

    def train(self, data_loader, nepochs, components=10, subspace_component=10):

        count = 0
        self.projections_pca = {}
        log_likelihood_cycle = {}
        self.params_mean = {}
        cycle_number = 0
        parameters_cycle = []

        log_likelihood_cycle[cycle_number] = []
        self.learning_schedule = []

        d = len(extract_parameters(self.net))
        parameters_proj = torch.empty((0, d)).to(self.device)
        params_mean = torch.zeros(d).to(self.device)
        nmodel = 0

        for epoch in range(nepochs):

            if count > self.t:
                break

            train_loss = 0
            for batch in data_loader:

                if count > self.t:
                    break

                x = batch['x'].to(self.device)
                y = batch['y'].to(self.device)

                iter_cycle = count / self.cycle - cycle_number

                if iter_cycle <= self.beta:
                    self.adjust_learning_rate(count)

                loss = self.optimize(x, y)
                train_loss += loss.detach().cpu() * len(x) / len(data_loader.dataset)

                if iter_cycle > self.beta:
                    parameters_cycle.append(extract_parameters(self.net))
                    log_likelihood_cycle[cycle_number].append(loss.detach())

                    if len(parameters_cycle) + parameters_proj.shape[0] >= 2 * components:
                        params = torch.stack(parameters_cycle).detach()
                        params_mean = (params.mean(0) * params.shape[0] + params_mean * nmodel) / \
                                      (nmodel + params.shape[0])
                        nmodel += params.shape[0]
                        params -= params_mean

                        params = torch.cat([params, parameters_proj], 0)
                        parameters_proj = self.get_projections_pca(params,
                                                                    components=components)

                        parameters_cycle = []

                count += 1

                if count % self.cycle == 0:
                    self.projections_pca[cycle_number] = torch.t(parameters_proj[:subspace_component, ...])
                    self.params_mean[cycle_number] = params_mean
                    cycle_number += 1
                    parameters_cycle = []
                    log_likelihood_cycle[cycle_number] = []
                    nmodel = 0
                    params_mean = torch.zeros(d)

            if count % 100 == 0:
                logger.info(f'Train  loss is {train_loss}')

        self.weights_cycle = np.zeros(len(self.params_mean))

        for iter_number, loglikelihood_list in log_likelihood_cycle.items():
            if len(loglikelihood_list) < 1:
                continue
            loss = torch.stack(loglikelihood_list)
            self.weights_cycle[iter_number] = loss.mean().detach().cpu().numpy()

        self.weights_cycle /= self.weights_cycle.sum()

    def sample_trajectory_pca(self, x, n=5):

        d = self.projections_pca[0].shape[-1]
        trajectory = np.zeros((n, x.shape[0]))
        x = torch.from_numpy(x).float()

        for i in range(n):
            idx = np.random.choice(np.arange(self.weights_cycle.shape[0]), 1, p=self.weights_cycle)[0]
            eps = torch.randn(d)
            param = self.params_mean[idx][..., None] + torch.matmul(self.projections_pca[idx], eps[..., None])
            set_weights(self.net, param)

            out = self.net.forward(x)
            trajectory[i, :] = out.detach().cpu().numpy()

        return trajectory