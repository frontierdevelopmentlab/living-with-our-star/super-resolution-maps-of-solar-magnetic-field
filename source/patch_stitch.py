import math
import os
import argparse
import pandas as pd
import numpy as np
import astropy.units as u

from sklearn.feature_extraction import image
from sunpy.map import all_pixel_indices_from_map
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor, wait

def get_patch(amap, size, stride=None, position=False):
    """
    create patches of dimension size * size with a defined stride.
    If the stride is equals to size, there is no overlap

    Parameters
    ----------
    amap: sunpy map

    size: integer
        size of each patch

    Keywords
    --------
    stride: integer
        Size of the stride

    Returns
    -------
    numpy array [num_patches, num_channel, size, size]
        channels are magnetic field, radial distance relative to radius

    """
    if not stride:
        stride = size

    x, y = np.meshgrid(*[np.arange(v.value) for v in amap.dimensions]) * u.pixel
    hpc_coords = amap.pixel_to_world(x, y)
    array_radius = np.sqrt(hpc_coords.Tx ** 2 + hpc_coords.Ty ** 2) / amap.rsun_obs

    patches = image.extract_patches(amap.data, (size, size), extraction_step=stride)
    patches = patches.reshape([-1] + list((size, size)))

    patches_r = image.extract_patches(array_radius, (size, size), extraction_step=stride)
    patches_r = patches_r.reshape([-1] + list((size, size)))

    if position:
        patches_position_x = image.extract_patches(all_pixel_indices_from_map(amap)[0, :, :], (size, size),
                                                   extraction_step=stride)
        patches_position_y = image.extract_patches(all_pixel_indices_from_map(amap)[1, :, :], (size, size),
                                                   extraction_step=stride)
        patches_position_x = patches_position_x.reshape([-1] + list((size, size)))
        patches_position_y = patches_position_y.reshape([-1] + list((size, size)))
        return np.stack([patches, patches_r, patches_position_x, patches_position_y], axis=1)

    return np.stack([patches, patches_r], axis=1)


def get_img_from_patch(tensor):
    """
    Creates full images from non-overlapping patches

    Parameters
    ---------- 
    tensor: tensor of shape [batch, width, height] or [batch, n_channels, width, height]
            Batch refers to the number of patches, which should be the square of the number of patches in x,y

    Returns
    -------
    numpy array [sqrt(batch size), sqrt(batchsize)]: full image
    """ 
    
    if len(tensor.shape) == 4:
        tensor = tensor[:,0,:,:]
    
    number = int(math.sqrt(tensor.shape[0])) # to go from #batches to image size
    
    array = tensor[0] # Initialize array
    
    for row in range(0,number):
        
        if row == 0:
            for col in range(1, number):
                array = np.concatenate((array, tensor[col]), axis = 1)
            
        else:
            new_row = tensor[row * number]
            for col in range(1, number):
                new_row = np.concatenate((new_row, tensor[row * number + col]), axis = 1)
            array = np.concatenate((array, new_row), axis =0)
    
    return array

def run_extraction(idx, filename_source, filename_target, date, origin, destination, remove=False):

    if not os.path.exists('{}/{}'.format(origin, filename_source)):
        return pd.DataFrame()

    index_expanded = pd.DataFrame()
    source_tensor = np.load('{}/{}'.format(origin, filename_source))
    target_tensor = np.load('{}/{}'.format(origin, filename_target))

    for i in range(source_tensor['arr_0'].shape[0]):
        path_source = filename_source.replace('.npz', '_{}.npy'.format(i))
        path_target = filename_target.replace('.npz', '_{}.npy'.format(i))
        arr_source = source_tensor['arr_0'][i]
        arr_target = target_tensor['arr_0'][i]
        np.save('{}/{}'.format(destination, path_source), arr_source)
        np.save('{}/{}'.format(destination, path_target), arr_target)

        index_expanded.loc[i, 'dateobs'] = date
        index_expanded.loc[i, 'patchnum'] = i
        index_expanded.loc[i, 'filename_source'] = path_source
        index_expanded.loc[i, 'filename_target'] = path_target

    if remove:
        os.remove('{}/{}'.format(origin, filename_source))
        os.remove('{}/{}'.format(origin, filename_target))

    return index_expanded


def get_tensor_from_patches(origin, destination, remove=False, nworkers=4):

    if not os.path.exists(destination):
        os.mkdir(destination)

    index_df = pd.read_csv('{}/index.csv'.format(origin))
    index_df.sort_values(by='dateobs', inplace=True)

    futures = []

    with ProcessPoolExecutor(max_workers=nworkers) as executor:
        for idx in index_df.index:
            filename_source = index_df.loc[idx, 'filename_source']
            filename_target = index_df.loc[idx, 'filename_target']
            date = index_df.loc[idx, 'dateobs']

            futures.append(executor.submit(run_extraction,
                                           idx,
                                           filename_source,
                                           filename_target,
                                           date,
                                           origin,
                                           destination,
                                           remove=remove))
    wait(futures)
    index_final = pd.concat([f.result()[0] for f in futures if len(f.result()) > 0], sort=True)
    index_final.set_index(np.arange(len(index_final)), inplace=True)
    index_final.index.name = 'index'

    index_final.to_csv('{}/index.csv'.format(destination))


def get_numpy_array(index_df, origin,  remove=False):
    files_to_export = []

    index_df.sort_values(by='dateobs', inplace=True)

    for idx in index_df.index:
        filename_source = index_df.loc[idx, 'filename_source']
        filename_target = index_df.loc[idx, 'filename_target']
        year = index_df.loc[idx, 'year']

        try:
            source_tensor = np.load('{}/{}'.format(origin, filename_source))
            target_tensor = np.load('{}/{}'.format(origin, filename_target))

            source_list = list(source_tensor['arr_0'])
            target_list = list(target_tensor['arr_0'])

            year_list = [year] * len(source_list) * 2

            path_source_list = [filename_source.replace('.npz', '')] * len(source_list)
            path_target_list = [filename_target.replace('.npz', '')] * len(source_list)

            files_to_export_to_add = zip(source_list, path_source_list, target_list, path_target_list, year_list)
            files_to_export += list(enumerate(files_to_export_to_add))
        
        # for i in range(source_tensor['arr_0'].shape[0]):
        #     path_source = filename_source.replace('.npz', '_{}.npy'.format(i))
        #     path_target = filename_target.replace('.npz', '_{}.npy'.format(i))
        #     arr_source = source_tensor['arr_0'][i]
        #     arr_target = target_tensor['arr_0'][i]
        #
        #     files_to_export.append((path_source, arr_source, path_target, arr_target, i))

            if remove:
                os.remove('{}/{}'.format(origin, filename_source))
                os.remove('{}/{}'.format(origin, filename_target))

        except Exception:
            continue

    return files_to_export

def save_process(file_array, destination):

    arr_source, path_source, arr_target, path_target, year = file_array[1]
    i = file_array[0]

    path_source = '{}_{}.npy'.format(path_source, i)
    path_target = '{}_{}.npy'.format(path_target, i)

    destination = '{}/{}'.format(destination, year)

    if np.all(arr_source == 0) & np.all(arr_target == 0):
        return {}

    np.save('{}/{}'.format(destination, path_source), arr_source)
    np.save('{}/{}'.format(destination, path_target), arr_target)

    file_info = {'filename_source': '{}/{}'.format(year, path_source),
                 'filename_target': '{}/{}'.format(year, path_target), 'patchnum': i}

    return file_info

def save_array(origin, destination, year_start, year_end, nworkers=4, chunksize=100, remove=False):

    for year in range(year_start, year_end + 1):
        os.makedirs('{}/{}'.format(destination, year), exist_ok=True)

    index_df = pd.read_csv('{}/index.csv'.format(origin))
    index_df['dateobs'] = pd.to_datetime(index_df['dateobs'])
    index_df['year'] = index_df.dateobs.apply(lambda x: x.year)

    index_df = index_df[(index_df.year >= year_start) & (index_df.year <= year_end)]

    index_list = []

    for _,index_chunk in index_df.groupby(np.arange(len(index_df)) // chunksize):
        files_to_export = get_numpy_array(index_chunk, origin, remove=remove)

        futures = []

        if len(files_to_export) == 0:
            continue

        with ProcessPoolExecutor(max_workers=nworkers) as executor:
            for file_array in files_to_export:
                futures.append(executor.submit(save_process, file_array, destination))

        wait(futures)

        results = [f.result() for f in futures]

        index_expanded = pd.DataFrame(results)
        index_list.append(index_expanded)

    index_final = pd.concat(index_list)
    index_final.set_index(np.arange(len(index_final)), inplace=True)
    index_final.index.name = 'index'

    index_final.to_csv('{}/index.csv'.format(destination))


if __name__ == '__main__':


    parser = argparse.ArgumentParser()
    parser.add_argument('--origin')
    parser.add_argument('--destination')
    parser.add_argument('--year_start')
    parser.add_argument('--year_end')
    parser.add_argument('--chunksize')
    parser.add_argument('--nworkers')

    args = parser.parse_args()

    print("You are running the script with arguments: ")
    for a in args.__dict__:
        print(str(a) + ": " + str(args.__dict__[a]))

    kwargs_dict = {
                   'nworkers': int(args.nworkers),
                   'chunksize': int(args.chunksize),
                   }

    save_array(args.origin,
               args.destination,
               int(args.year_start),
               int(args.year_end),
           **kwargs_dict)
