"""
Data pre-processing pipeline
"""
import argparse
import datetime
import os
import random
import numpy as np
from collections.abc import Iterable
from concurrent.futures import ProcessPoolExecutor, wait

import astropy.units as u
import dask.bytes
import pandas as pd
from astropy.io.fits import CompImageHDU
from google.cloud import storage
from pandas import Grouper
from skimage.transform import resize

from source.metrics import *
from source.prep import map_prep, scale_rotate
from source.utils import file_exist, upload_index

BUCKET_MAP = {'kp512': 'fdl-mag-512',
              'GONG': 'fdl-mag-gong',
              'MDI': 'fdl-mag-mdi',
              'MDI-NEW': 'fdl-mag-mdi',
              'MWO': 'fdl-mag-mt-wilson',
              'HMI': 'fdl-mag-sdo',
              'SPMG': 'fdl-mag-spmg',
              'SOLIS': 'fdl-mag-solis',
              'SOT': 'fdl-mag-sot'}

TARGET_FACTOR = {'HMI': 1.0, 'GONG': 4.0, 'MDI': 4.0, 'MDI-NEW': 4.0, 'SPMG': 2.0, 'kp512': 2.0, 'MWO': 16.0}


def get_files_from_index(index_filepath,
                         year_start,
                         year_end,
                         month_start,
                         month_end,
                         instrument='HMI'):
    """
    Get the file system for any instrument using the data_index.csv

    Parameters
    ----------
    instrument
    index_filepath: string
        path to the index file. If starts by "gs://" will get files from google cloud
    year_start: year start reading the magnetograms

    year_end: year end reading the magnetograms

    month_start: month start reading the magnetograms

    month_end: month end reading the magnetograms

    Returns
    -------
    a list of dask type files object

    """

    index_df = pd.read_csv(index_filepath)

    # select instrument and files that open
    index_df = index_df[index_df.instrument == instrument]
    index_df = index_df[index_df.did_not_open == 0]

    # select time range
    index_df['dateobs'] = pd.to_datetime(index_df['dateobs'])
    date_start = datetime.date(year_start, month_start, 1)
    if month_end < 12:
        date_end = datetime.date(year_end, month_end + 1, 1)
    else:
        date_end = datetime.date(year_end + 1, 1, 1)

    index_df = index_df.loc[(index_df.dateobs < date_end), :]
    index_df = index_df.loc[(index_df.dateobs >= date_start), :]

    path_list = list(index_df.filepath)

    if instrument == 'GONG':
        return dask.bytes.open_files(path_list, compression='gzip')

    return dask.bytes.open_files(path_list)

def flatten(iterable):
    """

    Parameters
    ----------
    iterable

    Returns
    -------
    a flat list of items

    """
    flat = []
    for item in iterable:
        if isinstance(item, Iterable):
            flat += [x for x in item]
        else:
            flat.append(item)

    return flat


def flatten_dict(iterable):

    """
    Transform a list of dicitonary into one flat dictionary

    Parameters
    ----------
    iterable: list
        list of dictionaries to flatten

    Returns
    -------
    dict: dictionary
        flat dictionary
    """
    output_dict = {}
    for d in iterable:
        for key, value in d.items():
            output_dict[key] = value

    return output_dict

def run_processes(amap, processes):
    """
    Run processes on the pre-processed map where processes
    are functions defined in `source.metrics`

    Parameters
    ----------
    amap : `sunpy.Map`
        Input map
    processes : list
        Name of functions to run

    Returns
    -------
    list : Extracted metrics
    """
    outputs = []

    for proc in processes:
        outputs.append(proc(amap))

    return outputs


def upload(amap, destination, client=None, instrument='HMI'):
    """
    Upload a sunpy map as a numpy as array into the bucket destination/folder

    Parameters
    ----------
    amap: sunpy.Map
        a map with a date and data attribute

    destination: string
        destination bucket name

    client:
        google.storage.Client() if loading to the cloud

    instrument: string
        magnetogram instrument as key of BUCKETNAME

    Returns
    -------

    """

    date = amap.date.datetime
    filename = '{}_{}.fits'.format(instrument, date.strftime("%Y%m%d-%H%M%S"))

    if client is not None:
        destination_bucket = client.get_bucket(destination)
        amap.save(filename, hdu_type=CompImageHDU)

        blob = destination_bucket.blob(filename)
        blob.upload_from_filename(filename)
        os.remove(filename)

    else:
        file = '{}/{}'.format(destination, filename)
        if os.path.exists(file):
            os.remove(file)
        amap.save(file, hdu_type=CompImageHDU)


def data_pipeline(index_filepath, destination, year_start, year_end, month_start, month_end,
                  instrument='HMI',
                  processes=None,
                  nworkers=4,
                  out=0):
    """
    Process data for instrument for the specified time
    range by applying processes to each
    magnetograms.
    Optionally, upload the modified magnetograms

    Parameters
    ----------
    index_filepath
    year_start: integer
        start (year) of time range

    year_end: integer
        end (year) of time range

    month_start: integer
        start (month) of time range

    month_end: integer
        end (month) of time range

    instrument: string

    processes: list
        functions from metrics to apply to each magnetogram

    nworkers: integer
        number of concurrent workers

    out: dicitonary
        information related to upload cleaned magnetograms

    Returns
    -------
    metrics_df: pandas dataframe
        metrics computed for each magnetogram in the time range

    bad_df: pandas dataframe
        filenames that are corrupted


    """
    if processes is None or len(processes) < 1:
        raise ValueError('Processes must be a list with at least one process')

    if instrument.upper() not in ['HMI', 'MDI', 'MDI-NEW', 'GONG', 'SPMG', 'KP512', 'MWO']:
        raise ValueError('Instrument is not defined')

    file_handles = get_files_from_index(index_filepath, year_start,
                                        year_end,
                                        month_start,
                                        month_end,
                                        instrument=instrument)

    futures = []
    with ProcessPoolExecutor(max_workers=nworkers) as executor:
        for handle in file_handles:
            futures.append(
                executor.submit(process_file, handle, processes, destination,
                                out=out, 
                                instrument=instrument)
                )

    wait(futures)

    metrics = [f.result()[0] for f in futures]
    prob_files = [f.result()[1] for f in futures]

    metrics_df = pd.DataFrame(metrics)

    bad_df = pd.DataFrame(flatten(prob_files), columns=['Filename'])

    if out == 1:
        upload_index(metrics_df, destination, 'index')

    return metrics_df, bad_df


def process_file(handle, processes, destination, instrument='HMI', out=0):
    """
    Process a fits file calculating metrics and pre-processing

    Parameters
    ----------
    handle :
        handle to fits file in bucket
    processes : list
        List of metrics to calculate

    instrument: string
        instrument processed

    out: dictionary
        information related to upload cleaned magnetograms

    Returns
    -------
        tuple : list of metrics and list of files with errors
    """
    prob_files = []

    with handle as file:
        try:

            sun_map = map_prep(file, instrument, handle.path)

            before_metrics = run_processes(sun_map, [disk_nans, radius])

            target_factor = TARGET_FACTOR[instrument]
            sun_map_prepped = scale_rotate(sun_map, target_factor=target_factor)

            date = sun_map_prepped.date.datetime
            month = date.month
            year = date.year
            day = date.day

            metrics = run_processes(sun_map_prepped, processes)

            metrics_results = flatten_dict(before_metrics)
            metrics_results.update(flatten_dict(metrics))

            metrics_results['dateobs'] = sun_map.date.datetime

            if out == 1:
                if "gs://" not in destination:
                    os.makedirs('{}/{}/{}/{}'.format(destination, year, month, day), exist_ok=True)
                    destination = '{}/{}/{}/{}'.format(destination, year, month, day)
                    client=None

                else:
                    client = storage.Client()

                upload(sun_map_prepped,
                       destination,
                       client=client,
                       instrument=instrument)
                
                metrics_results['filepath'] = destination + \
                                             '/{}_{}.fits'.format(instrument,
                                                                  date.strftime("%Y%m%d-%H%M%S"))

        except Exception as e:  # In case the file is corrupt / doesn't open
            print(f'Error in file {handle.path} with error: {e}')
            prob_files = handle.path
            metrics_results = {}

    return metrics_results, prob_files


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--filepath')
    parser.add_argument('--destination')
    parser.add_argument('--year_start')
    parser.add_argument('--year_end')
    parser.add_argument('--month_start')
    parser.add_argument('--month_end')
    parser.add_argument('--process')
    parser.add_argument('--instrument')
    parser.add_argument('--nworkers')
    parser.add_argument('--out')

    args = parser.parse_args()

    print("You are running the script with arguments: ")
    for a in args.__dict__:
        print(str(a) + ": " + str(args.__dict__[a]))

    kwargs_dict = {'processes': [eval(proc) for proc
                                 in args.process.split(',')],
                   'instrument': args.instrument,
                   'out': int(args.out),
                   'nworkers': int(args.nworkers)}

    df, bad_df = data_pipeline(args.filepath,
                               args.destination,
                               int(args.year_start),
                               int(args.year_end),
                               int(args.month_start),
                               int(args.month_end),
                               **kwargs_dict
                               )
