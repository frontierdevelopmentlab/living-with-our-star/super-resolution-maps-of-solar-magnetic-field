import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class DenoisedHistogram(nn.Module):

    def __init__(self, dl=0.15, lim=5000, normalisation=1, noise_level=20, device='cuda'):
        super().__init__()

        device = torch.device("cuda" if device =='cuda' else "cpu")

        # Define histogram properties
        lim = np.log10(lim)  # Maximum value

        # Creating positive part
        bins = np.round(np.power(10, np.arange(1, lim + dl, dl)), 2)
        bins = bins - 10 + noise_level

        # Mirroring it to 0
        bins = np.append(np.flip(-(bins)), bins)/normalisation

        # Defining centers and bin widths for the learnable histogram
        centers = (bins[1:] + bins[0:-1]) / 2
        widths = (bins[1:] - bins[0:-1])

        # Store centers and widths
        self.centers = centers
        self.widths = widths

        # Create array of non-zero centers to ignore noisy part of the spectrum
        nonzero = np.ones(centers.shape[0])
        nonzero[centers == 0] = 0

        # Center calculation
        self.conv1 = nn.Conv2d(1, centers.shape[0], 1).to(device)
        self.conv1.weight = torch.nn.Parameter(torch.from_numpy(nonzero[:, None, None, None]).float()).to(device)
        self.conv1.bias = torch.nn.Parameter(torch.from_numpy(-centers * nonzero).float()).to(device)

        # Width and offset calculation
        self.conv2 = nn.Conv2d(centers.shape[0], centers.shape[0], 1).to(device)

        # Setting weights and bias to zero for the center bin
        diag = -nonzero / widths

        self.conv2.weight = torch.nn.Parameter(
            torch.from_numpy(np.expand_dims(np.expand_dims(np.diag(diag), axis=2), axis=2)).float()).to(device)
        self.conv2.bias = torch.nn.Parameter(torch.from_numpy(nonzero).float()).to(device)

    def forward(self, x):
        patch_size = x.shape[2]

        if len(x.shape) == 3:
            x = x[:, None, :, :]
        else:
            x = x[:, 0, :, :]
            x = x[:, None]

        x = self.conv1(x)
        x = torch.abs(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = F.avg_pool2d(x, patch_size) * patch_size * patch_size

        return x