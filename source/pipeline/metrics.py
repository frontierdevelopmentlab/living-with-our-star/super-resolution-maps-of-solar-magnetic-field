import torch
import numpy as np

import torch.nn as nn


class MeanMetric(nn.Module):
    """
    Class to calculate mean of tensor
    """

    def __init__(self):
        super().__init__()
        self.name = 'MeanMetric'

    def forward(self, x):
        ''' x: batch, frame/channels, width, height'''
        
        return torch.mean(x, dim = (-2,-1))
    

class StdevMetric(nn.Module):
    """
    Class to calculate standard dev of tensor
    """

    def __init__(self):
        super().__init__()
        self.name = 'StdevMetric'

    def forward(self, x):
        ''' x: batch, frame/channels, width, height'''
        
        return torch.std(x, dim = (-2,-1))
    
    

class MinMetric(nn.Module):
    """
    Class to calculate min of tensor
    """

    def __init__(self):
        super().__init__()
        self.name = 'MinMetric'

    def forward(self, x):
        ''' x: batch, frame/channels, width, height'''
        
        return torch.min(torch.min(x,-1)[0],-1)[0]
    

class MaxMetric(nn.Module):
    """
    Class to calculate max of tensor
    """

    def __init__(self):
        super().__init__()
        self.name = 'MaxMetric'

    def forward(self, x):
        ''' x: batch, width, height'''
        
        return torch.max(torch.max(x,-1)[0],-1)[0]
    

class UnsignedMetric(nn.Module):
    """
    Class to calculate unsigned sum of tensor
    """

    def __init__(self):
        super().__init__()
        self.name = 'UnsignedMetric'

    def forward(self, x):
        ''' x: batch, frame/channels, width, height'''
        
        return torch.mean(torch.abs(x), dim = (-2,-1))
    

class SignedMetric(nn.Module):
    """
    Class to calculate signed sum of tensor
    """

    def __init__(self):
        super().__init__()
        self.name = 'SignedMetric'

    def forward(self, x):
        ''' x: batch, frame/channels, width, height'''
        
        return torch.mean(x, dim = (-2,-1))

class PNSR(nn.Module):
    """
    Class to compute PNSR as 10 * log(Max**2/MSE)
    """

    def __init__(self, norm=3500):
        super().__init__()
        self.norm = norm
        self.name = 'PNSR'

    def forward(self, x, y):
        """

        :param x: target
        :param y: output
        :return:
        """
        return torch.log10(torch.tensor(self.norm) ** 2 / torch.mean((x - y) ** 2))

class CorrPearson(nn.Module):
    """
    Compute Paerson correlation between target and ouptut
    """

    def __init__(self, clamp=None):
        super().__init__()
        self.clamp = clamp
        self.name = 'PearsonCorrelation'
        if clamp is not None:
            self.name += f'_{clamp}'

    def forward(self, x, y):
        """
        :param x: target (B, ...)
        :param y: output (B, ...)
        :return: correlation (B)
        """

        y = y.view(y.shape[0], -1)
        x = x.view(x.shape[0], -1)

        if self.clamp is not None:
            x[torch.abs(y) < self.clamp] = 0
            y[torch.abs(y) < self.clamp] = 0
            y[torch.abs(x) < self.clamp] = 0
            x[torch.abs(x) < self.clamp] = 0

        # Require at least 10 points to estimate a pearson correlation
        x = x / (torch.sum(x != 0, 1) > 10).float()[..., None]
        y = y / (torch.sum(x != 0, 1) > 10).float()[..., None]

        xmean = torch.sum(x, 1)/torch.sum(x != 0, 1).float()
        ymean = torch.sum(y, 1)/torch.sum(y != 0, 1).float()

        y_demean = y - ymean[..., None]
        y_demean[x==0] = 0

        x_demean = x - xmean[..., None]
        x_demean[y==0] = 0

        y_std = (torch.sum(y_demean ** 2, 1)/torch.sum(y != 0, 1).float()) ** 0.5
        x_std = (torch.sum(x_demean ** 2, 1)/torch.sum(x != 0, 1).float()) ** 0.5

        covariance = torch.sum(y_demean * x_demean, 1)/torch.sum(x != 0, 1).float()

        return covariance / (y_std * x_std)


class CorrSpearman(nn.Module):
    """
    Compute Paerson correlation between target and ouptut
    """

    def __init__(self):
        super().__init__()
        self.name = 'SpearmanCorrelation'

    def forward(self, x, y):
        """
        :param x: target (B, ...)
        :param y: output (B, ...)
        :return: correlation (B)
        """
        y = y.view(y.shape[0], -1)
        x = x.view(x.shape[0], -1)

        rx = torch.argsort(x, -1).float()
        ry = torch.argsort(y, -1).float()

        ry_demean = y - torch.mean(ry, 1)[..., None]
        rx_demean = x - torch.mean(rx, 1)[..., None]
        ry_std = torch.mean(ry_demean ** 2, 1) ** 0.5
        rx_std = torch.mean(rx_demean ** 2, 1) ** 0.5

        covariance = torch.mean(ry_demean * rx_demean, 1)

        return covariance / (ry_std * rx_std)

class Quantile(nn.Module):
    """
    Compute quantile values
    """

    def __init__(self, q):
        super().__init__()
        self.name = f'Quantile_{q}'
        self.q = q

    def forward(self, x):
        """

        :param x: (B, ...)
        :return: (B) quantile values
        """

        x = x.view(x.shape[0], -1)
        x = x.detach().cpu().numpy()
        return torch.from_numpy(np.quantile(x, self.q, axis=-1))

