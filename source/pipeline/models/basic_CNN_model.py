
import torch.nn as nn
import torch.nn.init as init

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel


@template
class NetCNN(TemplateModel):

    def __init__(self, upscale_factor, n_frames=1):
        super().__init__()
        self.name = 'NetCNN'

        self.upscale_factor = upscale_factor

        self.relu = nn.ReLU()
        self.conv1 = nn.Conv2d(n_frames, 64, (5, 5), (1, 1), (2, 2))
        self.conv2 = nn.Conv2d(64, 64, (3, 3), (1, 1), (1, 1))
        self.conv3 = nn.Conv2d(64, 32, (3, 3), (1, 1), (1, 1))
        self.conv4 = nn.Conv2d(32, upscale_factor ** 2, (3, 3), (1, 1), (1, 1))
        self.pixel_shuffle = nn.PixelShuffle(upscale_factor)

        self._initialize_weights()

    def forward(self, x):
        print(x.shape)
        x = x[None] 
        print(x.shape)
        x = self.relu(self.conv1(x))
        x = self.relu(self.conv2(x))
        x = self.relu(self.conv3(x))
        x = self.pixel_shuffle(self.conv4(x))
        return x[0]

    def _initialize_weights(self):
        init.orthogonal_(self.conv1.weight, init.calculate_gain('relu'))
        init.orthogonal_(self.conv2.weight, init.calculate_gain('relu'))
        init.orthogonal_(self.conv3.weight, init.calculate_gain('relu'))
        init.orthogonal_(self.conv4.weight)
