""" 
Pytorch implementation of a Highway-MFSR model with temporal smoothing.
"""

from source.pipeline.models.model_utils import ResidualBlock, Encoder, TemporalSmoothing, Decoder

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel

import torch
import torch.nn as nn


class TemporalFusion(nn.Module):
    
    def __init__(self, input_channels=64, kernel_size=3):
        
        super(TemporalFusion, self).__init__()

        self.fuse = nn.Sequential(ResidualBlock(2*input_channels, kernel_size),
                                nn.ReflectionPad2d(kernel_size//2),
                                nn.Conv2d(in_channels=2*input_channels, out_channels=2*input_channels, kernel_size=kernel_size, padding=0),
                                nn.PReLU())

    def forward(self, x, mode='forward', complexity='linear'):
        '''
        Fuse a sequence of hidden states ordered by time
        Args:
            x : tensor (B, L, C_h, H, W), pair of hidden states
            mode: 'forward' (using the past) or 'backward' (using the future)
            complexity: 'linear' or 'quadratic'
        Returns:
            out: tensor (B, C_h, H, W), fused hidden state
        '''
        
        batch_size, nviews, channels, heigth, width = x.shape # (B, L, C_h, H, W)
        
        if nviews == 1:
            return x[:,0]
        
        if complexity=='linear':
      
            if mode=='forward':
                previous_state = x[:,0] # oldest state (B, C_h, H, W)
                for t in range(nviews-1):
                    joint_state = torch.cat([previous_state, x[:,t+1]],1) # (B, 2*C_h, H, W)
                    previous_state = x[:,t+1] + self.fuse(joint_state)[:,:channels] # fused state (B, C_h, H, W)  
                return previous_state # s_t-
            
            else:
                future_state = x[:,-1] # most recent state (B, C_h, H, W)  
                for t in range(nviews-1):
                    joint_state = torch.cat([x[:,-t-2], future_state],1) # (B, 2*C_h, H, W)
                    future_state = x[:,-t-2] + self.fuse(joint_state)[:,channels:] # fused state (B, C_h, H, W)  
                return future_state # s_t+
        
        else:
            while nviews>1:
                previous_states = x[:,:-1] # older states (B, L-1, C_h, H, W)
                next_states = x[:,1:] # newer states (B, L-1, C_h, H, W)
                joint_state = torch.cat([previous_states, next_states],2) # (B, L-1, 2*C_h, H, W)
                joint_state = joint_state.view(batch_size*(nviews-1), 2*channels, heigth, width) # reshape hidden states (B*(L-1), 2*C_h, H, W)  
                if mode=='forward':
                    fused_state = self.fuse(joint_state)[:,:channels] # fused state (B*(L-1), C_h, H, W)
                    fused_state = fused_state.view(batch_size, nviews-1, channels, heigth, width) # reshape fused states (B, (L-1), C_h, H, W)  
                    x = next_states + fused_state # skip-connection
                else:
                    fused_state = self.fuse(joint_state)[:,channels:] # fused state (B*(L-1), C_h, H, W)
                    fused_state = fused_state.view(batch_size, nviews-1, channels, heigth, width) # reshape fused states (B, (L-1), C_h, H, W)  
                    x = previous_states + fused_state # skip-connection
                nviews-=1
                return x[:,0]


@template
class HighwayMFSR(TemplateModel):
    """
    Highway_MFSR, a neural network for multi-frame super resolution (MFSR) by temporal smoothing.
    """

    def __init__(self, in_channels=1, enc_num_layers=2, fuse_num_layers=2, kernel_size=3,
                 hidden_channel_size=64, upscale_factor=2, final_kernel_size=1, n_frames=1):

        super(HighwayMFSR, self).__init__()
        
        self.name = 'Highway_MFSR'
        self.upscale_factor = upscale_factor
        
        self.encode = Encoder(in_channels=in_channels, num_layers=enc_num_layers,
                              kernel_size=kernel_size, channel_size=hidden_channel_size)
        self.temporal_fusion = TemporalFusion(input_channels=hidden_channel_size,
                                              kernel_size=kernel_size)
        self.temporal_smooth = TemporalSmoothing(input_channels=hidden_channel_size,
                                                 kernel_size=kernel_size)
        self.decode = Decoder(deconv_in_channels=hidden_channel_size,
                              deconv_out_channels=hidden_channel_size,
                              upscale_factor=upscale_factor, final_kernel_size=final_kernel_size)

    def forward(self, lrs, complexity='linear'):
        """
        Super resolve a batch of low-resolution images.

        Args:
            lrs : tensor (B, L, H, W) or (B, H, W), low-resolution images. Expected L to be odd (t-2, t-1, t, t+1, t+2) eg. for L=5.
        Returns:
            srs: tensor (B, H, W), super-resolved images

        """
        if len(lrs.shape) == 3:  # (B, H, W)
            lrs = lrs[:, None]  # (B, L, H, W)
        
        batch_size, n_views, heigth, width = lrs.shape
        lrs = lrs.view(-1, n_views, 1, heigth, width)  # (B, L, C_in, H, W)
        lrs = lrs.view(batch_size*n_views, 1, heigth, width)  # (B*L, C_in, H, W)

        # embed inputs
        layer1 = self.encode(lrs)  # encode input tensor (B*L, C_h, H, W)
        layer1 = layer1.view(batch_size, n_views, -1, heigth, width)  # (B, L, C_h, H, W)

        # fuse states
        # (B, L/2, C_h, H, W) to (B, C_h, H, W)
        past_state = self.temporal_fusion(layer1[:,:n_views//2+1],
                                          mode='forward', complexity=complexity)

        # (B, L/2, C_h, H, W) to (B, C_h, H, W)
        future_state = self.temporal_fusion(layer1[:,n_views//2:],
                                            mode='backward', complexity=complexity)

        # smooth states
        # (B, C_h, H, W)
        smoothed_state = self.temporal_smooth(torch.stack([past_state, future_state],1))
        
        # upsample, decode
        srs = self.decode(smoothed_state) # (B, upscale_factor*H, upscale_factor*W)
        return srs
