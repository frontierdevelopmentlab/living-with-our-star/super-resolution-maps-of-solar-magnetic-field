""""
Pytorch implementation of ESPCN
    Paper: https://arxiv.org/pdf/1609.05158.pdf
    Github: https://github.com/leftthomas/ESPCN/blob/master/model.py
​"""
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel


@template
class ESPCN(TemplateModel):

    def __init__(self, upscale_factor=2, n_frames=1, n_channels=1):
        super(ESPCN, self).__init__()

        self.name = 'ESPCN'
        self.upscale_factor = upscale_factor

        self.up = nn.Upsample(scale_factor=upscale_factor, mode='bicubic')

        self.conv1 = nn.Conv2d(n_channels, 64, (5, 5), (1, 1), (2, 2))
        self.conv2 = nn.Conv2d(64, 64, (3, 3), (1, 1), (1, 1))
        self.conv3 = nn.Conv2d(64, 32, (3, 3), (1, 1), (1, 1))
        self.conv4 = nn.Conv2d(32, 1 * (upscale_factor ** 2), (3, 3), (1, 1), (1, 1))
        self.pixel_shuffle = nn.PixelShuffle(upscale_factor)


    def forward(self, x, clip=2.):
        if len(x.shape) == 3:  # (batch,height,width)
            x = x[:, None]  # (batch,nviews,height,width)

        bicubic = self.up(x)

        x = F.tanh(self.conv1(x))
        x = F.tanh(self.conv2(x))
        x = F.tanh(self.conv3(x))
        x = clip * F.tanh(self.pixel_shuffle(self.conv4(x)))

        out = torch.add(bicubic, x)
        return out[:, 0, :, :]
