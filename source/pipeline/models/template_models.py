import torch.nn as nn

from source.pipeline.model_manager import template


@template
class TemplateModel(nn.Module):

    def __init__(self):
        super().__init__()
        self.name = 'template'
    
    @classmethod
    def from_config(cls, config_data):
        """
        create an input configuration from a saved yaml file
            
        Parameters
        ----------
        config_file: configuration file (yaml)
            
        Returns
        a upscaler moder
        """
        config_data.pop('name')
        return cls(**config_data)

    def forward(self, input):
        pass