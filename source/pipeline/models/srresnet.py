import numpy as np
import torch
import torch.nn as nn

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel


class _Residual_Block(nn.Module):
    def __init__(self, n):
        super(_Residual_Block, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=n, out_channels=n, kernel_size=3, stride=1, padding=1, padding_mode = "reflection", bias=False)
        self.in1 = nn.InstanceNorm2d(n, affine=True)
        self.relu = nn.LeakyReLU(0.2, inplace=True)
        self.conv2 = nn.Conv2d(in_channels=n, out_channels=n, kernel_size=3, stride=1, padding=1, padding_mode = "reflection", bias=False)
        self.in2 = nn.InstanceNorm2d(n, affine=True)

    def forward(self, x):
        identity_data = x
        output = self.relu(self.in1(self.conv1(x)))
        output = self.in2(self.conv2(output))
        output = torch.add(output, identity_data)
        return output


@template
class SRResNet(TemplateModel):
    
    def __init__(self, upscale_factor, n_frames=1, n_channels=1):
        super().__init__()
        self.name = 'SRResNet'
        self.upscale_factor = upscale_factor
        
        self.n = 64  # number of channels for the convolutional layers
        self.d = 16  # number of residual blocks

        self.conv_input = nn.Conv2d(in_channels=n_channels, out_channels=self.n, kernel_size=9, stride=1, padding=4, padding_mode = "reflection", bias=False)
        self.relu = nn.LeakyReLU(0.2, inplace=True)
        
        self.residual = self.make_layer(_Residual_Block, self.d)

        self.conv_mid = nn.Conv2d(in_channels=self.n, out_channels=self.n, kernel_size=3, stride=1, padding=1, padding_mode = "reflection", bias=False)
        self.bn_mid = nn.InstanceNorm2d(self.n, affine=True)

        if upscale_factor == 2:
            self.upscale = nn.Sequential(
            nn.Conv2d(in_channels=self.n, out_channels=self.n*4, kernel_size=3, stride=1, padding=1, padding_mode = "reflection", bias=False),
            nn.PixelShuffle(2),
            nn.LeakyReLU(0.2, inplace=True)
        )

        elif upscale_factor == 4:
            self.upscale = nn.Sequential(
                nn.Conv2d(in_channels=self.n, out_channels=self.n * 4, kernel_size=3, stride=1, padding=1,
                          padding_mode="reflection", bias=False),
                nn.PixelShuffle(2),
                nn.LeakyReLU(0.2, inplace=True),
		nn.Conv2d(in_channels=self.n, out_channels=self.n * 4, kernel_size=3, stride=1, padding=1,
                          padding_mode="reflection", bias=False),
                nn.PixelShuffle(2),
                nn.LeakyReLU(0.2, inplace=True)
            )

        elif upscale_factor == 8:
            self.upscale = nn.Sequential(
                nn.Conv2d(in_channels=self.n, out_channels=self.n * 4, kernel_size=3, stride=1, padding=1,
                          padding_mode="reflection", bias=False),
                nn.PixelShuffle(2),
                nn.LeakyReLU(0.2, inplace=True),
		nn.Conv2d(in_channels=self.n, out_channels=self.n * 4, kernel_size=3, stride=1, padding=1,
                          padding_mode="reflection", bias=False),
                nn.PixelShuffle(2),
                nn.LeakyReLU(0.2, inplace=True),
		nn.Conv2d(in_channels=self.n, out_channels=self.n * 4, kernel_size=3, stride=1, padding=1,
                          padding_mode="reflection", bias=False),
                nn.PixelShuffle(2),
                nn.LeakyReLU(0.2, inplace=True)
            )

        self.conv_output = nn.Conv2d(in_channels=self.n, out_channels=1, kernel_size=9, stride=1, padding=4, padding_mode = "reflection", bias=False)
        
        #for m in self.modules():
        #    if isinstance(m, nn.Conv2d):
        #        n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
        #        m.weight.data.normal_(0, np.sqrt(2. / n))
        #        if m.bias is not None:
        #            m.bias.data.zero_()

    def forward(self, x):
        
        if len(x.shape) == 3:
            batch_size, height, width = x.shape
            x = x.reshape((batch_size, 1, height, width))
        
        out = self.relu(self.conv_input(x))
        residual = out + 0.0  # to break equality
        out = self.residual(out)
        out = self.bn_mid(self.conv_mid(out))
        out = torch.add(out, residual)
        out = self.upscale(out)
        out = self.conv_output(out)
        return out[:, 0, :, :]

    def make_layer(self, block, num_of_layer):
        layers = []
        for _ in range(num_of_layer):
            layers.append(block(n=self.n))
        return nn.Sequential(*layers)
