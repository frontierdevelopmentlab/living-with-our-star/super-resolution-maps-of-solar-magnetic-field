"""
Pytorch implementation of DRRN
    Paper: http://openaccess.thecvf.com/content_cvpr_2017/papers/Tai_Image_Super-Resolution_via_CVPR_2017_paper.pdf
    Github: https://github.com/jt827859032/DRRN-pytorch/blob/master/drrn.py
"""

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from math import sqrt

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel


@template
class DRRN(TemplateModel):

    def __init__(self, upscale_factor=2, n_frames=1, n_channels=1):
        super(DRRN, self).__init__()

        self.name = 'DRRN'
        self.upscale_factor = upscale_factor

        self.up = nn.Upsample(scale_factor=upscale_factor, mode='bicubic')

        self.input = nn.Conv2d(in_channels=n_channels, out_channels=128, kernel_size=3, stride=1,
                               padding=1, bias=False)
        self.conv1 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1,
                               padding=1, bias=False)
        self.conv2 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1,
                               padding=1, bias=False)
        self.output = nn.Conv2d(in_channels=128, out_channels=1, kernel_size=3, stride=1, padding=1,
                                bias=False)
        self.prelu = nn.LeakyReLU(inplace=True)

        # weights initialization
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, sqrt(2. / n))

    def forward(self, x):
        if len(x.shape) == 3:  # (batch,height,width)
            x = x[:, None]  # (batch,nviews,height,width)

        x = self.up(x)  # upsample low-resolution frames to high-res space first using bicubic upsampling
        residual = x
        inputs = self.input(x)
        out = inputs
        for _ in range(25):
            out = self.conv2(self.prelu(self.conv1(self.prelu(out))))
            out = torch.add(out, inputs)

        out = self.output(self.prelu(out))
        out = torch.add(out, residual)
        return out[:, 0, :, :]
