""" 
Pytorch implementation of a convGRU-MFSR model with temporal smoothing.
"""

from model_utils import ResidualBlock, Encoder, TemporalSmoothing, Decoder

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel

import torch
import torch.nn as nn


class ConvGRUUnit(nn.Module):

    def __init__(self, in_channels, hidden_channel, kernel_size):
        super(ConvGRUUnit, self).__init__()
        padding = kernel_size // 2
        self.in_channels = in_channels
        self.hidden_channel = hidden_channel
        self.reset_gate = nn.Conv2d(in_channels + hidden_channel, hidden_channel, kernel_size, padding=padding)
        self.update_gate = nn.Conv2d(in_channels + hidden_channel, hidden_channel, kernel_size, padding=padding)
        self.out_gate = nn.Conv2d(in_channels + hidden_channel, hidden_channel, kernel_size, padding=padding)

    def forward(self, x, prev_state):

        # get batch and spatial sizes
        batch_size = x.data.size()[0]
        spatial_size = x.data.size()[2:]

        # generate empty prev_state, if it is None
        if prev_state is None:
            state_size = [batch_size, self.hidden_channel] + list(spatial_size)
            if torch.cuda.is_available():
                prev_state = Variable(torch.zeros(state_size)).cuda()
            else:
                prev_state = Variable(torch.zeros(state_size))

        # data size is [batch, channel, height, width]
        stacked_inputs = torch.cat([x, prev_state], dim=1)
        update = torch.sigmoid(self.update_gate(stacked_inputs))
        reset = torch.sigmoid(self.reset_gate(stacked_inputs))
        out_inputs = torch.tanh(self.out_gate(torch.cat([x, prev_state * reset], dim=1)))
        new_state = prev_state * (1 - update) + out_inputs * update
        return new_state


class ConvGRU(nn.Module):

    def __init__(self, input_channels=64, num_layers=2, hidden_channel=64, kernel_size=3):
        super(ConvGRU, self).__init__()

        self.input_channels = input_channels
        self.num_layers = num_layers
        hidden_channel = hidden_channel
        kernel_sizes = kernel_size

        gru_units = []
        for i in range(0, self.num_layers):
            cur_input_dim = self.input_channels if i == 0 else hidden_channel
            gru_unit = ConvGRUUnit(in_channels=cur_input_dim, hidden_channel=hidden_channel, kernel_size=kernel_size)
            gru_units.append(gru_unit)
        self.gru_units = nn.ModuleList(gru_units)

    def forward(self, x, h=None):

        if h is None:
            hidden_states = [None] * self.num_layers
        num_low_res = x.size(1)
        cur_layer_input = x

        for l in range(self.num_layers):
            gru_unit = self.gru_units[l]
            h = hidden_states[l]

            out = []
            for t in range(num_low_res):
                h = gru_unit(cur_layer_input[:, t, :, :, :], h)
                out.append(h)

            out = torch.stack(out, dim=1)
            cur_layer_input = out

        return cur_layer_input[:, -1, :, :, :], cur_layer_input


@template
class convGRU_MFSR(TemplateModel):
    ''' convGRU_MFSR, a neural network for multi-frame super resolution (MFSR) by temporal smoothing. '''
            
    def __init__(self, in_channels=1, enc_num_layers=2, fuse_num_layers=2, kernel_size=3, hidden_channel_size=64, upscale_factor=2, final_kernel_size=1, n_frames=1):

        super(convGRU_MFSR, self).__init__()
        
        self.name = 'convGRU_MFSR'
        self.upscale_factor = upscale_factor
        
        self.encode = Encoder(in_channels=in_channels, num_layers=enc_num_layers, kernel_size=kernel_size, channel_size=hidden_channel_size)
        self.fuse_forward = ConvGRU(input_channels=hidden_channel_size, num_layers=fuse_num_layers, hidden_channel=hidden_channel_size, kernel_size=kernel_size)
        self.fuse_reverse = ConvGRU(input_channels=hidden_channel_size, num_layers=fuse_num_layers, hidden_channel=hidden_channel_size, kernel_size=kernel_size)
        self.temporal_smooth = TemporalSmoothing(input_channels=hidden_channel_size, kernel_size=kernel_size)
        self.decode = Decoder(deconv_in_channels=hidden_channel_size, deconv_out_channels=hidden_channel_size, upscale_factor=upscale_factor, final_kernel_size=final_kernel_size)

    def forward(self, lrs):
        '''
        Super resolve a batch of low-resolution images.
        Args:
            lrs : tensor (B, L, H, W) or (B, H, W), low-resolution images. Expected L to be odd (t-2, t-1, t, t+1, t+2) eg. for L=5.
        Returns:
            srs: tensor (B, H, W), super-resolved images
        '''
        
        if len(lrs.shape)==3: # (B, H, W)
            lrs = lrs[:,None] # (B, L, H, W)
        
        batch_size, n_views, heigth, width = lrs.shape
        lrs = lrs.view(-1, n_views, 1, heigth, width)  # (B, L, C_in, H, W)
        lrs = lrs.view(batch_size*n_views, 1, heigth, width) # (B*L, C_in, H, W)

        # embed inputs
        layer1 = self.encode(lrs) # encode input tensor (B*L, C_h, H, W)
        layer1 = layer1.view(batch_size, n_views, -1, heigth, width) # (B, L, C_h, H, W)

        # fuse states
        past_state, _ = self.fuse_forward(layer1[:,:n_views//2+1]) # (B, L/2, C_h, H, W) to (B, C_h, H, W)
        future_state, _ = self.fuse_reverse(torch.flip(layer1[:,n_views//2:], [1])) # (B, L/2, C_h, H, W) to (B, C_h, H, W)

        # smooth states
        smoothed_state = self.temporal_smooth(torch.stack([past_state, future_state],1)) # (B, C_h, H, W)
        
        # upsample, decode
        srs = self.decode(smoothed_state) # (B, upscale_factor*H, upscale_factor*W)
        return srs