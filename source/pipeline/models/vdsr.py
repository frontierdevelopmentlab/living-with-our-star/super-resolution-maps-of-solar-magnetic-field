#pylint: disable=W0221
"""
Pytorch implementation of VDSR
    Paper: https://cv.snu.ac.kr/research/VDSR/
    Github: https://github.com/twtygqyy/pytorch-vdsr/blob/master/vdsr.py
"""

import numpy as np
import torch
import torch.nn as nn

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel


class ConvReLUBlock(nn.Module):
    """
    Conv + ReLu Block
    """
    def __init__(self):
        super(ConvReLUBlock, self).__init__()
        self.conv = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1,
                              bias=False)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        return self.relu(self.conv(x))


@template
class VDSR(TemplateModel):
    """
    VDSR model
    """

    def __init__(self, upscale_factor=2, n_frames=1, n_channels=1):

        super(VDSR, self).__init__()

        self.name = 'VDSR'
        self.upscale_factor = upscale_factor

        self.up_scale = nn.Upsample(scale_factor=upscale_factor, mode='bicubic')

        self.residual_layer = make_layer(ConvReLUBlock, 18)
        self.input = nn.Conv2d(in_channels=n_channels, out_channels=64, kernel_size=3, stride=1,
                               padding=1, bias=False)
        self.output = nn.Conv2d(in_channels=64, out_channels=1, kernel_size=3, stride=1, padding=1,
                                bias=False)
        self.relu = nn.ReLU(inplace=True)

        for i in self.modules():
            if isinstance(i, nn.Conv2d):
                std = i.kernel_size[0] * i.kernel_size[1] * i.out_channels
                i.weight.data.normal_(0, np.sqrt(2. / std))

    def forward(self, x):
        if len(x.shape) == 3:  # (batch, height, width)
            x = x[:, None]  # (batch, nviews, height, width)

        # Upsample low-resolution frames to high-res space first using bicubic upsampling
        x = self.up_scale(x)

        residual = x
        out = self.relu(self.input(x))
        out = self.residual_layer(out)
        out = self.output(out)
        out = torch.add(out, residual)
        return out[:, 0, :, :]


def make_layer(block, num_of_layer):
    """
    Make layers of block
    Parameters
    ----------
    block : block to layer
    num_of_layer : num layers

    Returns
    -------
    layered blocks
    """
    layers = []
    for _ in range(num_of_layer):
        layers.append(block())
    return nn.Sequential(*layers)
