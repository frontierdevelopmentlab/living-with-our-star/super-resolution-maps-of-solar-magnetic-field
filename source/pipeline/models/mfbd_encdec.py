""" 
Pytorch implementation of Multi Frame Blind Deconvolution (MFBD) Encoder-Decoder model
Github: https://github.com/aasensio/learned_mfbd
Paper: https://arxiv.org/pdf/1806.07150.pdf
"""

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel

import torch
import torch.nn as nn


class ConvBlock(nn.Module):

    def __init__(self, inplanes, outplanes, kernel_size=3, stride=1, upsample=False):
        super(ConvBlock, self).__init__()

        self.upsample = upsample

        if (upsample):
            self.upsample = nn.Upsample(scale_factor=2)
            self.conv = nn.Conv2d(inplanes, outplanes, kernel_size=kernel_size, stride=1)
        else:
            self.conv = nn.Conv2d(inplanes, outplanes, kernel_size=kernel_size, stride=stride)

        nn.init.kaiming_normal_(self.conv.weight)
        nn.init.constant_(self.conv.bias, 0.1)

        self.reflection = nn.ReflectionPad2d(int((kernel_size-1)/2))
        self.bn = nn.BatchNorm2d(inplanes)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        out = self.bn(x)
        out = self.relu(out)
        
        if (self.upsample):
            out = self.upsample(out)

        out = self.reflection(out)
        out = self.conv(out)
            
        return out


@template
class MFBD_encdec(TemplateModel):

    def __init__(self, upscale_factor, n_frames=1, n_channels=1):
        super(MFBD_encdec, self).__init__()
        
        self.name = 'MFBD_encdec'
        self.upscale_factor = upscale_factor
        self.up = nn.Upsample(scale_factor=upscale_factor, mode='bicubic')
        
        self.A01 = ConvBlock(n_channels, 32, kernel_size=3)
        
        self.C01 = ConvBlock(32, 64, stride=2)
        self.C02 = ConvBlock(64, 64)
        self.C03 = ConvBlock(64, 64)
        self.C04 = ConvBlock(64, 64, kernel_size=1)

        self.C11 = ConvBlock(64, 64)
        self.C12 = ConvBlock(64, 64)
        self.C13 = ConvBlock(64, 64)
        self.C14 = ConvBlock(64, 64, kernel_size=1)
        
        self.C21 = ConvBlock(64, 128, stride=2)
        self.C22 = ConvBlock(128, 128)
        self.C23 = ConvBlock(128, 128)
        self.C24 = ConvBlock(128, 128, kernel_size=1)
        
        self.C31 = ConvBlock(128, 256, stride=2)
        self.C32 = ConvBlock(256, 256)
        self.C33 = ConvBlock(256, 256)
        self.C34 = ConvBlock(256, 256, kernel_size=1)
        
        self.C41 = ConvBlock(256, 128, upsample=True)
        self.C42 = ConvBlock(128, 128)
        self.C43 = ConvBlock(128, 128)
        self.C44 = ConvBlock(128, 128)
        
        self.C51 = ConvBlock(128, 64, upsample=True)
        self.C52 = ConvBlock(64, 64)
        self.C53 = ConvBlock(64, 64)
        self.C54 = ConvBlock(64, 64)
        
        self.C61 = ConvBlock(64, 64, upsample=True)
        self.C62 = ConvBlock(64, 64)
        self.C63 = ConvBlock(64, 16)

        self.C64 = nn.Conv2d(16, 1, kernel_size=1, stride=1, bias=False)
        self.C64.weight.data = self.C64.weight.data * 0.0

        # nn.init.kaiming_normal_(self.C64.weight)
        # nn.init.constant_(self.C64.bias, 0.1)
                   
    def forward(self, x):  
        if len(x.shape) == 3: # (batch,height,width)
            x_expanded = x[:, None] # (batch,nviews,height,width)
        else:
            x_expanded = x

        # upsample low-resolution frames to high-res space first using bicubic upsampling
        x_expanded= self.up(x_expanded)
        A01 = self.A01(x_expanded)

        # N -> N/2
        C01 = self.C01(A01)
        C02 = self.C02(C01)
        C03 = self.C03(C02)
        C04 = self.C04(C03)
        C04 += C01
        
        # N/2 -> N/2
        C11 = self.C11(C04)
        C12 = self.C12(C11)
        C13 = self.C13(C12)
        C14 = self.C14(C13)
        C14 += C11
        
        # N/2 -> N/4
        C21 = self.C21(C14)
        C22 = self.C22(C21)
        C23 = self.C23(C22)
        C24 = self.C24(C23)
        C24 += C21
        
        # N/4 -> N/8
        C31 = self.C31(C24)
        C32 = self.C32(C31)
        C33 = self.C33(C32)
        C34 = self.C34(C33)
        C34 += C31
        
        C41 = self.C41(C34)
        C41 += C24
        C42 = self.C42(C41)
        C43 = self.C43(C42)
        C44 = self.C44(C43)
        C44 += C41
        
        C51 = self.C51(C44)
        C51 += C14
        C52 = self.C52(C51)
        C53 = self.C53(C52)
        C54 = self.C54(C53)
        C54 += C51
        
        C61 = self.C61(C54)        
        C62 = self.C62(C61)
        C63 = self.C63(C62)
        C64 = self.C64(C63)
        out = C64 + x_expanded[:,0:1,:,:]
        
        if len(x.shape) == 3:
            return out[:, 0]
        else:
            return out
