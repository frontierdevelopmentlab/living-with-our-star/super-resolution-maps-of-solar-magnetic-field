"""
Pytorch implementation of SRCNN
    Paper: https://arxiv.org/pdf/1501.00092.pdf
    Github: https://github.com/lizhuangzi/SRCNN/blob/master/network.py
            https://github.com/topics/srcnn
"""

import numpy as np
import torch
import torch.nn as nn

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel


@template
class SRCNN(TemplateModel):

    def __init__(self, upscale_factor=2, n_frames=1, n_channels=1):
        super(SRCNN, self).__init__()

        self.name = 'SRCNN'
        self.upscale_factor = upscale_factor

        self.up = nn.Upsample(scale_factor=upscale_factor, mode='bicubic')

        self.conv1 = nn.Conv2d(n_channels, 64, kernel_size=9, padding=4)
        self.relu1 = nn.ReLU()
        self.conv2 = nn.Conv2d(64, 32, kernel_size=1, padding=0)
        self.relu2 = nn.ReLU()
        self.conv3 = nn.Conv2d(32, 1, kernel_size=5, padding=2)

    def forward(self, x):
        if len(x.shape) == 3:  # (batch,height,width)
            x = x[:, None]  # (batch,nviews,height,width)

        x = self.up(
            x)  # upsample low-resolution frames to high-res space first using bicubic upsampling

        out = self.conv1(x)
        out = self.relu1(out)
        out = self.conv2(out)
        out = self.relu2(out)
        out = self.conv3(out)

        return out[:, 0, :, :]
