
import torch.nn as nn

from source.pipeline.model_manager import template
from source.pipeline.models.template_models import TemplateModel


@template
class BiLinear(TemplateModel):
    """
    Create an architecture with at minimum a forward method using a bilinear interpolation
    """

    def __init__(self, upscale_factor, n_frames=1):
        super().__init__()
        self.upscale_factor = upscale_factor
        self.name = 'BiLinear'
    
    def forward(self, input):
        input = input[None] # add one dimension to account for channels
        output = nn.Upsample(scale_factor=self.upscale_factor, mode='bilinear', align_corners=False)(input)
        return output[0]

    def to(self, device): # this is hackish we need this because BaseScaler calls net.to(device)
        return self

@template
class BiCubic(BiLinear):

    """
    Create an architecture with at minimum a forward method using a bicubic interpolation
    """

    def __init__(self, upscale_factor, n_frames=1):
        super().__init__(upscale_factor)
        self.name = 'BiCubic'

    def forward(self, input):

        if len(input.shape) == 3:  # (batch,height,width)
            input = input[:,None]  # (batch,nviews,height,width)

        output = nn.Upsample(scale_factor=self.upscale_factor, mode='bicubic', align_corners=False)(input)
        return output[:, 0, :, :]
