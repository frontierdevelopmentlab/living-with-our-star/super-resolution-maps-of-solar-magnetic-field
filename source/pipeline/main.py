from dataset import BaseDataset, MagDataset, GoogleDataset
from torch.utils.data import DataLoader, Subset
import torch
from torch.utils.data.sampler import SubsetRandomSampler, SequentialSampler, BatchSampler
from model_manager import BaseScaler, _templates
from models.basic_CNN_model import NetCNN
from models.SRResNet import SRResNet
from models.baseline_model import BiLinear, BiCubic
#from baseline_loss import MSELoss
from visualizer import Visualize 
from losses.entropy_loss import EntropyLoss
from losses.baseline_loss import MSELoss
from losses.gradient_loss import GradientLoss
from google.cloud import storage
import torch.nn as nn
#from torchsummary import summary
import numpy as np
from tensorboardX import SummaryWriter
import os
import yaml


print('===> Loading datasets')

conversion = {'input': 'LR', 'target': 'HR'}
root_dir = "../../../data/toy_example"
index_filename = 'index.csv'

#dset = BaseDataset(index_filename, root_dir, conversion)

gcs_storage_client = storage.Client()
dset_train = GoogleDataset(index_filename, 'fdl-mag-sandbox', gcs_storage_client, conversion, np.arange(0,10))
dset_test = GoogleDataset(index_filename, 'fdl-mag-sandbox', gcs_storage_client, conversion, np.arange(10,20))
#data_size = len(dset)

config_file = 'config_test.yml'

with open(config_file, 'r') as stream:
    config_data = yaml.load(stream, Loader=yaml.SafeLoader) 

#test_indices = np.arange(11,15) #[10:14]
#train_indices = np.arange(0,11) #[0:10]
train_loader = DataLoader(dset_train,batch_size=config_data['batch_size'], shuffle = True) #, sampler=SubsetRandomSampler(train_indices))
test_loader = DataLoader(dset_test,batch_size=config_data['batch_size']) #, sampler=SequentialSampler(test_indices))

print('====> Building model')
model = BaseScaler.from_config(config_file)


logging_dir = 'summaries/' + f'{model.net.name}_{model.net.upscale_factor}_{model.Loss}_{model.batch_size}_{model.learning_rate}'
print(logging_dir)

#print(summary(model.net, input_size=(1024, 1024)))
print(np.sum([p.numel() for p in model.net.parameters()]))

#print('====> Training')
writer = SummaryWriter(logging_dir)
model.train(10, train_loader, writer) #5 #epochs
print('<<<< Training completed >>>>')

for iteration, batch in enumerate(train_loader, 1): #enumerate(iterable, start=0 by default)
    input = batch['input'].to(model.device)
    target = batch['target'].to(model.device)
#    print(input.shape)
    output = model.forward(input) #output is predict for future name change
    loss = model.get_loss(output, target)
    print(loss)
    Visualize().visualize_img(input, output,target,root_dir) #root_dir not being used for bucket
    #Visualize().visualize_single(output, root_dir)

mean_eval = model.evaluation(test_loader, writer)
print('mean_eval', mean_eval)

writer.close()
