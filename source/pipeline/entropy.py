import torch.nn as nn
import torch
import numpy as np
from source.pipeline.metrics_denoised_hist import DenoisedHistogram

class Entropy():
    """
    Class to calculate entropy
    """

    def __init__(self, device):
        super().__init__()
        self.device = device


    def avg_ker(self, ker_size):
        """
        :param: ker_size: averaging kernel size
        :return: Normalized torch tensor of size ker_size * ker_size
        """

        K = torch.ones((ker_size,ker_size), device = self.device)
        N = ker_size**2

        K = K/N

        return K
    
    def entropy(self, P, eps=1e-6):
        """
        :param P: probability tensor
        :param eps: factor to avoid log(0)
        :return: entropy tensor
        """

        entropy = -torch.sum(P*torch.log(P+eps))
        return entropy
    
    
    def forward(self, x, max_ker_size=0):
        """
        x: [batch, widht, height]
        max_ker_size: number for maximum kernel size
        """
        
        if max_ker_size == 0:
            max_ker_size = x.shape[1]
        
        list_ker_size = list(range(1, max_ker_size+1))
        
        entropies = []
        no_bins = []

        for w in list_ker_size:
            conv_layer = torch.nn.Conv2d(1, 1, w, padding=0, stride=w, bias=False)
            conv_layer.weight.data = self.avg_ker(w)[None,None,]
            if len(x.shape) == 3:
                conv_img = conv_layer(x[:,None,:,:])
            else:
                x = x[:, 0, :, :]
                conv_img = conv_layer(x[:,None,:,:])

#            gausshist = GaussianHistogram(bins=10, min = -700 / 3.5e3, max = 700  / 3.5e3, sigma=0.5).to(x.device)
#            P = gausshist(x.view(-1))
            
            P = torch.histc(conv_img.cpu(), bins=2**16, min=-1500, max=1500)

            P = P / P.sum()

            H = self.entropy(P.float())
            entropies.append(H) #.detach().cpu().numpy()

            no_bins.append((x.shape[2]/w**2)) # This will be floats. We might want to change it to integers
            
        return no_bins, list_ker_size, entropies
    

class Diff_Entropy():
    """
    Class to calculate entropy using a differentiable histogram
    """

    def __init__(self, dl=0.15, lim=5000, noise_level=1, normalisation=1, ker_size_list=[1, 2, 4, 8, 16, 32], device='cuda'):
        super().__init__()

        self.normalisation = normalisation
        self.dl = dl
        self.lim = lim
        self.noise_level = noise_level
        self.ker_size_list = ker_size_list
        self.device = device

    def avg_ker(self, ker_size):
        """
        :param: ker_size: averaging kernel size
        :return: Normalized torch tensor of size ker_size * ker_size
        """

        K = torch.ones((ker_size,ker_size), device = self.device)
        N = ker_size**2

        K = K/N

        return K
    
    def entropy(self, P, eps=1e-6):
        """
        :param P: probability tensor
        :param eps: factor to avoid log(0)
        :return: entropy tensor
        """

        entropy = -torch.sum(P*torch.log(P+eps))
        return entropy    
    
    def forward(self, x):
        """
        x: [batch, width, height]
        ker_size_list: list of kernel sizes
        """
        
        entropies = []
        no_bins = []
        
        Hist = DenoisedHistogram(dl=self.dl, lim=self.lim, noise_level=self.noise_level, normalisation=self.normalisation, device=self.device).to(self.device)

        for w in self.ker_size_list:
            if w <= x.shape[2]: # only perform operation if kernel size is smaller/equal to image size

                conv_layer = torch.nn.Conv2d(1, 1, w, padding=0, stride=w, bias=False)
                conv_layer.weight.data = self.avg_ker(w)[None,None,]

                if len(x.shape) == 3:
                    conv_img = conv_layer(x[:,None,:,:])
                else:
                    x = x[:, 0, :, :]
                    conv_img = conv_layer(x[:,None,:,:])

                P = Hist.forward(conv_img)

                # Colapsing batches
                P = torch.squeeze(torch.sum(P, dim=0))

                # Normalizing
                P = P / P.sum() / torch.from_numpy(Hist.widths).to(self.device).float()

                H = self.entropy(P.float())
                entropies.append(H) #.detach().cpu().numpy()

                no_bins.append((x.shape[2]/w**2)) # This will be floats. We might want to change it to integers
            
        return no_bins, self.ker_size_list, entropies
    
    
class GaussianHistogram(nn.Module):
    """
    Class to generate differentiable histogram
    """

    def __init__(self, bins, min, max, sigma):
        '''
        :param bins: number of hist bins
        :param min: lower bound for histogram
        :param max: upper bound for histogram
        :param sigma:
        '''
        super(GaussianHistogram, self).__init__()
        self.bins = bins
        self.min = min
        self.max = max
        self.sigma = sigma
        self.delta = float(max - min) / float(bins)
        self.centers = float(min) + self.delta * (torch.arange(bins).float() + 0.5)

    def forward(self, x):
        x = torch.unsqueeze(x, 0) - torch.unsqueeze(self.centers.to(x.device), 1)
        x = torch.exp(-0.5*(x/self.sigma)**2) / (self.sigma * np.sqrt(np.pi*2)) * self.delta
        x = x.sum(dim=1)
        return x