import numpy as np 
import matplotlib.pyplot as plt
from matplotlib.colors import SymLogNorm
import matplotlib


class Visualize(object):

    """
    Visualizes output image and target image, which are coming out as tensors, visualizes the two images side-by-side, and saves the image
    """

    def visualize_img(self, image1, image2, image3, root_dir):
        """
        :param image1
        :param image2
        :param image3
        :param root_dir
        """
        
        # Colormap Definition
#        current_cmap = plt.cm.get_cmap('hmimag')
#         current_cmap = plt.cm.get_cmap('seismic')
#        current_cmap.set_bad(color='black')
        
        # Color Axis limits
        vmin = -2000
        vmax = 2000
        
        image1_tensortonump = np.squeeze(image1[0].detach().cpu().numpy())
        image2_tensortonump = np.squeeze(image2[0].detach().cpu().numpy())
        image3_tensortonump = np.squeeze(image3[0].detach().cpu().numpy())
        fig, axarr = plt.subplots(1,3)
        axarr[0].imshow(image1_tensortonump,norm=SymLogNorm(0.001))
        axarr[0].axis('off')
        axarr[0].set_title("Input")
        axarr[1].imshow(image2_tensortonump,norm=SymLogNorm(0.001))
        axarr[1].axis('off')
        axarr[1].set_title("Output")
        axarr[2].imshow(image3_tensortonump,norm=SymLogNorm(0.001)) #cmap=current_cmap, vmin=vmin, vmax=vmax
        axarr[2].axis('off')
        axarr[2].set_title("Target")
        
        #fig.savefig(f'{root_dir}/output_target.png',bbox_inches='tight')
        fig.savefig('output_target.png',bbox_inches='tight')
        
        


    
