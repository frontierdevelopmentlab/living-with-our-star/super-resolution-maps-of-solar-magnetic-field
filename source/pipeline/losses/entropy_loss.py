import torch
from torch.nn.modules import MSELoss, L1Loss
import torch.nn as nn
import numpy as np
import torch.nn.functional as F

from source.pipeline.metrics_denoised_hist import DenoisedHistogram
from source.pipeline.entropy import Diff_Entropy
from source.pipeline.model_manager import template


@template
class EntropyLoss(MSELoss):

    def __init__(self, dl, lim, noise_level, normalisation, coeff_hist, ker_size_list, device='cuda'):
        super().__init__()
        self.device = torch.device("cuda" if device =='cuda' else "cpu")
        self.name = 'EntropyLoss'

        self.ker_size_list = ker_size_list
        self.Entropy = Diff_Entropy(dl=dl, lim=lim, noise_level=noise_level, normalisation=normalisation, ker_size_list=ker_size_list, device=self.device)
        self.mseloss = MSELoss().to(device)
        self.coeff_hist = coeff_hist

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)

    def forward(self, x, y):
        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return: loss
        """

        no_bins, tar_ker_list, tar_entropy = self.Entropy.forward(x)
        no_bins, out_ker_list, out_entropy = self.Entropy.forward(y)

        print(tar_entropy.shape, out_entropy.shape)

        print(torch.mean(torch.abs(tar_entropy - out_entropy)), self.mseloss(x, y))

        return self.coeff_hist * torch.mean(torch.abs(tar_entropy - out_entropy)) + self.mseloss(x, y)