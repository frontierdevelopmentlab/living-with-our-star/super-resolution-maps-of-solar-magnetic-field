import torch
import torch.nn.functional as F
from torch.nn.modules import MSELoss
from source.pipeline.metrics_gradient import Grad_Sobel

from source.pipeline.model_manager import template

@template
class KLDivLoss(MSELoss):
    """
    KL Div Loss
    """
    def __init__(self):
        super().__init__()
        self.name = 'KLDivLoss'

    def forward(self, x, y):

        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return: loss
        """

        target = x
        output = y

        output_hist = torch.histc(output.cpu(), bins=2 ** 8, min=-750/3500, max=750/3500)
        output_hist = output_hist / output_hist.sum()
        output_hist += 0.000001 # to avoid log(0)

        target_hist = torch.histc(target.cpu(), bins=2 ** 8, min=-750/3500, max=750/3500)
        target_hist = target_hist / target_hist.sum()
        target_hist += 0.000001 # to avoid log(0)

        kl_div = F.kl_div(output_hist.log(), target_hist)

        mseloss = MSELoss()
        n = 1

        loss = mseloss(x, y) + n * kl_div.detach()

        return loss


@template
class KLGradLoss(MSELoss):
    """
    Combination Loss of KL Div and Gradient Loss
    """
    def __init__(self):
        super().__init__()
        self.name = 'KLGradLoss'

    def forward(self, x, y):

        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return: loss
        """

        target = x
        output = y

        output_hist = torch.histc(output.cpu(), bins=2 ** 8, min=-750/3500, max=750/3500)
        output_hist = output_hist / output_hist.sum()
        output_hist += 0.000001 # to avoid log(0)

        target_hist = torch.histc(target.cpu(), bins=2 ** 8, min=-750/3500, max=750/3500)
        target_hist = target_hist / target_hist.sum()
        target_hist += 0.000001 # to avoid log(0)

        kl_div = F.kl_div(output_hist.log(), target_hist)

        grad_sobel = Grad_Sobel(size=11, sigma=2, device=x.device)
        grad_out_x, grad_out_y, grad_out = grad_sobel.forward(output)  ###
        grad_tar_x, grad_tar_y, grad_tar = grad_sobel.forward(target)  ###

        n_k = 1.0
        n_g = 100.0

        mseloss = MSELoss()
        total_loss = mseloss(x,y) + n_k * kl_div.detach() + n_g * mseloss(grad_out, grad_tar)

        return total_loss


@template
class SignedFluxLoss(MSELoss):
    """
    KL Div Loss
    """
    def __init__(self):
        super().__init__()
        self.name = 'SignedFluxLoss'

    def forward(self, x, y):

        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return: loss
        """

        target = x
        output = y

        output_flux = torch.sum(output, dim = (-2,-1))
        target_flux = torch.sum(target, dim = (-2,-1))

        mseloss = MSELoss()
        n = 10**(-8)

        loss = mseloss(x, y) + n * mseloss(target_flux, output_flux)

        return loss


@template
class DistLoss(MSELoss):
    """
    Distribution Loss
    """
    def __init__(self):
        super().__init__()
        self.name = 'DistLoss'

    def forward(self, x, y):

        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return:
        """

        target_copy = x
        output_copy = y

        output_hist = torch.histc(output_copy.cpu(), bins=2 ** 8, min=-750/3500, max=750/3500)
        target_hist = torch.histc(target_copy.cpu(), bins=2 ** 8, min=-750/3500, max=750/3500)

        diff_hist = target_hist - output_hist

        mean_diff = diff_hist.mean()

        a = 1

        mseloss = MSELoss()

        loss = mseloss(x,y) + a * mseloss(target_hist, output_hist)
        #loss = mseloss(x,y) + a * mean_diff

        #this could potentially overbias the discrepancy at 0 ag field

        return loss