import torch
from torch.nn.modules import MSELoss

from source.pipeline.metrics_SSIM import SSIM
from source.pipeline.metrics_gradient import Grad_Sobel
from source.pipeline.model_manager import template


@template
class SSIMGradLoss(MSELoss):

    def __init__(self, coeff_ssim, coeff_grad, normalisation, device='cuda'):
        super().__init__()
        device = torch.device("cuda" if device =='cuda' else "cpu")
        self.name = 'SSIMGradLoss'

        self.normalisation = normalisation
        self.coeff_ssim = coeff_ssim
        self.ssim_loss = SSIM(window_size=32).to(device)
        self.mseloss = MSELoss()

        self.coeff_grad = coeff_grad

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)

    def forward(self, x, y):
        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return: loss
        """

        # Gradient
        grad_sobel = Grad_Sobel(size=11, sigma=2, device = x.device)
        grad_out_x, grad_out_y ,grad_out = grad_sobel.forward(y) ###
        grad_tar_x, grad_tar_y ,grad_tar = grad_sobel.forward(x)  ###

        if len(x.shape) == 3:
            x = x[:, None, :, :]
        if len(y.shape) == 3:
            y = y[:, None, :, :]

        return - self.coeff_ssim * self.ssim_loss(x, y) + self.mseloss(x, y) + self.coeff_grad * self.mseloss(grad_out, grad_tar)
