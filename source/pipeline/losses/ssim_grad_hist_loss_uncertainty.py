import math

import numpy as np
import torch
from torch.nn.modules import MSELoss

from source.pipeline.metrics_SSIM import SSIM
from source.pipeline.metrics_denoised_hist import DenoisedHistogram
from source.pipeline.metrics_gradient import Grad_Sobel
from source.pipeline.model_manager import template


@template
class SSIMGradHistLossUnc(MSELoss):

    def __init__(self, dl, lim, noise_level, normalisation, coeff_ssim, coeff_grad, coeff_hist, device='cuda'):
        super().__init__()
        device = torch.device("cuda" if device =='cuda' else "cpu")
        self.name = 'SSIMGradHistLossUnc'

        self.coeff_ssim = coeff_ssim
        self.ssim_loss = SSIM(window_size=32).to(device)
        self.mseloss = MSELoss()

        self.Hist = DenoisedHistogram(dl=dl, lim=lim, noise_level=noise_level, normalisation=normalisation, device=device).to(device)
        self.mseloss = MSELoss()
        self.coeff_hist = coeff_hist
        self.coeff_grad = coeff_grad

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)

    def forward(self, x, y):
        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return: loss
        """

        ymean = y[:, [0], ...]
        log_var = y[:, [1], ...]
        norm = 2 * np.log(350)
        # log_var = norm * torch.tanh(log_var / norm) # normalization so that std of field is at least +- 1 Gauss
        log_var = norm * torch.sigmoid( log_var / norm)
        std = torch.exp(0.5 * log_var) + 1. / 3500.

        # # Histogram
        hist_target = self.Hist.forward(x)
        hist_output = self.Hist.forward(ymean)
        #
        # # Gradient
        # grad_sobel = Grad_Sobel(size=11, sigma=2, device = x.device)
        # grad_out_x, grad_out_y ,grad_out = grad_sobel.forward(ymean) ###
        # grad_tar_x, grad_tar_y ,grad_tar = grad_sobel.forward(x)  ###

        if len(x.shape) == 3:
            x = x[:, None, :, :]
        if len(y.shape) == 3:
            ymean = ymean[:, None, :, :]

        # uncertainty
        l2_loss = 0.5 * (x - ymean) ** 2 / (std ** 2) + 0.5 * log_var + np.log(math.pi)
        l2_loss = torch.mean(l2_loss)

        return l2_loss + self.coeff_hist * torch.mean(torch.abs(hist_target - hist_output))
            #- self.coeff_ssim * self.ssim_loss(x, ymean) + l2_loss + self.coeff_grad * self.mseloss(grad_out, grad_tar) \
              # + self.coeff_hist * torch.mean(torch.abs(hist_target - hist_output))