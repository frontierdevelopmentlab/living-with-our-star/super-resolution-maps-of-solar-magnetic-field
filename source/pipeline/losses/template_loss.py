from model_manager import template
import torch.nn as nn

@template
class TemplateLoss(nn.Module):
    """
    Shared functionality for the template models.
    Need to define a loss  and a name
    
    Parameters
    ----------
    
    """

    def __init__(self):
        self.name = 'template'

    def loss(self):
        pass