import torch
from torch.nn.modules import MSELoss

from source.pipeline.metrics_SSIM import SSIM
from source.pipeline.metrics_denoised_hist import DenoisedHistogram
from source.pipeline.model_manager import template


@template
class SSIMHistLoss(MSELoss):

    def __init__(self, dl, lim, noise_level, normalisation, coeff_hist, coeff_ssim, device='cuda'):
        super().__init__()
        device = torch.device("cuda" if device =='cuda' else "cpu")
        self.name = 'SSIMHistLoss'

        self.coeff_ssim = coeff_ssim
        self.ssim_loss = SSIM(window_size=32).to(device)
        self.mseloss = MSELoss()

        self.Hist = DenoisedHistogram(dl=dl, lim=lim, noise_level=noise_level, normalisation=normalisation, device=device).to(device)
        self.mseloss = MSELoss()
        self.coeff_hist = coeff_hist

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)

    def forward(self, x, y):
        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return: loss
        """

        # Histogram
        hist_target = self.Hist.forward(x)
        hist_output = self.Hist.forward(y)

        if len(x.shape) == 3:
            x = x[:, None, :, :]
        if len(y.shape) == 3:
            y = y[:, None, :, :]

        return - self.coeff_ssim * self.ssim_loss(x, y) + self.mseloss(x, y) + self.coeff_hist * torch.mean(torch.abs(hist_target - hist_output))
