import torch
from torch.nn.modules import MSELoss

from source.pipeline.metrics_SSIM import SSIM
from source.pipeline.model_manager import template


@template
class SSIMLoss(MSELoss):

    def __init__(self, window_size, coeff_ssim, normalisation, device='cuda'):
        super().__init__()
        device = torch.device("cuda" if device =='cuda' else "cpu")
        self.name = 'SSIMLoss'
        self.normalisation = normalisation

        self.coeff_ssim = coeff_ssim
        self.ssim_loss = SSIM(window_size=window_size).to(device)
        self.mseloss = MSELoss()

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)

    def forward(self, x, y):
        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return: loss
        """

        if len(x.shape) == 3:
            x = x[:, None, :, :]
        if len(y.shape) == 3:
            y = y[:, None, :, :]

        return - self.coeff_ssim * self.ssim_loss(x, y) + self.mseloss(x, y)