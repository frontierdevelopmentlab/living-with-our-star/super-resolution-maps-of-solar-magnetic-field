import torch
from torch.nn.modules import MSELoss, L1Loss
import torch.nn as nn
import numpy as np
import torch.nn.functional as F

from source.pipeline.metrics_denoised_hist import DenoisedHistogram
from source.pipeline.model_manager import template


@template
class HistLoss(MSELoss):

    def __init__(self, dl, lim, noise_level, normalisation, coeff_hist, device='cuda'):
        super().__init__()
        device = torch.device("cuda" if device =='cuda' else "cpu")
        self.name = 'HistLoss'

        self.Hist = DenoisedHistogram(dl=dl, lim=lim, normalisation=normalisation, noise_level=noise_level, device=device).to(device)
        self.mseloss = MSELoss().to(device)
        self.coeff_hist = coeff_hist

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)

    def forward(self, x, y):
        """
        :param x: target [B, C, W, H]
        :param y: output [B, C, W, H]
        :return: loss
        """
        hist_target = self.Hist.forward(x)
        hist_output = self.Hist.forward(y)

        return self.coeff_hist * torch.mean(torch.abs(hist_target - hist_output)) + self.mseloss(x, y)