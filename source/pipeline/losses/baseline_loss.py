from torch.nn.modules import MSELoss, L1Loss, SmoothL1Loss, CrossEntropyLoss

from source.pipeline.model_manager import template


@template
class MSELoss(MSELoss):

    """
    L2 loss

    Parameters 
    -----------------
    name: loss name used by the config file

    """

    def __init__(self):
        super().__init__()
        self.name = 'MSELoss'

@template
class L1Loss(L1Loss):

    """
    L1 loss

    Parameters 
    -----------------
    name: loss name used by the config file

    """

    def __init__(self):
        super().__init__()
        self.name = 'L1Loss'

@template
class SmoothL1Loss(SmoothL1Loss):

    """
    Smooth L1 loss

    Parameters
    -----------------
    name: loss name used by the config file

    """

    def __init__(self):
        super().__init__()
        self.name = 'SmoothL1Loss'

@template
class CrossEntropyLoss(CrossEntropyLoss):

    """
    Cross Entropy loss

    Parameters
    -----------------
    name: loss name used by the config file

    """

    def __init__(self):
        super().__init__()
        self.name = 'CrossEntropyLoss'
