import torch
from torch.nn.modules import MSELoss, L1Loss

from source.pipeline.metrics_gradient import Grad_Sobel
from source.pipeline.metrics_denoised_hist import DenoisedHistogram
from source.pipeline.model_manager import template


@template
class GradHistLoss(MSELoss):
    """
    Gradient Loss
    """
    def __init__(self, dl, lim, noise_level, normalisation, coeff_grad, coeff_hist, device='cuda'):
        super().__init__()
        device = torch.device("cuda" if device =='cuda' else "cpu")
        self.name = 'GradHistLoss'

        self.Hist = DenoisedHistogram(dl=dl, lim=lim, noise_level=noise_level, normalisation=normalisation, device=device).to(device)
        self.mseloss = MSELoss()
        self.coeff_hist = coeff_hist
        self.coeff_grad = coeff_grad

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)

    def forward(self, x, y):

        # Histogram
        hist_target = self.Hist.forward(x)
        hist_output = self.Hist.forward(y)

        # Gradient
        grad_sobel = Grad_Sobel(size=11, sigma=2, device = x.device)
        grad_out_x, grad_out_y ,grad_out = grad_sobel.forward(y) ###
        grad_tar_x, grad_tar_y ,grad_tar = grad_sobel.forward(x)  ###

        total_loss = self.mseloss(x,y) + self.coeff_grad * self.mseloss(grad_out, grad_tar) + self.coeff_hist * torch.mean(torch.abs(hist_target - hist_output))

        return total_loss



@template
class GradL1HistLoss(L1Loss):
    """
    Gradient Loss with L1
    """

    def __init__(self, dl, lim, noise_level, coeff_grad=100, coeff_hist=0.00005, device='cuda'):
        super().__init__()
        device = torch.device("cuda" if device =='cuda' else "cpu")
        self.name = 'GradL1HistLoss'

        self.Hist = DenoisedHistogram(dl=dl, lim=lim, noise_level=noise_level).to(device)
        self.mseloss = MSELoss()
        self.coeff_hist = coeff_hist
        self.coeff_grad = coeff_grad

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)

    def forward(self, x, y):
        # Histogram
        hist_target = self.Hist.forward(x)
        hist_output = self.Hist.forward(y)

        # Gradient
        grad_sobel = Grad_Sobel(size=11, sigma=2, device=x.device)
        grad_out_x, grad_out_y, grad_out = grad_sobel.forward(y)  ###
        grad_tar_x, grad_tar_y, grad_tar = grad_sobel.forward(x)  ###

        l1loss = L1Loss()
        total_loss = self.mseloss(x,y) + self.coeff_grad * l1loss(grad_out, grad_tar) + self.coeff_hist * torch.mean(torch.abs(hist_target - hist_output))

        return total_loss