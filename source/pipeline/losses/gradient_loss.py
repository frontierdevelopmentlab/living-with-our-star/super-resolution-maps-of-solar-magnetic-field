import torch
from torch.nn.modules import MSELoss, L1Loss

from source.pipeline.metrics_gradient import Grad_Sobel
from source.pipeline.model_manager import template


@template
class GradientLoss(MSELoss):
    """
    Gradient Loss
    """
    def __init__(self, coeff_grad, normalisation, device='cuda'):
        super().__init__()
        self.name = 'GradientLoss'
        self.coeff_grad = coeff_grad
        self.normalisation = normalisation
        self.device = torch.device("cuda" if device == 'cuda' else "cpu")

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)

    def forward(self, x, y):
        
        grad_sobel = Grad_Sobel(size=11, sigma=2, device = x.device)
        grad_out_x, grad_out_y ,grad_out = grad_sobel.forward(y) ###
        grad_tar_x, grad_tar_y ,grad_tar = grad_sobel.forward(x)  ###
        
        grad_norm_out = torch.sum(grad_out,dim=(1,2))
        grad_norm_tar = torch.sum(grad_tar,dim=(1,2))

#        n_x = 1.0
#        n_y = 1.0

        
        mseloss = MSELoss()        
#        total_loss = mseloss(x,y) + n_x * mseloss(grad_out_x, grad_tar_x) + n_y * mseloss(grad_out_y, grad_tar_y)
        total_loss = mseloss(x,y) + self.coeff_grad * mseloss(grad_out, grad_tar)

        return total_loss

@template
class GradientLoss_L1(L1Loss):
    """
    Gradient Loss with L1
    """

    def __init__(self, coeff_grad=100, normalisation=1, device='cuda'):
        super().__init__()
        self.name = 'GradientLoss_L1'
        self.coeff_grad = coeff_grad
        self.normalisation = normalisation
        self.device = torch.device("cuda" if device == 'cuda' else "cpu")

    @classmethod
    def from_dict(cls, config_loss):
        """
        create an input configuration from a saved yaml file

        Parameters
        ----------
        config_file: configuration dictionary

        Returns
        a upscaler moder
        """
        config_loss.pop('name')
        return cls(**config_loss)


    def forward(self, x, y):
        grad_sobel = Grad_Sobel(size=11, sigma=2, device=x.device)
        grad_out_x, grad_out_y, grad_out = grad_sobel.forward(y)  ###
        grad_tar_x, grad_tar_y, grad_tar = grad_sobel.forward(x)  ###

        grad_norm_out = torch.sum(grad_out, dim=(1, 2))
        grad_norm_tar = torch.sum(grad_tar, dim=(1, 2))


        l1loss = L1Loss()
        total_loss = l1loss(x, y) + self.coeff_grad * l1loss(grad_out, grad_tar)

        print(l1loss(x, y))
        print(l1loss(grad_out, grad_tar))
        print(n * l1loss(grad_out, grad_tar))

        return total_loss