import torch.nn as nn
import torch
import math
import numpy as np

class Grad_Sobel(nn.Module):

    def __init__(self, size, sigma, device):
        super().__init__()
        self.name = 'Grad_Sobel'
        self.size = size
        self.sigma = sigma
        self.device = device
        
        self.S_x, self.S_y = self.sobel_kernel(self.size, self.sigma, self.device)
        
    
    def forward(self, x):
        ''' x: tensor with shape [batch, width, height]'''
        
        S_x = self.S_x[None,None,]
        S_y = self.S_y[None,None,]
        
        if len(x.shape) == 3:
            x_4d = x[:, None, :, :]
        else:
            x_4d = x[:, 0, :, :]
            x_4d = x_4d[:, None]
        
        Img_gradx = torch.conv1d(x_4d,
             weight=S_x.to(dtype=torch.float32),
             groups = S_x.shape[0],
             padding=[S_x.shape[2] // 2, S_x.shape[3] // 2],
            )  # same padding #output 4d

        Img_grady = torch.conv1d(x_4d,
             weight=S_y.to(dtype=torch.float32),
             groups = S_y.shape[0],
             padding=[S_y.shape[2] // 2, S_y.shape[3] // 2],
            )  # same padding

        Img_grad = torch.sqrt(Img_gradx**2 + (Img_grady**2))
        
        return Img_gradx[:,0,:,:], Img_grady[:,0,:,:], Img_grad[:,0,:,:] #input 3d, output 3d #o creates dummy dimension [:,0] == [:,0,:,:], #[:,0,[2],:]
        
        
    def gaussian_kernel(self, size, sigma, device):
        '''
        Create Gaussian kernel
        :param size: size of kernel
        :param sigma: sigma of gaussian
        :param device: either cuda or cpu
        :return: gaussian_kernel: gaussian matrix
        '''

        s = (size-1)/2
        x = torch.arange(-s,s+1, device=device)    
        sigma = torch.tensor(sigma, dtype = torch.float64, device = device)   
        K = (1./(torch.sqrt(2.*math.pi*(sigma**2.))) * torch.exp(-(x**2.) /(2*(sigma**2.))))  
        gaussian_kernel = K[:,None]@K[None,:]
        
        return gaussian_kernel
        
     
    def sobel_kernel(self, size, sigma, device):
        '''
        Create Sobel kernel for differentiation
        :param size: size of kernel
        :param sigma: sigma of gaussian kernel
        :param device: either cuda or cpu
        :return: S_x, S_y - Gaussian Sobel kernels
        '''
        
        Gaussian_2d = self.gaussian_kernel(size, sigma, device = device)
        Gaussian_4d = Gaussian_2d[None,None,]
    
        grad_x_2d = torch.tensor(np.array([[-1, 0, 1]]), dtype=torch.float32, device = device)
        grad_x_4d = grad_x_2d[None,None,]
        
        S_x = torch.conv1d(Gaussian_4d,
             weight=grad_x_4d,
             groups = grad_x_4d.shape[0],
             padding=[0, grad_x_4d.shape[3] // 2],
            )  # same padding

        S_x /= torch.sum(torch.abs(S_x), dim = (2,3) )
        
        S_x = S_x[0,0]
        S_y = S_x.t()
        
        return S_x, S_y





















