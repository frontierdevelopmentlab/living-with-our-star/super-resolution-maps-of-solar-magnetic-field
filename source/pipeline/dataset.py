"""
Datasets which use index.csv to load files
"""

import os
from random import random, randint

import numpy as np
import pandas as pd
import torch
import torch.nn as nn


from google.cloud import storage
from torch import from_numpy
from torch.utils.data import Dataset
from torchvision import transforms
from skimage.feature import register_translation
from skimage.feature.register_translation import _upsampled_dft
from scipy.ndimage import fourier_shift, gaussian_filter
from skimage.transform import resize

from source.utils import get_logger
from torch.nn import functional as F

logger = get_logger(__name__)

class RandomInversion(object):
    """Invert the given tensor array randomly with a given probability.

    Args:
        p (float): probability of the image being flipped. Default value is 0.5
    """

    def __init__(self, p=0.5):
        self.p = p

    def __call__(self, sample):
        """
        Args:
            img (PIL Image): Image to be flipped.

        Returns:
            PIL Image: Randomly flipped image.
        """
        source, target = sample['input'], sample['target']
        if random() < self.p:
            source = source * -1.0
            target = target * -1.0

        return {'input': source, 'target': target}

    def __repr__(self):
        return self.__class__.__name__ + '(p={})'.format(self.p)


class RandomVerticalFlip(object):
    """Vertically flip the given tensor randomly with a given probability.

    Args:
        p (float): probability of the array being flipped. Default value is 0.5
    """

    def __init__(self, p=0.5):
        self.p = p

    def __call__(self, sample):
        """
        Args:
            numpy.array : Array to be flipped.

        Returns:
            numpy.array: Randomly flipped array.
        """
        source, target = sample['input'], sample['target']
        if random() < self.p:
            source = torch.flip(source, dims=(-2,))
            target = torch.flip(target, dims=(-2,))

        return {'input': source, 'target': target}

    def __repr__(self):
        return self.__class__.__name__ + '(p={})'.format(self.p)


class RandomTranslate(object):
    """Translate the given tensor randomly with a given probability.

    Args:
        p (float): probability of the array being flipped. Default value is 0.5
    """

    def __init__(self, p=0.5):
        self.p = p

    def __call__(self, sample):
        """
        Args:
            numpy.array : Array to be flipped.

        Returns:
            numpy.array: Randomly flipped array.
        """
        source, target = sample['input'], sample['target']
        if random() < self.p:
            shift = randint(0, 4)
            source = from_numpy(np.roll(source.numpy(), shift, axis=-2))

        return {'input': source, 'target': target}

    def __repr__(self):
        return self.__class__.__name__ + '(p={})'.format(self.p)


class RandomHorizontalFlip(object):
    """Horizontally flip the given numpy array randomly with a given probability.

    Args:
        p (float): probability of the array being flipped. Default value is 0.5
    """

    def __init__(self, p=0.5):
        self.p = p

    def __call__(self, sample):
        """
        Args:
            numpy.array : Array to be flipped.

        Returns:
            numpy.array: Randomly flipped array.
        """
        source, target = sample['input'], sample['target']
        if random() < self.p:
            source = torch.flip(source, dims=(-1,))
            target = torch.flip(target, dims=(-1,))

        return {'input': source, 'target': target}

    def __repr__(self):
        return self.__class__.__name__ + '(p={})'.format(self.p)


class LocalMagDataset(Dataset):
    """
    Mag Dataset on local file system.
    """
    def __init__(self, index_filename, rootdir, folder,
                 range_data=None, norm_factor=None, upscale_factor=None,
                 nframe=1, transform=None, target_align=None, asinh=None,
                 filename_source=None, target_downscale=1):
        """

        Parameters
        ----------
        index_filename : str
            Name of index file
        rootdir : str
            Root directory
        folder : str
            Folder within root
        range_data : list or tuple
            Start and end indices to use for data (e.g [0, 10])
        norm_factor : float
            Normalisation factor
        upscale_factor_factor : float
            upscaling factor in case the user wants to donwscale the target.  It will be approximated to a power of 2
        asinh: Boolean
            Transforms input and target data using the inverse hyperbolic cosine
        """
        self.norm_factor = norm_factor
        self.folder = folder
        self.root_dir = rootdir
        self.nframe = nframe
        self.transform = transform
        self.target_align = target_align
        self.asinh = asinh
        self.upscale_factor = None
        self.target_downscale = target_downscale
        if upscale_factor:
            self.upscale_factor = np.power(2, np.floor(np.log2(upscale_factor)))

        # get index
        self.index_file = pd.read_csv('{}/{}/{}'.format(self.root_dir, self.folder,
                                                            index_filename, index_col='index'))

        if filename_source is not None:
            self.index_file = self.index_file.loc[self.index_file.filename_source == filename_source]
            self.index_file.reset_index(inplace=True)
        elif range_data is not None:
            self.index_file = self.index_file.iloc[range_data].reset_index()

        if self.nframe % 2 == 0:
            raise ValueError("The number of frames needs to be odd")

        endpoint = int((self.nframe - 1) / 2)
        index_range = self.index_file.index[endpoint:(len(self.index_file) - endpoint)]

        self.index_target = self.index_file.loc[index_range, :]
        self.index_target.set_index(np.arange(len(self.index_target)), inplace=True)

    def __getitem__(self, idx):

        # prepend instrument for target
        target_name = os.path.join(self.root_dir,
                                   '{}/{}'.format(self.folder,
                                                  self.index_target.loc[idx, 'filename_target']))
        target_image = np.load(target_name)

        if len(target_image.shape) > 2:
            target_image = target_image[0, :, :]

        # # prepend instrument for input
        # if self.target_downscale is not None:
        #     input_stacked = F.avg_pool2d(target_image[None], self.target_downscale)
        #
        # else:
        input_list = []
        for subidx in range(self.nframe):
            input_name = os.path.join(self.root_dir,
                                  '{}/{}'.format(self.folder,
                                                 self.index_file.loc[idx + subidx, 'filename_source']))
            input_image = np.load(input_name)
            input_list.append(input_image)

        input_stacked = np.stack(input_list, axis=0)

        # cannot take multi frames and multi channels at the same time
        if len(input_stacked.shape) == 4:
            input_stacked = input_stacked[0, :, :, :] # take the first frame

        if self.target_align:
            input_stacked, target_image = align_target(input_stacked, target_image)

        input_stacked = from_numpy(input_stacked).float()

        target_image = from_numpy(target_image).float()

        if self.asinh:
            input_stacked = np.arcsinh(input_stacked)
            target_image = np.arcsinh(target_image)

            if self.norm_factor:
                input_stacked[0, :, :] = input_stacked[0, :, :] / np.arcsinh(self.norm_factor)
                target_image = target_image /np.arcsinh(self.norm_factor)

        elif self.norm_factor:
            input_stacked[0, :, :] = input_stacked[0, :, :] / self.norm_factor
            target_image = target_image / self.norm_factor

        if self.upscale_factor:
            real_factor = target_image.shape[0]/input_stacked.shape[1]
            if self.upscale_factor<real_factor:
                target_image = F.avg_pool2d(target_image[None], int(real_factor/self.upscale_factor))[0, :, :]

        input_stacked[input_stacked != input_stacked] = 0
        sample = {'input': input_stacked, 'target': target_image}

        if self.transform:
            sample = self.transform(sample)

        return sample

    def __len__(self):
        return len(self.index_target)

class GCSMagDataset(Dataset):
    """
    Dataset stored on google cloud object store (buckets and blobs)
    """

    def __init__(self, index_filename, bucketname, folder, client, range_data=None,
                 norm_factor=None, upscale_factor=None,
                 nframe=1, transform=None, target_align=None, asinh=None,
                 filename_source=None):
        """

        Parameters
        ----------
        index_filename : str
            Name of index file
        bucketname : str
            Bucket name
        folder : str
            Folder in bucket
        client : `cloud.storage.Client`
            Client for google cloud storage
        range_data : list or tuple
            Start and end indices to use for data (e.g [0, 10])
        norm_factor : float
            Normalisation factor
        upscale_factor_factor : float
            upscaling factor in case the user wants to donwscale the target. It will be approximated to a power of 2
        asinh: Boolean
            Transforms input and target data using the inverse hyperbolic cosine

        Keywords
        nframe: integer
            number of frames: an odd number, the middle one being aligned with the target
        """
        self.norm_factor = norm_factor
        self.client = client
        self.bucket = self.client.get_bucket(bucketname)
        self.folder = folder
        self.nframe = nframe
        self.transform = transform
        self.target_align = target_align
        self.asinh = asinh
        self.upscale_factor = None
        if upscale_factor:
            self.upscale_factor = np.power(2, np.floor(np.log2(upscale_factor)))

        self.index_file = pd.read_csv('gs://{}/{}/{}'.format(bucketname, folder, index_filename),
                                      index_col='index')
        if filename_source is not None:
            self.index_file = self.index_file.loc[self.index_file.filename_source == filename_source]
            self.index_file.reset_index(inplace=True)

        elif range_data is not None:
            self.index_file = self.index_file.iloc[range_data].reset_index()

        if self.nframe % 2 == 0:
            raise ValueError("The number of frames needs to be odd")

        endpoint = int((self.nframe - 1) / 2)
        index_range = self.index_file.index[endpoint:(len(self.index_file) - endpoint)]

        self.index_target = self.index_file.loc[index_range, :]
        self.index_target.set_index(np.arange(len(self.index_target)), inplace=True)

    def __getitem__(self, idx):

        # input
        input_list = []
        for subidx in range(self.nframe):
            blob = storage.Blob('{}/{}'.format(self.folder,
                                           self.index_file.loc[idx + subidx, 'filename_source']),
                            self.bucket)
            blob.download_to_filename('temp.npy')
            input_image = np.load('temp.npy')
            input_list.append(input_image)
            os.remove('temp.npy')

        input_stacked = np.stack(input_list, axis=0)

        # cannot take multi frames and multi channels at the same time
        if len(input_stacked.shape) == 4:
            input_stacked = input_stacked[0, :, :, :]  # take the first frame

        # target
        blob = storage.Blob('{}/{}'.format(self.folder,
                                           self.index_target.loc[idx, 'filename_target']),
                            self.bucket)
        blob.download_to_filename('temp.npy')
        target_image = np.load('temp.npy')
        os.remove('temp.npy')

        # the target should have only one channel:
        if len(target_image.shape) > 2:
            target_image = target_image[0, :, :]

        if self.target_align:
            input_stacked, target_image = align_target(input_stacked, target_image)

        input_stacked = from_numpy(input_stacked).float()
        target_image = from_numpy(target_image).float()

        if self.asinh:
            input_stacked = np.arcsinh(input_stacked)
            target_image = np.arcsinh(target_image)

            if self.norm_factor:
                input_stacked[0, :, :] = input_stacked[0, :, :] / np.arcsinh(self.norm_factor)
                target_image = target_image /np.arcsinh(self.norm_factor)

        elif self.norm_factor:
            input_stacked[0, :, :] = input_stacked[0, :, :] / self.norm_factor
            target_image = target_image / self.norm_factor

        if self.upscale_factor:
            real_factor = target_image.shape[0]/input_stacked.shape[1]
            if self.upscale_factor<real_factor:
                target_image = F.avg_pool2d(target_image[None], int(real_factor/self.upscale_factor))[0, :, :]

        sample = {'input': input_stacked, 'target': target_image}

        if self.transform:
            sample = self.transform(sample)

        return sample

    def __len__(self):
        return len(self.index_target)


def align_target(source, target):
    """
    Align target image to match source image
    """
    # Upsample source
    channel, width, height = source.shape
    scale_factor = target.shape[1]/width
    source_upsampled = F.upsample(torch.tensor(source[0, :, :]).reshape(1, 1, width, height), scale_factor=scale_factor
                                  , mode='bicubic')
    source_upsampled = source_upsampled[0, ...].numpy()
    #np.fft.ifft2(_upsampled_dft(source[0,:,:], upsampled_region_size=target.shape[0], upsample_factor=1)).real

    # Calculate Shift
    shift, error, diffphase = register_translation(source_upsampled[0, ...], gaussian_filter(target, scale_factor))

    # Shift in Fourier space
    target_aligned_dft = fourier_shift(np.fft.fftn(target), shift)

    # Transform back to Image space
    target_aligned = np.fft.ifftn(target_aligned_dft).real

    #logger.info(f'shift: {shift}')

    return source, target_aligned


def bin_ndarray(ndarray, new_shape, operation='sum'):
    """
    Bins an ndarray in all axes based on the target shape, by summing or
        averaging.

    Number of output dimensions must match number of input dimensions and
        new axes must divide old ones.

    Example
    -------
    >>> m = np.arange(0,100,1).reshape((10,10))
    >>> n = bin_ndarray(m, new_shape=(5,5), operation='sum')
    >>> print(n)

    [[ 22  30  38  46  54]
     [102 110 118 126 134]
     [182 190 198 206 214]
     [262 270 278 286 294]
     [342 350 358 366 374]]

    """
    operation = operation.lower()
    if not operation in ['sum', 'mean']:
        raise ValueError("Operation not supported.")
    if ndarray.ndim != len(new_shape):
        raise ValueError("Shape mismatch: {} -> {}".format(ndarray.shape,
                                                           new_shape))
    compression_pairs = [(d, c // d) for d, c in zip(new_shape,
                                                     ndarray.shape)]
    flattened = [l for p in compression_pairs for l in p]
    ndarray = ndarray.reshape(flattened)
    for i in range(len(new_shape)):
        op = getattr(ndarray, operation)
        ndarray = op(-1 * (i + 1))
    return ndarray