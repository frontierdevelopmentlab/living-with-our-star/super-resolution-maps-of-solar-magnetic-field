from abc import ABC, abstractmethod

import matplotlib.pyplot as plt
import torch
import yaml
import os
from sunpy.cm import cm
import numpy as np
from mpl_toolkits.axes_grid1 import ImageGrid
from torch.nn.parallel.data_parallel import DataParallel

from source.pipeline.entropy import Entropy
from source.pipeline.metrics import MeanMetric, StdevMetric, MinMetric, MaxMetric, UnsignedMetric, \
    SignedMetric
from source.pipeline.metrics_gradient import Grad_Sobel
from source.utils import get_logger

_templates = {}  # global registry of template classes

logger = get_logger(__name__)

from torch.nn import functional as F


def template(cls):
    """
    This is a decorator for BaseUpscale-compliant template classes. Place
    `@base_model.template` on the line before a class defintion.

    This makes the class available to BaseUpscale (e.g. for reading saved steps from
    disk) whenever it's imported.

    """
    _templates[cls.__name__] = cls
    return cls


class _CustomDataParallel(DataParallel):
    """
    DataParallel distribute batches across multiple GPUs

    https://github.com/pytorch/pytorch/issues/16885
    """
    def __init__(self, model):
        super(_CustomDataParallel, self).__init__(model)

    def __getattr__(self, name):
        try:
            return super(_CustomDataParallel, self).__getattr__(name)
        except AttributeError:
            return getattr(self.module, name)


class AbstractUpScaler(ABC):
    """
    Abstract class representing an upscaler.
    """

    def __init__(self, net, loss, device, batch_size, nepochs, target_downscale, learning_rate, upscale_factor, n_frames):
        self.net = net.to(device)
        self.loss = loss
        self.device = device
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.nepochs = nepochs
        self.n_frames = n_frames
        self.upscale_factor = upscale_factor

    @abstractmethod
    def forward(self, input):
        """
        Return forward pass on input.

        Parameters
        ----------
        input: `torch.tensor`

        """
        pass

    @abstractmethod
    def get_loss(self, target, output):
        """
        Return loss function evaluated on output and target

        Parameters
        ----------
        target : `torch.tensor`
            Target or 'ground truth'

        output : `torch.tensor`
            Output from model
        """
        pass


class BaseScaler(AbstractUpScaler):
    """
    Wrap together the architecture -- net --, the loss -- loss -- and optimization
    parameters and add a train, forward and test methods

    Parameters
    ----------
    net: class
        model architecture inherited from TemplateModel
    loss: class
        loss class inherited from TemplateLoss
    device: bool
        True if GPU else False
    batch_size: int
        batch size
    learning rate: float
        learning rate

    kwargs (specified by =)
    ------
    beta1: float
        first parameter of Adam optimizer
    is_gradient: bool
        True if using gradient descent to optimize parameters

    """

    def __init__(self, net, loss, device, batch_size, nepochs, learning_rate, n_frames, target_downscale, upscale_factor, beta1=0.5,
                 is_gradient=True):

        super().__init__(net, loss, device, batch_size, nepochs, learning_rate, target_downscale, upscale_factor, n_frames=n_frames)

        self.is_gradient = is_gradient

        if is_gradient:  # switch off gradient descent for non-gradient method
            self.optimizer = torch.optim.Adam(self.net.parameters(), lr=learning_rate,
                                              betas=(beta1, 0.999))

        metric_mean = MeanMetric()
        metric_stdev = StdevMetric()
        metric_min = MinMetric()
        metric_max = MaxMetric()
        metric_unsigned = UnsignedMetric()
        metric_signed = SignedMetric()

        self.metrics = [metric_mean, metric_stdev, metric_min, metric_max, metric_unsigned,
                        metric_signed]

    @classmethod
    def from_config(cls, config_file):
        """
        Create a model input configuration from a saved yaml file
            
        Parameters
        ----------
        config_file : configuration file (yaml) path

        """

        with open(config_file, 'r') as stream:
            config_dict = yaml.load(stream, Loader=yaml.SafeLoader)

        base_scaler = cls.from_dict(config_dict)

        return base_scaler

    @classmethod
    def from_dict(cls, config_dict):
        """
        Create a model input configuration from dictionary.

        Parameters
        ----------
        config_dict

        Returns
        -------

        """
        net_config = config_dict['net']  # dictionary defining the architecture

        net = _templates[net_config['name']].from_config(net_config)

        if torch.cuda.device_count() > 1:
            net = _CustomDataParallel(net)

        config_loss = config_dict['loss']
        if isinstance(config_loss, dict):
            # Add device
            config_loss['device'] = ('cuda' if config_dict['cuda_device'] else 'cpu')
            # Add Normalization
            config_loss['normalisation'] = config_dict['data']['normalisation']
            loss = _templates[config_loss['name']].from_dict(config_loss)
            logger.info(loss)
        else:
            loss = _templates[config_dict['loss']]()

        batch_size = config_dict['batch_size']
        device = torch.device("cuda" if config_dict['cuda_device'] else "cpu")
        is_gradient = config_dict['is_gradient']
        learning_rate = config_dict['learning_rate']
        beta1 = config_dict['beta1']
        nepochs = config_dict['nepochs']
        n_frames = net_config['n_frames']
        upscale_factor = net_config['upscale_factor']

        target_downscale = 1
        if 'target_downscale' in net_config:
            target_downscale = net_config['target_downscale']

        base_scaler = cls(
            net,
            loss,
            device,
            batch_size,
            nepochs,
            learning_rate,
            n_frames,
            upscale_factor,
            target_downscale,
            beta1=beta1,
            is_gradient=is_gradient
        )

        base_scaler.config = config_dict
        return base_scaler

    def forward(self, x):

        """
        Takes x and computes f(x); takes low res and computes high res for one forward pass of the model. 
        """

        return self.net.forward(x)

    def get_loss(self, target, output):
        return self.loss(target, output)

    def optimize_parameters(self, x, target):
        """

        Parameters
        ----------
        x
        target

        Returns
        -------

        """
        output = self.forward(x)

        # For the case when there is no optim
        loss = self.get_loss(target, output)

        if self.is_gradient:
            self.optimizer.zero_grad()  # clear network existing gradient
            loss.backward()  # calculate gradients for network
            self.optimizer.step()  # update gradients for network

        if self.loss.name == 'SSIMGradHistLossUnc':
            output = output[:, 0, ...]

        output_metrics, target_metrics = self.calculate_metrics(output, target)

        return output, output_metrics, target_metrics, loss

    def calculate_metrics(self, output, target):
        """
        Calculate metrics set in `self.metrics`

        Parameters
        ----------
        output
        target

        Returns
        -------
        tuple : Two dictionaries containing output and target metrics
        """
        output_metrics = {}
        target_metrics = {}

        for metric in self.metrics:
            if metric.name not in ['SSIM', 'Grad_Sobel', 'PNSR', 'PearsonCorrelation',
                                   'SpearmanCorrelation', 'PearsonCorrelation_15' ]:
                output_metrics[metric.name] = metric(output)
                target_metrics[metric.name] = metric(target)

        return output_metrics, target_metrics
    

    def train(self, train_dataloader, val_dataloader, writer, chkpt_dir):

        best_loss = 1000.

        for epoch in range(self.nepochs):

            train_loss = 0
            val_loss = 0

            train_metrics = {'Grad_Sobel':0}
            val_metrics = {'Grad_Sobel':0}

            for metric in self.metrics:
                train_metrics[metric.name] = 0
                val_metrics[metric.name] = 0

            self.net.train()
            for iteration, batch in enumerate(train_dataloader, self.batch_size):
                input = batch['input'].to(self.device)
                target = batch['target'].to(self.device)

                output, output_metrics, target_metrics, loss = self.optimize_parameters(input,
                                                                                        target)
                train_loss += loss.detach() * len(input) / len(train_dataloader.dataset)

                current_diff_metrics = log_mad(output_metrics, target_metrics, writer, epoch,
                                               phase='Train', logHist = True)
                for metric_name, metric_value in current_diff_metrics.items():
                    train_metrics[metric_name] += \
                        metric_value.detach() * len(input) / len(train_dataloader.dataset)

            log_magnetograms('Magnetograms/train/target', target, writer, epoch,
                             self.config.get('data').get('normalisation'),
                             asinh=self.config.get('data').get('asinh'))
            log_magnetograms('Magnetograms/train/output', output, writer, epoch,
                             self.config.get('data').get('normalisation'),
                             asinh=self.config.get('data').get('asinh'))

            writer.add_scalar("Loss/Train", train_loss, epoch)
            log_diff_metrics(train_metrics, writer, epoch, phase='Train')

            logger.info(f'Epoch: {epoch} Train loss: {train_loss}')

            self.net.eval()
            for iteration, batch in enumerate(val_dataloader, int(self.batch_size)):
                input = batch['input'].to(self.device)
                target = batch['target'].to(self.device)

                output = self.forward(input)

                loss = self.get_loss(target, output)
                val_loss += loss.detach() * len(input) / len(val_dataloader.dataset)

                if self.loss.name == 'SSIMGradHistLossUnc':
                    output = output[:, 0, ...]

                output_metrics, target_metrics = self.calculate_metrics(output, target)

                current_diff_metrics = log_mad(output_metrics, target_metrics, writer, epoch,
                                               phase='Validation', logHist = True)
                for metric_name, metric_value in current_diff_metrics.items():
                    val_metrics[metric_name] += \
                        metric_value.detach() * len(input) / len(val_dataloader.dataset)

            log_magnetograms('Magnetograms/val/target', target, writer, epoch,
                             self.config.get('data').get('normalisation'),
                             asinh=self.config.get('data').get('asinh'))
            log_magnetograms('Magnetograms/val/output', output, writer, epoch,
                             self.config.get('data').get('normalisation'),
                             asinh=self.config.get('data').get('asinh'))

            writer.add_scalar("Loss/Validation", val_loss, epoch)
            log_diff_metrics(val_metrics, writer, epoch, phase='Validation')

            logger.info(f'Epoch: {epoch} Validation loss: {val_loss}')

            if self.is_gradient:
                model_dict = {'epoch': epoch,
                            'loss': loss,
                            'model_state_dict': self.net.state_dict(),
                            'optimizer_state_dict': self.optimizer.state_dict()}
            
                if val_loss < best_loss:
                    best_loss = val_loss

                torch.save(model_dict,
                           f'{chkpt_dir}/epoch_{epoch}'
                            )

        ### torch.load()

    def evaluation(self, testloader, writer, logging_dir): # pass in filename to load model

        # eval_loss_list = []
        eval_loss = 0

        counter = 0

        for iteration, batch in enumerate(testloader, self.batch_size):
            input = batch['input'].to(self.device)
            target = batch['target'].to(self.device)
            output = self.forward(input)

            eval_loss += self.get_loss(target, output).item()

            if counter == 0:
                upload_results(writer, input, output, target, self.config.get('data').get('normalisation'),
                             self.config.get('data').get('asinh'), logging_dir)

            counter += 1

            # eval_loss_list.append(eval_loss)

        mean_eval = eval_loss / counter  # torch.mean(torch.FloatTensor(eval_loss_list))
            
        return mean_eval


def calculate_entropy(input_, output_, target_):
    """
    Calculate entropy

    Parameters
    ----------
    input_: input images
    output_: output images
    target_: target images

    Returns
    -------
    dict : Entropy info for input, output and target

    """
    entropy_ = Entropy(device=output_.device)
    bins_in, ker_size_in, entropies_in = entropy_.forward(input_)
    bins_out, ker_size_out, entropies_out = entropy_.forward(output_)
    bins_tar, ker_size_tar, entropies_tar = entropy_.forward(target_)

    input_dict = {'bins': bins_in, 'ker_size': ker_size_in, 'entropies': entropies_in}
    output_dict = {'bins': bins_out, 'ker_size': ker_size_out, 'entropies': entropies_out}
    target_dict = {'bins': bins_tar, 'ker_size': ker_size_tar, 'entropies': entropies_tar}

    return input_dict, output_dict, target_dict


def log_mad(output_metrics, target_metrics, writer, epoch, phase, logHist = False):
    """
    Log metrics

    Parameters
    ----------
    output_metrics: dictionary of metrics
    target_metrics: dictionary of metrics
    writer: Tensorboard writer
    epoch
    phase: either "Train" or "Validation"
    logHist

    Returns
    -------
    current_diff_metrics: dictionary of differences of metrics between output and target
    """

    current_diff_metrics = {}
    for key in output_metrics.keys():
        if key not in ['bins', 'ker_size_out', 'entropies']:
            current_diff_metrics[key] = \
                torch.mean(torch.abs(target_metrics[key]-output_metrics[key]))
            if logHist:
                writer.add_histogram(key + '_output/' + phase, output_metrics[key], epoch)
                writer.add_histogram(key + '_target/' + phase, target_metrics[key], epoch)
    return current_diff_metrics


def log_diff_metrics(metric_dict, writer, epoch, phase):
    """
    Log difference of metrics

    Parameters
    ----------
    metric_dict: dictionary of metrics
    writer: Tensorboard writer
    epoch
    phase: either "Train" or "Validation"
    """

    for key in metric_dict.keys():
        if key not in ['bins', 'ker_size_out', 'entropies']:
            writer.add_scalar(key + '/' + phase, metric_dict[key], epoch)


def log_magnetograms(tag, tensor, writer, epoch, norm, asinh=None):
    """
    Log magnetogram images

    Parameters
    ----------
    tag: str with descriptor
    tensor: tensor with data [batch, width, height]
    writer: Tensorboard writer
    epoch
    norm: number to normalize data
    """

    if len(tensor.shape) == 3:
        data = tensor[0, :, :].cpu().detach().numpy()
    else:
        data = tensor[0, 0, :, :].cpu().detach().numpy()

    if asinh:
        if norm:
            data = data*np.arcsinh(norm).item()
        data = np.sinh(data)
    elif norm:
        data *= norm

    fig, ax = plt.subplots()
    ax.imshow(data, cmap=cm.hmimag, vmin=-1500.0, vmax=1500.0)
    writer.add_figure(tag, fig, epoch)
    

def extract_magnetograms(tensor, asinh=None, norm=1):
    """
    Extract first and last magnetogram from a batch

    Parameters
    ----------
    tensor: tensor with data [batch, width, height] or [batch, channel, width, height]
    norm: number to normalize data
    
    Returns
    -------
    data_f: first image in the batch
    data_l: last image in the batch
    """  
    
    if len(tensor.shape) == 3:
        data = tensor
    elif len(tensor.shape) == 4:
        data = tensor[:, 0, :, :]

    if asinh:
        if norm:
            data = data * np.arcsinh(norm).item()
        data = torch.sinh(data)
    elif norm:
        data *= norm
        
    return data

    
def upload_results(writer, input_, output_, target_, norm, asinh, logging_dir):
    """
    Compile results and save graphs

    Parameters
    ----------
    writer:
    input:
    output:
    target:    
    """  
    
    # Plot Entropy

    input_mag = extract_magnetograms(input_, asinh=asinh, norm=norm)
    output_mag = extract_magnetograms(output_, asinh=asinh, norm=norm)
    target_mag = extract_magnetograms(target_, asinh=asinh, norm=norm)

    in_batch,in_width,in_height = input_mag.shape
    input_mag_upsampled = F.upsample(input_mag.reshape(in_batch,1,in_width, in_height), size=target_mag.shape[-2:],
                                     mode='bicubic', align_corners=False)

    input_mag_upsampled = input_mag_upsampled[:, 0, :, :]
    
    input_entropy, output_entropy, target_entropy = calculate_entropy(input_mag_upsampled, output_mag, target_mag)

    fig2, ax2 = plt.subplots(figsize=(10,7))
    ax2.semilogx(output_entropy['ker_size'], output_entropy['entropies'], label='Output', linewidth=2, color='#ffa232', linestyle='-')
    ax2.semilogx(input_entropy['ker_size'], input_entropy['entropies'], label='Bicubic', linewidth=2, color='#a61aa4', linestyle='--')
    ax2.semilogx(target_entropy['ker_size'], target_entropy['entropies'], label='Target', linewidth=2, color='#006600', linestyle=':')
    plt.xlabel('Kernel Size', fontsize = 20)
    plt.ylabel('Entropy', fontsize = 20)
    plt.tick_params(labelsize=20, direction='in', axis='both', which='major', length=8, width=2)
    plt.tick_params(labelsize=20, direction='in', axis='both', which='minor', length=4, width=2)
    plt.legend(fontsize = 20)
    writer.add_figure('Entropy', fig2)

    # Plot Histogram

    input_hist = torch.histc(input_mag_upsampled.cpu(), bins=2 ** 8, min=-750, max=750) #min=-1500, max=1500
    output_hist = torch.histc(output_mag.cpu(), bins=2 ** 8, min=-750, max=750)
    target_hist = torch.histc(target_mag.cpu(), bins=2 ** 8, min=-750, max=750)

    #x_bin = np.arange(-1500, 1500, 3000 / 2 ** 8)
    x_bin = np.arange(-750, 750, 1500/len(output_hist))

    fig3, ax3 = plt.subplots(figsize=(14, 5)) #(10, 7)
    ax3.semilogy(x_bin, output_hist.detach().numpy(), label='Output', linewidth=3, color='#ffa232', linestyle='-')
    ax3.semilogy(x_bin, input_hist.detach().numpy(), label='Bicubic', linewidth=3, color='#a61aa4', linestyle='--')
    ax3.semilogy(x_bin, target_hist.detach().numpy(), label='Target', linewidth=3, color='#006600', linestyle=':')
#    ax3.semilogy(x_bin, target_hist.detach().numpy() - output_hist.detach().numpy(), label='Diff', linewidth=3)
    plt.xlabel('Magnetic Field Value [Gauss]', fontsize=16)
    plt.ylabel('Frequency', fontsize=20)
    plt.tick_params(labelsize=20, direction='in', axis='both', which='major', length=8, width=2)
    plt.tick_params(labelsize=20, direction='in', axis='both', which='minor', length=4, width=2)
    plt.legend(fontsize=20)
    plt.xlim(-600, 600)
    writer.add_figure('Distribution Histogram/Semilogy', fig3)

    # Plot Correlation Plot

    output_corr = torch.reshape(output_mag, (output_mag.shape[0], output_mag.shape[1]**2))
    target_corr = torch.reshape(target_mag, (target_mag.shape[0], target_mag.shape[1]**2))
    fig5, ax5 = plt.subplots(figsize=(7,7))

    for n in range(0,output_mag.shape[0]):
        ax5.plot(target_corr[n,:].detach().cpu().numpy(), output_corr[n,:].detach().cpu().numpy(), color='000000', marker='.', linewidth=0, alpha=0.1)

    x_bin = np.arange(-400, 600,10)
    ax5.plot(x_bin, x_bin, color='red', linewidth=3)

    plt.xlabel('Target Mag. Field [Gauss]', fontsize = 20)
    plt.ylabel('Output Mag. Field [Gauss]', fontsize = 20)
    plt.tick_params(labelsize=20, direction='in', axis='both', which='major', length=8, width=2)
    plt.tick_params(labelsize=20, direction='in', axis='both', which='minor', length=4, width=2)
    plt.xlim(-400, 400)
    plt.ylim(-400, 400)
    #plt.legend(fontsize = 20)
    writer.add_figure('Correlation_Output', fig5)

    input_corr = torch.reshape(input_mag_upsampled, (input_mag_upsampled.shape[0], input_mag_upsampled.shape[1]**2))
    target_corr = torch.reshape(target_mag, (target_mag.shape[0], target_mag.shape[1]**2))
    fig6, ax6 = plt.subplots(figsize=(7,7))

    for n in range(0,input_mag.shape[0]):
        ax6.plot(target_corr[n,:].detach().cpu().numpy(), input_corr[n,:].detach().cpu().numpy(), color='blue', marker='.', linewidth=0, alpha=0.1)

    x_bin = np.arange(-400, 600,10)
    ax6.plot(x_bin, x_bin, color='red', linewidth=3)

    plt.xlabel('Target Mag. Field [Gauss]', fontsize = 20)
    plt.ylabel('Bicubic Mag. Field [Gauss]', fontsize = 20)
    plt.tick_params(labelsize=20, direction='in', axis='both', which='major', length=8, width=2)
    plt.tick_params(labelsize=20, direction='in', axis='both', which='minor', length=4, width=2)
    plt.xlim(-400, 400)
    plt.ylim(-400, 400)
    #plt.legend(fontsize = 20)
    writer.add_figure('Correlation_Bicubic', fig6)
        
    # Plot Magnetograms
            
    b_size = output_.shape[0]
    
    if b_size > 10:
        b_size = 10
        
    fig1 = plt.figure(figsize = (15,5))
    grid1 = ImageGrid(fig1, 111, (4,b_size), axes_pad=0.05,
                  aspect=True, share_all=True, label_mode = 'L',
                  cbar_location="right", cbar_mode="single")
    
    for n in range(b_size):
        in_ = grid1[n].imshow(input_mag_upsampled[n, :, :].detach().cpu().numpy(), cmap=cm.hmimag, vmin=-1500.0, vmax=1500.0, aspect='equal')
        # Inset for original image input in the Bicubic imshow plot.
        # in_ = grid1[n].imshow(input_mag[n, :, :].detach().cpu().numpy(), cmap=cm.hmimag, vmin=-1500.0, vmax=1500.0, aspect='equal')
        grid1[n].get_xaxis().set_visible(False)
        grid1[n].get_yaxis().set_visible(False)
        
        out_ = grid1[b_size + n].imshow(output_mag[n, :, :].detach().cpu().numpy(), cmap=cm.hmimag, vmin=-1500.0, vmax=1500.0)
        grid1[b_size + n].get_xaxis().set_visible(False)
        grid1[b_size + n].get_yaxis().set_visible(False)

        tar_ = grid1[2*b_size + n].imshow(target_mag[n, :, :].detach().cpu().numpy(), cmap=cm.hmimag, vmin=-1500.0, vmax=1500.0)
        grid1[2*b_size + n].get_xaxis().set_visible(False)
        grid1[2*b_size + n].get_yaxis().set_visible(False)
        
        diff_ = grid1[3*b_size + n].imshow(target_mag[n, :, :].detach().cpu().numpy() -
                                           output_mag[n, :, :].detach().cpu().numpy(), cmap=cm.hmimag, vmin=-1500.0,
                                           vmax=1500.0)
        grid1[3*b_size + n].get_xaxis().set_visible(False)
        grid1[3*b_size + n].get_yaxis().set_visible(False)
        
    
    grid1.cbar_axes[0].colorbar(tar_) 
    
    #grid1[0].get_xaxis().set_visible(True)
    grid1[0].get_yaxis().set_visible(True)
    grid1[b_size].get_yaxis().set_visible(True)
    grid1[2*b_size].get_yaxis().set_visible(True)
    grid1[3*b_size].get_yaxis().set_visible(True)
    grid1[3*b_size].get_xaxis().set_visible(True)
    
    plt.tick_params(labelsize=16, direction='in', axis='both', which='major', length=8, width=2)
    plt.tick_params(labelsize=16, direction='in', axis='both', which='minor', length=4, width=2) 
    
    fig1.text(0,0.8, 'Bicubic', fontsize=16)
    fig1.text(0,0.6, 'Output', fontsize=16)
    fig1.text(0,0.4, 'Target', fontsize=16)
    fig1.text(0,0.2, 'Difference', fontsize=16)
     
    plt.tight_layout()
    
    writer.add_figure('Magnetogram Comparison', fig1)

    # Save figures into Summaries folder

    os.makedirs(f'{logging_dir}/outputs/')

    fig1.savefig(f'{logging_dir}/outputs/Magnetograms.jpg', dpi=1000)
    fig2.savefig(f'{logging_dir}/outputs/Entropy.jpg', dpi=800)
    fig3.savefig(f'{logging_dir}/outputs/Histogram.jpg', dpi=800)
    fig5.savefig(f'{logging_dir}/outputs/Correlation_Output.jpg', dpi=800)
    fig6.savefig(f'{logging_dir}/outputs/Correlation_Bicubic.jpg', dpi=800)
