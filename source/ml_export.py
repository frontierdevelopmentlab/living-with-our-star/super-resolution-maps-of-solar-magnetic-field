import itertools
import argparse
import datetime
import os
import random
from collections.abc import Iterable
from concurrent.futures import ProcessPoolExecutor, wait

import astropy.units as u
import dask.bytes
import numpy as np
import pandas as pd
from astropy.coordinates import SkyCoord
from sunpy.map import Map

from google.cloud import storage
from pandas import Grouper
from skimage.transform import resize
from sklearn.feature_extraction import image
from source.prep import map_prep
from source.utils import get_logger, file_exist
from source.patch_stitch import get_patch


logger = get_logger(__name__)

BUCKET_MAP = {'512': 'fdl-mag-512',
              'MDI': 'fdl-mag-mdi',
              'MWO': 'fdl-mag-mt-wilson',
              'HMI': 'fdl-mag-sdo',
              'SPMG': 'fdl-mag-spmg',
              'SOLIS': 'fdl-mag-solis',
              'GONG': 'fdl-mag-gong',
              'SOT': 'fdl-mag-sot'}

def flatten(iterable):
    """

    Parameters
    ----------
    iterable

    Returns
    -------
    a flat list of items

    """
    flat = []
    for item in iterable:
        if isinstance(item, Iterable):
            flat += [x for x in item]
        else:
            flat.append(item)

    return flat


def train_test_split(index_df):
    """
    Create train, test, validate split. Currently breaks data down into years and selects two
    random months for testing and validation.

    Parameters
    ----------
    index_df : `pandas.DataFrame`
        The index df produce while creating the preped data
    Returns
    -------
    `dict()` : contain key of train, test, validate

    """
    train = []
    test = []
    validate = []

    yr_swtch = True
    for _, year in index_df.groupby(Grouper(key='dateobs', freq='Y')):
        months = list(year.groupby(Grouper(key='dateobs', freq='M')))
        if len(months) > 3:
            if yr_swtch:
                yr_swtch = False
                cur_test = months.pop(random.randrange(len(months)))
            else:
                cur_test = months.pop(3)

            cur_validate = months.pop(random.randrange(len(months)))
            train.extend(months)
            # These are tuples so need to append instead of extend
            test.append(cur_test)
            validate.append(cur_validate)

    return {'train': train, 'test': test, 'validate': validate}


def time_alignment(index_df, instrument_left, instrument_right, tolerance):
    """
    find time closest match for instrument_left among
    magnetograms in instrumetn_right within a predefined 
    time tolerance

    Parameters
    ----------
    index_df : `pandas.DataFrame`
        The index df produce while creating the preped data

    instrument_left: string
        instrument for which matches need to be found

    instrument_right: string
        instrument whose magnetograms form the matches

    tolerance: float
        time tolerance in seconds

    Returns
    -------
    pandas.DataFrame : filepaths of time-aligned magnetograms

    """

    instrument1_df = index_df[(index_df.instrument == instrument_left)].sort_values(by='dateobs')
    instrument2_df = index_df[(index_df.instrument == instrument_right)].sort_values(by='dateobs')
    instrument2_df['dateobsT'] = instrument2_df['dateobs']

    inst1_inst2 = pd.merge_asof(instrument1_df[['dateobs', 'filepath']],
                                instrument2_df[['dateobs', 'filepath', 'dateobsT']],
                                on='dateobs',
                                suffixes=('_{}'.format(instrument_left),
                                '_{}'.format(instrument_right)),
                                allow_exact_matches=True,
                                tolerance=pd.Timedelta(seconds=tolerance))

    inst1_inst2['timedelta'] = inst1_inst2['dateobs']-inst1_inst2['dateobsT']

    inst1_inst2.dropna(inplace=True)
    return inst1_inst2


def downsample(amap, scale):
    """
    Downsample map by scale factor

    Parameters
    ----------
    amap: sunpy map

    scale: integer scale factor

    Returns
    -------
    amap_downscaled: downscaled map

    """

    amap_data = amap.data
    amap_downscaled = resize(amap_data, (amap_data.shape[0] / scale, amap_data.shape[1] / scale),
                             anti_aliasing=True)

    # update headers image scale and number of pixels
    new_meta = amap.meta.copy()

    scale_factor_x = scale
    scale_factor_y = scale


    # Update metadata
    new_meta['cdelt1'] *= scale_factor_x
    new_meta['cdelt2'] *= scale_factor_y

    if 'CD1_1' in new_meta:
        new_meta['CD1_1'] *= scale_factor_x
        new_meta['CD2_1'] *= scale_factor_x
        new_meta['CD1_2'] *= scale_factor_y
        new_meta['CD2_2'] *= scale_factor_y

    new_meta['crpix1'] = (amap_data.shape[0] / scale + 1)/ 2.
    new_meta['crpix2'] = (amap_data.shape[1] / scale + 1)/ 2.

    center = amap.center

    new_meta['crval1'] = center.Tx.value
    new_meta['crval2'] = center.Ty.value

    return Map(amap_downscaled, new_meta)


def crop(amap, width, height, instrument='HMI', target_plate=0.504273):
    """
    Crop map to from disk center by arcsecond matching dim

    Parameters
    ----------
    amap: sunpy map

    width: float
        width of the cropping patch

    height: float
        height of the cropping patch

    Returns
    -------
    crop_map: cropped map to dimension x dimension from disk center

    """
    if instrument == 'HMI':
        scale_x = amap.meta['cdelt1']
        scale_y = amap.meta['cdelt2']
    else:
        scale_x = target_plate
        scale_y = target_plate

    width_arcsec = scale_x * width * u.arcsec
    height_arcsec = scale_y * height * u.arcsec

    bottom_left = SkyCoord(- width_arcsec / 2, - height_arcsec / 2, frame=amap.coordinate_frame)
    top_right = SkyCoord(width_arcsec / 2, height_arcsec / 2, frame=amap.coordinate_frame)

    # crop the center of the sun (check the projection)
    crop_map = amap.submap(bottom_left, top_right)

    return crop_map

def get_files_from_index(index_df, instrument1, instrument2):
    """
    Get the file system for any instrument using the data_index.csv

    Parameters
    ----------
    index_df: pandas.DataFrame()

    Returns
    -------
    a list of tuple with dask type files object and instrument name

    """

    path_list = list(index_df['filepath_{}'.format(instrument1)]) + \
                list(index_df['filepath_{}'.format(instrument2)])

    instrument_list = list([instrument1] * len(index_df)) + \
                      list([instrument2] * len(index_df))

    date_list = list(index_df['dateobs']) + \
                list(index_df['dateobs'])
    
    path_list = ['gs://{}'.format(filename.replace('\\', "/")) for filename in path_list]
    return zip(dask.bytes.open_files(path_list), instrument_list, date_list)


def upload_npy(amapdata, instrument, date, destination, folder, suffix=None, compressed=False):
    """
    Download maps from bucket, replace nans, crop and reupload

    Parameters
    ----------
    amapdata: numpy array

    destination: string
        bucket where to upload data
    
    instrument: string
        
    date: datetime object
        date to be appended to the filename

    Returns
    string: filename
    -------
    """

    client = storage.Client()
    destination_bucket = client.get_bucket(destination)

    if compressed:
        extension = 'npz'
    else:
        extension = 'npy'

    if suffix is None:
        filename = '{}_{}.{}'.format(instrument, date.strftime("%Y%m%d-%H%M%S"), extension)
    else:
        filename = '{}_{}_{}.{}'.format(instrument, date.strftime("%Y%m%d-%H%M%S"), suffix, extension)

    if compressed:
        np.savez_compressed(filename, amapdata)
    else:
        np.save(filename, amapdata)

    blob = destination_bucket.blob('{}/{}'.format(folder, filename))
    blob.upload_from_filename(filename)
    logger.info(filename)
    os.remove(filename)

    return filename


def upload_index(data, destination):

    """
    Upload a pandas dataframe with metrics and an index for each file in destination/folder

    Parameters
    ----------
    data: pandas dataframe
        metrics, date and filename
    destination: string
        name of the bucket destination to load index file

    Returns
    -------

    """
    client = storage.Client()
    bucketname = destination.split('/', 1)[0]
    folder = destination.split('/', 1)[1]

    if file_exist(bucketname, '{}/index.csv'.format(folder), client):
        index_df = pd.read_csv('gs://{}/index.csv'
                               .format(destination),
                               index_col='index')
        index_df['dateobs'] = pd.to_datetime(index_df['dateobs'])
        data = pd.concat([data, index_df]).drop_duplicates('filename_source')

    data.sort_values(by='dateobs', inplace=True)
    data.set_index(np.arange(len(data)), inplace=True)
    data.index.name = 'index'

    data.to_csv('gs://{}/index.csv'.format(destination))


def data_export(instrument1,
                instrument2,
                tolerance,
                destination_bucket,
                cropscale,
                downscale=None,
                nworkers=4, compressed=False):
    """
        Aligns instrument1 and instrument2 by their time with a defined
        tolerance and split aligned data into test/train/validate
        Downscale instrument if instrument1==instrument2 by a defined factor
        Crop magnetograms to patches of size cropscale around the center
        of the sun
        Upload to destination bucket fdl-mag-to-ml

        Parameters
        ----------
        instrument1: basestring
            source instrument

        instrument2: basestring
            target instrument

        tolerance: float
            time tolerance in seconds when aligning magnetograms by time

        destination_bucket: basestring
            bucket to upload the patches for ml

        cropscale: tuple
            width, height in pixels

        downscale: int
            HR width in pixels/LR width in pixels

        nworkers: int
            number of concurrent workers

        Returns
        -------
        inst1_inst2: pandas dataframe
            index of aligned magnetograms

        """

    index_filepath1 = "gs://{}/index.csv".format(BUCKET_MAP[instrument1] + '-prepped')
    index_df1 = pd.read_csv(index_filepath1)
    
    index_filepath2 = "gs://{}/index.csv".format(BUCKET_MAP[instrument2] + '-prepped')
    index_df2 = pd.read_csv(index_filepath2)
    
    if (instrument1 == instrument2) & (downscale is not None):
        instrument1= 'LR{}_'.format(downscale) + instrument1
        instrument2 = 'HR_' + instrument2

    elif instrument1 == instrument2:
        raise ValueError('If using same instrument for source and target, '
                         'need a downscale factor')

    if downscale is not None:
        downscale_name = int(downscale)
    else:
        downscale_name = 1

    destination_folder = '{}_{}_{}_{}_{}'.format(instrument1,
                                                 instrument2,
                                                 downscale_name,
                                                 cropscale[0],
                                                 cropscale[1])

    index_df1['instrument'] = instrument1
    index_df2['instrument'] = instrument2

    index_df = pd.concat([index_df1, index_df2], axis=0)
    index_df.dropna(subset=['dateobs'], inplace=True)

    index_df['dateobs'] = pd.to_datetime(index_df['dateobs'])

    # time alignment
    inst1_inst2_index = time_alignment(index_df, instrument1, instrument2, tolerance)

    # train/test/validate split
    train_test_val_dict = train_test_split(inst1_inst2_index)

    # files handles corresponding to aligned index

    with ProcessPoolExecutor(max_workers=nworkers) as executor:
        for type_df, df_time_list in train_test_val_dict.items():
            d_list = [df_time[1] for df_time in df_time_list]
            df = pd.concat(d_list)
            
            file_handles = get_files_from_index(df, instrument1, instrument2)
            futures = []

            for handle, instrument, date in file_handles:
                if (instrument == instrument1) & (downscale is not None):
                    futures.append(executor.submit(
                                    run_export, handle, destination_bucket,
                                                '{}/{}'.format(destination_folder, type_df),
                                                instrument,
                                                date,
                                                cropscale=(cropscale[0] / downscale, cropscale[1] / downscale),
                                                downscale=downscale,
                                                compressed=compressed))
                else:
                    futures.append(executor.submit(
                        run_export, handle,
                        destination_bucket,
                        '{}/{}'.format(destination_folder, type_df),
                        instrument,
                        date,
                        cropscale=cropscale,
                        downscale=None,
                        compressed=compressed))

            wait(futures)

            results = []
            for f in futures:
                try:
                    results.append(f.result())
                except:
                    logger.info(handle.path)

            results_flat = itertools.chain.from_iterable(results)

            exported_files = pd.DataFrame(results_flat)
            exported_files_1 = exported_files[exported_files['instrument'] == instrument1]
            exported_files_2 = exported_files[exported_files['instrument'] == instrument2]

            exported_files = pd.merge(exported_files_1,
                                          exported_files_2,
                                          on='dateobs',
                                          suffixes=('_source', '_target'))

            # upload/update index
            upload_index(exported_files, '{}/{}/{}'.format(destination_bucket, destination_folder, type_df))

    return inst1_inst2_index


def data_export_patches(instrument1,
                instrument2,
                tolerance,
                destination_bucket,
                patchsize,
                patchstride=None,
                downscale=None,
                nworkers=4,
                year_start=None,
                year_end=None,
                month_start=None,
                month_end=None,
                compressed=True):
    """
        Aligns instrument1 and instrument2 by their time with a defined
        tolerance and split aligned data into test/train/validate
        Downscale instrument if instrument1==instrument2 by a defined factor
        Crop magnetograms to patches of size cropscale around the center
        of the sun
        Upload to destination bucket fdl-mag-to-ml

        Parameters
        ----------
        instrument1: basestring
            source instrument

        instrument2: basestring
            target instrument

        tolerance: float
            time tolerance in seconds when aligning magnetograms by time

        destination_bucket: basestring
            bucket to upload the patches for ml

        patchsize: int
            size of patches

        patchstride: int
            stride of patching (if None, it will be set to patchsize and
            there will be no overlap)

        downscale: int
            HR width in pixels/LR width in pixels

        nworkers: int
            number of concurrent workers

        Returns
        -------
        inst1_inst2: pandas dataframe
            index of aligned magnetograms

        """

    index_filepath1 = "gs://{}/index.csv".format(BUCKET_MAP[instrument1] + '-prepped')
    index_df1 = pd.read_csv(index_filepath1)

    index_filepath2 = "gs://{}/index.csv".format(BUCKET_MAP[instrument2] + '-prepped')
    index_df2 = pd.read_csv(index_filepath2)

    if (instrument1 == instrument2) & (downscale is not None):
        instrument1 = 'LR{}_'.format(downscale) + instrument1
        instrument2 = 'HR_' + instrument2

    elif instrument1 == instrument2:
        raise ValueError('If using same instrument for source and target, '
                         'need a downscale factor')

    if downscale is not None:
        downscale_name = int(downscale)
    else:
        downscale_name = 1

    if patchstride is None:
        patchstride = patchsize

    else:
        raise ValueError("You need to either get one patch at the center (cropscale argument) or "
                         "multiple patches (patchscale argument)")

    destination_folder = '{}_{}_{}_patch_all{}_{}'.format(instrument1,
                                                 instrument2,
                                                 downscale_name,
                                                 int(patchsize),
                                                 int(patchstride))

    index_df1['instrument'] = instrument1
    index_df2['instrument'] = instrument2

    index_df = pd.concat([index_df1, index_df2], axis=0)
    index_df.dropna(subset=['dateobs'], inplace=True)

    index_df['dateobs'] = pd.to_datetime(index_df['dateobs'])

    if year_start is not None:
        date_start = datetime.date(year_start, month_start, 1)

        if month_end < 12:
            date_end = datetime.date(year_end, month_end + 1, 1)
        else:
            date_end = datetime.date(year_end + 1, 1, 1)

        index_df = index_df.loc[(index_df.dateobs < date_end), :]
        index_df = index_df.loc[(index_df.dateobs >= date_start), :]

    # time alignment
    inst1_inst2_index = time_alignment(index_df, instrument1, instrument2, tolerance)

    # train/test/validate split
    train_test_val_dict = train_test_split(inst1_inst2_index)

    # files handles corresponding to aligned index

    with ProcessPoolExecutor(max_workers=nworkers) as executor:
        for type_df, df_time_list in train_test_val_dict.items():
            d_list = [df_time[1] for df_time in df_time_list]
            df = pd.concat(d_list)

            file_handles = get_files_from_index(df, instrument1, instrument2)
            futures = []

            for handle, instrument, date in file_handles:
                if (instrument == instrument1) & (downscale is not None):
                    futures.append(executor.submit(
                        run_export, handle, destination_bucket,
                        '{}/{}'.format(destination_folder, type_df),
                        instrument,
                        date,
                        patchscale=(patchsize / downscale, patchstride / downscale),
                        downscale=downscale,
                        compressed=compressed),
                    )
                else:
                    futures.append(executor.submit(
                        run_export, handle,
                        destination_bucket,
                        '{}/{}'.format(destination_folder, type_df),
                        instrument,
                        date,
                        patchscale=(patchsize, patchstride),
                        downscale=None,
                        compressed=compressed))

            wait(futures)

            results = []
            for f in futures:
                try:
                    results.append(f.result())
                except:
                    logger.info(handle.path)

            results_flat = itertools.chain.from_iterable(results)
            exported_files = pd.DataFrame(results_flat)

            exported_files_1 = exported_files[exported_files['instrument'] == instrument1]
            exported_files_2 = exported_files[exported_files['instrument'] == instrument2]

            exported_files = pd.merge(exported_files_1,
                                          exported_files_2,
                                          on=['dateobs'],
                                          suffixes=('_source', '_target'))

            # upload/update index
            upload_index(exported_files, '{}/{}/{}'.format(destination_bucket, destination_folder, type_df))

    return inst1_inst2_index


def run_export(handle,
               destination,
               folder,
               instrument,
               date,
               cropscale=None,
               downscale=None,
               patchscale=None,
               compressed=False):
    """
        Process a fits file calculating metrics and pre-processing

        Parameters
        ----------
        handle :
            handle to fits file in bucket

        destination : basestring
            bucket to write the data for ml

        folder: basestring
            name of folder to upload data

        instrument: string
            instrument processed

        cropscale: tuple
            width, height in pixels

        patchscale: tuple
            patchsize, patchstride in pixels

        downscale: int
            HR width in pixels/LR width in pixels

        Returns
        -------
            dictionary with magnetograms info (instrument, dateobs, dimensions)
        """

    with handle as file:

        try:
            amap = map_prep(file, instrument)
            date_instrument = amap.date.datetime
            logger.info(date)
            instrument_base = instrument.split('_')[-1]

            if downscale is not None:
                amap = downsample(amap, downscale)
        
            if cropscale is not None:
                amap = crop(amap, cropscale[0], cropscale[1], instrument_base)

            if patchscale is not None:
                tensor_to_export = get_patch(amap,
                                             int(patchscale[0]),
                                             stride=int(patchscale[1]),
                                             instrument=instrument_base)
                logger.info(tensor_to_export.shape)

                dim_x = tensor_to_export.shape[2]
                dim_y = tensor_to_export.shape[3]

            else:
                dim_x = amap.dimensions.x
                dim_y = amap.dimensions.y
                tensor_to_export = amap.data

            filename = upload_npy(tensor_to_export, instrument, date_instrument, destination, folder, compressed=compressed)

            export_info = [{'filename': filename, 'dateobs': date,
                       'instrument': instrument, 'dim_x': dim_x,
                           'dim_y': dim_y}]

        except Exception as e:
            logger.info(f'Error in file {handle.path} with error: {e}')
            export_info = [{}]

        return export_info

def export(instrument1,
           instrument2,
           tolerance,
           destination_bucket,
           patchsize=None,
           patchstride=None,
           cropscale=None,
           downscale=None,
           nworkers=4,
           year_start=None,
           year_end=None,
           month_start=None,
           month_end=None,
           compressed=False
           ):

    if patchsize is not None:
        data_export_patches(instrument1, instrument2, tolerance, destination_bucket, patchsize, patchstride,
                            downscale=downscale, nworkers=nworkers, year_start=year_start, year_end=year_end,
                            month_start=month_start, month_end=month_end, compressed=compressed)

    elif cropscale is not None:
        data_export(instrument1, instrument2, tolerance, destination_bucket, cropscale,
                            downscale=downscale, nworkers=nworkers)

    else:
        raise ValueError("You need to define either a cropscale or a patchscale method")


if __name__ == '__main__':

    # destination = 'test-xavier'
    # instrument1 = 'HMI'
    # instrument2 = 'HMI'
    # tolerance = 0
    # nworkers = 1

    # instrument1 = 'HMI'
    # instrument2 = 'HMI'
    # bucket = 'fdl-mag-2-ml'
    # tolerance = 3600
    # year_start = None
    # year_end = None
    # month_start = None
    # month_end = None
    # patchsize = 128
    # cropscale = None
    # patchstride = None
    # compressed = True
    # downscale = 2
    #
    # index = export(instrument1, instrument2, tolerance, bucket, downscale=2, cropscale=cropscale, patchsize=patchsize, nworkers=1, compressed=compressed)

    parser = argparse.ArgumentParser()
    parser.add_argument('--bucket')
    parser.add_argument('--instrument1')
    parser.add_argument('--instrument2')
    parser.add_argument('--cropscale',  nargs='+', type=float)
    parser.add_argument('--patchsize')
    parser.add_argument(('--patchstride'))
    parser.add_argument('--tolerance')
    parser.add_argument('--downscale')
    parser.add_argument('--nworkers')
    parser.add_argument('--year_start')
    parser.add_argument('--year_end')
    parser.add_argument('--month_start')
    parser.add_argument('--month_end')
    parser.add_argument('--compressed')

    args = parser.parse_args()

    print("You are running the script with arguments: ")
    for a in args.__dict__:
        print(str(a) + ": " + str(args.__dict__[a]))

    if args.downscale is not None:
        downscale = float(args.downscale)
    else:
        downscale = None

    if args.cropscale is not None:
        cropscale = tuple(args.cropscale)
    else:
        cropscale = None

    if args.patchsize is not None:
        patchsize = int(args.patchsize)

        if args.patchstride is not None:
            patchstride = int(args.patchstride)
        else:
            patchstride = None

    else:
        patchsize = None
        patchstride = None

    if args.year_start is not None:
        year_start = int(args.year_start)
        year_end = int(args.year_end)
        month_start = int(args.month_start)
        month_end = int(args.month_end)
    else:
        year_start = None
        year_end = None
        month_start = None
        month_end = None


    kwargs_dict = {'downscale': downscale,
                   'cropscale': cropscale,
                   'patchsize': patchsize,
                   'patchstride': patchstride,
                   'nworkers': int(args.nworkers),
                   'year_start': year_start,
                   'year_end': year_end,
                   'month_start': month_start,
                   'month_end': month_end,
                   'compressed': args.compressed
                   }

    export(args.instrument1,
                args.instrument2,
                float(args.tolerance),
                args.bucket,
                **kwargs_dict)

