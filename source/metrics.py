"""
Metrics
"""

import numpy as np

from scipy.stats import moment
from sunpy.map.maputils import all_coordinates_from_map


def disk_nans(amap):
    """
    Counts number of on-disk nans

    Parameters
    ----------
    amap : `sunpy.Map`
        input map
    Returns
    -------
    tuple : number and fraction of disk nans
    """
    hpc_coords = all_coordinates_from_map(amap)
    r = np.sqrt(hpc_coords.Tx ** 2 + hpc_coords.Ty ** 2) / amap.rsun_obs
    nans = np.count_nonzero(~np.isfinite(amap.data[r <= 1]))
    fraction = nans / np.count_nonzero(r <= 1)

    return {'nan_no': nans, 'nan_frac': fraction}


def radius(amap):
    """
    Determines observed radius of the instrument

    Parameters
    ----------
    amap : `sunpy.Map`
        input map
    Returns
    -------
    float : Observed sun radius in arcseconds
    """  
    
    rsun = amap.rsun_obs.value
    return {'rsun_obs': rsun}


def radius_scaled(amap):
    """
    Determines scaled radius of the instrument

    Parameters
    ----------
    amap : `sunpy.Map`
        input map
    Returns
    -------
    float : Scaled sun radius in arcseconds
    """  
    
    rsun = amap.rsun_obs.value
    return {'rsun_obs_scaled': rsun}
    
    
def minmax(amap):
    """
    Return min and max of array.

    Parameters
    ----------
    data : np.array
        data array

    Returns
    -------
    tuple : min, max

    """
    min = amap.data.min()
    max = amap.data.max()
    return {'min': min, 'max': max}


def percentiles(amap, levels=(0.1, 0.5, .99)):
    """
    Return data percentiles.

    Parameters
    ----------
    data : np.array
        data array

    levels : list
        percent to calculate percentiles at

    Returns
    -------
      list : percentiles

    """
    percentiles = np.percentile(amap.data, levels)
    return percentiles


def moments(amap, moments=[1, 2, 3, 4]):
    """
    Return moments of the data.

    Parameters
    ----------
    data : np.array
        data array
    moments : list
        order of moments (1=mean, 2=variance, 3=skewness, 4=kurtosis)

    Returns
    -------
    np.array : moments
    """
    res = moment(amap.data, moment=moments, axis=None)
    return {'mean': res[0], 'stdev': res[1], 'skewness': res[2], 'kurtosis': res[3]}


def signed_flux(amap):
    """
    Returns the unsigned flux

    Parameters
    ----------
    data : np.array
       data array

    Returns
    -------
    float : unsigned flux
    """
    return {'signed_flux': amap.data.sum()}

def unsigned_flux(amap):
    """
    Returns the unsigned flux

    Parameters
    ----------
    data : np.array
       data array

    Returns
    -------
    float : unsigned flux
    """
    
    return {'unsigned_flux': np.absolute(amap.data).sum()}
