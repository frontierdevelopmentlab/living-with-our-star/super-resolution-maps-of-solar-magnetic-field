import pandas as pd
import numpy as np
import torch


from source.pipeline.dataset import LocalMagDataset


def test_multiframe():
    index_filename = 'index.csv'
    rootdir = '/home/mx/Documents/Xavier/FDL'
    folder = 'data'
    nframe = 1

    dset = LocalMagDataset(index_filename, rootdir, folder, nframe=nframe)

    t0 = torch.from_numpy(np.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]])).float()
    t1 = torch.from_numpy(np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])).float()
    t2 = torch.from_numpy(np.array([[0, 0, 1], [0, 0, 1], [0, 0, 1]])).float()

    assert torch.eq(dset[0]['input'], t0).all()
    assert torch.eq(dset[1]['input'], t1).all()
    assert torch.eq(dset[2]['input'], t2).all()

    assert torch.eq(dset[0]['target'], t0).all()
    assert torch.eq(dset[1]['target'], t1).all()
    assert torch.eq(dset[2]['target'], t2).all()

    assert len(dset) == 3

    nframe = 3
    dset = LocalMagDataset(index_filename, rootdir, folder, nframe=nframe)

    a0 = np.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
    a1 = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
    a2 = np.array([[0, 0, 1], [0, 0, 1], [0, 0, 1]])

    t = torch.from_numpy(np.stack([a0, a1, a2], axis=0)).float()

    assert dset[0]['input'].shape == (3, 3, 3)
    assert dset[0]['target'].shape == (3, 3)
    assert torch.eq(t[0, :, :], t0).all()
    assert torch.eq(dset[0]['input'][1], dset[0]['target']).all()
    assert torch.eq(dset[0]['input'][0], t0).all()
    assert torch.eq(dset[0]['input'][2], t2).all()

    assert len(dset) == 1