import os

from google.cloud import storage

from source.utils import disable_warnings, get_logger


disable_warnings()
logger = get_logger(__name__)


def download_test_data():
    """
    Download data for testing.

    Returns
    -------

    """
    if os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'):
        logger.info('Assuming running on local machine')
    else:
        logger.info('Assuming running on GCE VM')

    client = storage.Client()
    bucket = client.get_bucket('fdl-mag-sdo')
    blob = storage.Blob('2010/04/07/hmi.M_720s.20100407_013600_TAI.1.magnetogram.fits',
                        bucket)

    if not os.path.exists('source/test/data'):
        os.mkdir('source/test/data')

    blob.download_to_filename('source/test/data/HMI_test.fits')

    bucket = client.get_bucket('fdl-mag-sdo-prepped')
    blob = storage.Blob('2010/04/07/hmi.M_720s.20100407_013600_TAI.1.magnetogram.fits',
                        bucket)

    if not os.path.exists('source/test/data'):
        os.mkdir('source/test/data')

    blob.download_to_filename('source/test/data/HMI_test.fits')


if __name__ == '__main__':
    download_test_data()




