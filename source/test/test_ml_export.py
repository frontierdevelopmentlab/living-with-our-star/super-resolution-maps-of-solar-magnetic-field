import pandas as pd

from astropy.io import fits
from sunpy.map import Map
from astropy import units as u

from source.ml_export import train_test_split, downsample


def test_train_test_split():
    test_dates = pd.date_range(start='2018-01-01', end='2018-12-31', freq='D')
    out = pd.DataFrame(test_dates, list(range(len(test_dates))))
    out.columns = ['dateobs']

    splits = train_test_split(out)

    assert len(splits['train']) == 10
    assert len(splits['test']) == 1
    assert len(splits['validate']) == 1

    test_dates = pd.date_range(start='2018-01-01', end='2020-12-31', freq='D')
    out = pd.DataFrame(test_dates, list(range(len(test_dates))))
    out.columns = ['dateobs']

    splits = train_test_split(out)

    assert len(splits['train']) == 30
    assert len(splits['test']) == 3
    assert len(splits['validate']) == 3

def test_downsample_headers():

    HMI_fits = fits.open('source/test/data/HMI_test.fits', cache=False)
    HMI_fits.verify('fix')
    dataHMI = HMI_fits[1].data

    # Specify angular size of the Sun for compatibility purposes with MDI


    # Assemble Sunpy map
    HMImap = Map(HMI_fits[1].data, HMI_fits[1].header)

    # downsample map using sunpy resample
    HMImap_sunpy = HMImap.resample(u.Quantity([1024, 1024], u.pixel))

    # downsample map using downsample
    HMImap_mlexport = downsample(HMImap, 4)

    assert HMImap_sunpy.meta['cdelt1'] == HMImap_mlexport.meta['cdelt1']
    assert HMImap_sunpy.meta['cdelt2'] == HMImap_mlexport.meta['cdelt2']
    assert HMImap_sunpy.meta['crpix1'] == HMImap_mlexport.meta['crpix1']
    assert HMImap_sunpy.meta['crval1'] == HMImap_mlexport.meta['crval1']
    assert HMImap_sunpy.meta['crval2'] == HMImap_mlexport.meta['crval2']

    if 'CD1_1' in HMImap_sunpy.meta:
        assert HMImap_sunpy.meta['CD2_2'] == HMImap_mlexport.meta['CD2_2']




    

