import json
import os
import pandas as pd
import numpy as np

from sunpy.cm import cm as cm

from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1.axes_grid import ImageGrid

from source.utils import create_hist_bins

def make_metric_table(results_directory, outfolder, tag=None):
    """
    Open all the json files in result_directory, construct a pandas
    table of metrics read from those files and save it in outfolder
    :param results_directory:
    :return:
    """
    results_df = pd.DataFrame()

    for filename in os.listdir(results_directory):
        if ~filename.endswith('.json'):
            pass
        with open(os.path.join(results_directory, filename)) as json_file:
            results = json.load(json_file)
            method = results['comment']
            for metric_name, metric_value in results['metrics'].items():
                results_df.loc[method, metric_name] = metric_value

    os.makedirs(outfolder, exist_ok=True)

    if tag is not None:
        results_df.to_csv(f'{outfolder}/metrics_{tag}.csv')
    else:
        results_df.to_csv(f'{outfolder}/metrics.csv')


def plot_error_map(target, mc_mean, mc_var, outfolder, tag=None):
    """
        Plot high resolution, mean and variance MC dropout samples images
        Parameters
        ----------
        target : 'numpy.array'
            High resolution target (BxWxH)
        mc_mean : 'numpy.array'
            Mean of MC dropout samples (BxHxW)
        mc_var : 'numpy.array'
            Variance of MC dropout samples (BxHxW)

        """
    fig = plt.figure(figsize=(12, 5))

    grid = ImageGrid(fig, 111,
                     nrows_ncols=(1, 4),
                     axes_pad=0.05,
                     share_all=True,
                     cbar_mode='edge',
                     cbar_location="bottom",
                     #cbar_mode="edge",
                     cbar_pad=0.05,
                     label_mode = "1",
                     direction="column"
                     )

    if tag is not None:
        fig.suptitle(f'Error map: {tag}', fontsize=20)

    img_plots = []

    img_plots.append(grid.axes_all[0].imshow(target[0, ...], cmap=cm.hmimag, vmin=-2000, vmax=2000))
    img_plots.append(grid.axes_all[1].imshow(mc_mean[0, ...], cmap=cm.hmimag, vmin=-2000, vmax=2000))
    img_plots.append(grid.axes_all[2].imshow(mc_var[0, ...], cmap='magma', vmin=0, vmax=200))
    img_plots.append(grid.axes_all[3].imshow(100 * mc_var[0, ...] / mc_mean[0, ...], cmap='magma', vmin=0, vmax=50))

    for ax in grid:
        ax.set_axis_off()

    grid[0].set_title('Target')
    grid[1].set_title('Mean of Posterior')
    grid[2].set_title('Standard Deviation of Posterior')
    grid[3].set_title('Percentage Uncertainty')

    # plt.colorbar(img_plots[0], cax=grid.cbar_axes[0])
    # plt.colorbar(img_plots[1], cax=grid.cbar_axes[1])
    # plt.colorbar(img_plots[2], cax=grid.cbar_axes[2])
    # plt.colorbar(img_plots[3], cax=grid.cbar_axes[3])

    grid.cbar_axes[0].colorbar(img_plots[0])
    grid.cbar_axes[1].colorbar(img_plots[1])
    grid.cbar_axes[2].colorbar(img_plots[2])
    grid.cbar_axes[3].colorbar(img_plots[3])

    for cax in grid.cbar_axes:
        cax.axes.tick_params(labelsize=5)

    plt.tight_layout()

    tag = tag.replace('/', '_')
    if tag is not None:
        fig.savefig(f'{outfolder}/error_map_{tag}.png')
    else:
        fig.savefig(f'{outfolder}/error_map.png')
    plt.close('all')

def plot_hist2d_mc_direct(counts2d, centers, outfolder, tag='mean'):
    """
    From a 2d counts plot a an image of the bivariate density distribution
    :param counts2d: numpy array (B,2)
    :param centers (B, 2)
    :param outfolder:
    :return:
    """

    fig = plt.figure(figsize=(12, 5))

    X, Y = centers[:, 0], centers[:, 1]
    plt.pcolormesh(X, Y, counts2d, cmap = plt.cm.get_cmap('gray_r'), norm=LogNorm())

    plt.plot(centers[:, 0], centers[:, 1], linestyle='-', color='#ffa232', linewidth=1)

    plt.ylabel("MC inference (Gauss)")
    plt.xlabel("One Pass Forward inference (Gauss)")

    if tag == 'variance':
        plt.xlim(0, 350)

    fig.savefig(f'{outfolder}/mc_direct_{tag}.png')
    plt.close('all')

def plot_hist2d_uncertainty_error(counts2d, centers, outfolder, tag='mean', esp=10**(-10)):
    """
    From a 2d counts plot a an image of the bivariate density distribution
    :param counts2d: numpy array (B,2)
    :param centers (B, 2)
    :param outfolder:
    :return:
    """

    fig = plt.figure(figsize=(12, 5))

    X, Y = centers[:, 0], centers[:, 1] # x corresponds to column coordinate
    plt.pcolormesh(X, Y, counts2d, cmap = plt.cm.get_cmap('gray_r'), norm=LogNorm())

    plt.plot(centers[:, 0], centers[:, 1], linestyle='-', color='#ffa232', linewidth=1)

    # add mean
    counts_total = counts2d.sum(axis=1)
    counts_total_exp = counts_total[:, None]
    counts2d_weight = counts2d * (centers[:-1, 0][None, :]) / (counts_total_exp + esp)
    expected_error = counts2d_weight.sum(axis=1)
    centers_plot = centers[:-1, 0][counts_total != 0]
    expected_error = expected_error[counts_total != 0]

    plt.plot(expected_error, centers_plot, linestyle='dashed', color='#27ae60', linewidth=1)

    plt.xlabel("Uncertainty (Gauss)")
    plt.ylabel("Error (Gauss)")

    #plt.xlim(0, 350)

    fig.savefig(f'{outfolder}/error_uncertainty_{tag}.png')
    plt.close('all')


def plot_diagnostic(diagnostic, outfolder, performance, x_variable, title=None, tag=None):
    """
    Plot diagnostic as 2d histogram with marginals varying by radius and magnetic field
    :param diagnostic:
    :param outfolder:
    :return:
    """

    # Size definitions
    dpi = 1000
    pxx = 2000
    pxy = pxx

    nph = 1
    npv = len(performance)

    # Padding
    padv = 0
    padv2 = 0
    padh = 0
    padh2 = 50

    # Figure sizes in pixels
    fszv = (npv * pxy + 2 * padv + (npv - 1) * padv2)
    fszh = (nph * pxx + 2 * padh + (nph - 1) * padh2)

    # Conversion to relative units
    ppxx = pxx / fszh
    ppxy = pxy / fszv
    ppadv = padv / fszv
    ppadv2 = padv2 / fszv
    ppadh = padh / fszh
    ppadh2 = padh2 / fszh

    # style
    contours = np.array([0.01, 0.1, 1]) / 3
    linestyles = ['-', '--', ':']
    clr1 = '#63acbe'
    clr2 = '#ee442f'

    clra = '#f7614d'
    clrb = '#b36a62'
    clrc = '#9eb0b8'
    clrd = '#97ddee'

    titleft = 10
    alphaR = 0.7
    clr1 = clrd
    clr2 = clra

    # xlabel
    lablx, namex, minx, maxx, nbinsx, rlim, xlimhp = x_variable
    varx = diagnostic[namex]
    binsx = np.arange(minx, maxx + (maxx - minx) / nbinsx, (maxx - minx) / nbinsx)
    centersx = (binsx[1:] + binsx[0:-1]) / 2

    fig = plt.figure(figsize=(fszh / dpi, fszv / dpi), dpi=dpi)

    ax3 = fig.add_axes([ppadh, ppadv + ppxy + (npv - 1) * ppxy, ppxx, ppxy / 2])
    hist = ax3.hist(varx, bins=binsx, density=True, color='k', alpha=0.5)

    y2 = np.max(hist[0]) * xlimhp
    ax3.plot([rlim, rlim], [0, y2], linestyle=':', color='k')
    ax3.fill([minx, minx, rlim, rlim], [0, y2, y2, 0], color=clr1, edgecolor=None, alpha=alphaR)
    ax3.fill([rlim, rlim, maxx, maxx], [0, y2, y2, 0], color=clr2, edgecolor=None, alpha=alphaR)

    ax3.xaxis.tick_top()
    ax3.xaxis.set_label_position("top")
    ax3.set_xlabel(lablx)
    ax3.set_xlim([minx, maxx])
    ax3.set_ylim([0, y2])
    ax3.set_yticks([])
    if title is not None:
        ax3.set_title(title, fontsize=titleft)

    for i, perf in enumerate(performance):
        lably, name, miny, maxy, nbinsy, invert_yaxis = perf
        if type(name) == tuple:
            if name[2] == 'Absolute':
                print(f'Plotting abs({name[0]} - {name[1]})')
                vary = np.abs(diagnostic[name[0]] - diagnostic[name[1]])
            elif 'Relative':
                print(f'Plotting {name[0]} - {name[1]})')
                vary = diagnostic[name[0]] - diagnostic[name[1]]
        else:
            vary = diagnostic[name]
        binsy = np.arange(miny, maxy + (maxy - miny) / nbinsx, (maxy - miny) / nbinsy)
        centersy = (binsy[1:] + binsy[0:-1]) / 2

        ax1 = fig.add_axes([ppadh, ppadv + (npv - i - 1) * ppxy, ppxx, ppxy])
        hist = ax1.hist2d(varx, vary, bins=(binsx, binsy), cmap=plt.cm.get_cmap('gray_r'))
        CS2 = ax1.contour(centersx, centersy, np.transpose(hist[0]), contours * np.max(hist[0]), colors='k',
                          linewidths=0.5, linestyles=linestyles)

        ax1.fill([minx, minx, rlim, rlim], [miny, maxy, maxy, miny], color=clr1, edgecolor=None, alpha=alphaR)
        ax1.fill([rlim, rlim, maxx, maxx], [miny, maxy, maxy, miny], color=clr2, edgecolor=None, alpha=alphaR)
        ax1.set_xlim([minx, maxx])
        ax1.set_ylim([miny, maxy])
        ax1.set_ylabel(lably)
        ax1.set_xticks([])
        ax1.plot([rlim, rlim], [miny, maxy], linestyle=':', color='k')

        ax2 = fig.add_axes([ppadh + ppxx, ppadv + (npv - i - 1) * ppxy, ppxx / 2, ppxy])
        hist = ax2.hist(vary, bins=binsy, orientation='horizontal', color='k', alpha=0.5)
        x2 = np.max(hist[0]) * xlimhp
        ax2.plot([0, x2], [np.nanmedian(vary), np.nanmedian(vary)], color='k')

        hist = ax2.hist(vary[varx < rlim], bins=binsy, orientation='horizontal', color=clr1, alpha=0.5)
        ax2.plot([0, x2], [np.nanmedian(vary[varx < rlim]), np.nanmedian(vary[varx < rlim])], color=clr1)
        hist = ax2.hist(vary[varx > rlim], bins=binsy, orientation='horizontal', color=clr2, alpha=0.5)
        ax2.plot([0, x2], [np.nanmedian(vary[varx > rlim]), np.nanmedian(vary[varx > rlim])], color=clr2)

        ax2.yaxis.tick_right()
        ax2.set_yticklabels([])
        ax2.set_xticks([])
        ax2.set_ylim([miny, maxy])
        ax2.set_xlim([0, x2])
        ax2.tick_params(axis="y", direction="in")

        if invert_yaxis:
            ax2.invert_yaxis()
            ax1.invert_yaxis()

    if tag is not None:
        fig.savefig(f'{outfolder}/{namex}_correlation_r_{tag}.png', bbox_inches='tight', dpi=dpi, pad_inches=0)
    else:
        fig.savefig(f'{outfolder}/{namex}_correlation_r.png', bbox_inches='tight', dpi=dpi, pad_inches=0)


if __name__ == '__main__':
    results_directory = '/home/mx/Documents/Xavier/FDL/data'

    diagnostic = pd.read_csv('../../../data/results_mag_hmi_hmi.csv')
    diagnostic['grad_diff'] = np.abs(diagnostic['Grad_Sobel_target'] - diagnostic['Grad_Sobel_output'])

    lablx = ' Average Radius Input'
    varx = diagnostic['average_radius_input']
    minx = 0
    maxx = 1
    nbinsx = 20
    namex = 'average_radius_input'
    x_variable = (lablx, namex, minx, maxx, nbinsx, 0.9, 1.1)
    performance = [('Pearson\'s Correlation', 'PearsonCorrelation', 0, 1, 32, False),
                   ('Pearson\'s Correlation\n(capped at 10 Gauss)', 'PearsonCorrelation_10', 0, 0.999, 32, False),
                   ('SSIM', 'SSIM', 0.001, 0.7, 32, False),
                   ('Difference in Average\nSigned Field', ('SignedMetric_target', 'SignedMetric_output', 'Relative'), -5, 5, 32, False),
                   ('Difference in Average\nGrad Sobel', 'grad_diff', 0, 2.99, 32, True)]

    outfolder = '../figures/proposal'
    os.makedirs(outfolder, exist_ok=True)
    title = 'HMI_HMI'
    plot_diagnostic(diagnostic, outfolder, performance, x_variable, title=title, tag='HMI_HMI')
