import os
import itertools
import argparse

import pandas as pd
import numpy as np

from concurrent.futures import ProcessPoolExecutor, wait


def remove_zeros_process(chunk, folder, nexpansion):

    chunk_expanded = expand_index(chunk, nexpansion)
    chunk_expanded.drop('index', axis=1, inplace=True)
    index_zeros = []
    chunk_expanded.set_index(np.arange(len(chunk_expanded)), inplace=True)

    for idx in chunk_expanded.index:
        filename_source = chunk_expanded.loc[idx, 'filename_source']
        filename_target = chunk_expanded.loc[idx, 'filename_target']

        if not os.path.exists('{}/{}'.format(folder, filename_source)):
            index_zeros.append(idx)

        elif not os.path.exists('{}/{}'.format(folder, filename_target)):
            index_zeros.append(idx)

        else:
            source = np.load('{}/{}'.format(folder, filename_source))[0, :, :]
            target = np.load('{}/{}'.format(folder, filename_target))

            if np.all(source == 0) & np.all(target == 0):
                index_zeros.append(idx)

    chunk_expanded.drop(index_zeros, inplace=True)
    return chunk_expanded


def remove_zeros_mp(folder, outname, nworkers, nexpansion=1024, chunksize=100):

    index_df = pd.read_csv('{}/index.csv'.format(folder), chunksize=chunksize)

    futures = []
    with ProcessPoolExecutor(max_workers=nworkers) as executor:
        for chunk in index_df:
            futures.append(executor.submit(remove_zeros_process,chunk, folder, nexpansion))

    wait(futures)
    results = [f.result() for f in futures]
    index_expanded = pd.DataFrame(results)
    index_expanded.set_index(np.arange(len(index_expanded)), inplace=True)
    index_expanded.index.name = 'index'
    index_expanded.to_csv('{}/{}'.format(folder, outname))

    return index_expanded


def expand_index(index_df, nexpansion):

    index_expanded = index_df.loc[index_df.index.repeat(nexpansion), :]

    index_expanded['patchnum'] = index_expanded.groupby(['filename_source', 'filename_target']).cumcount()
    index_expanded['filename_source'] = index_expanded['filename_source'].apply(lambda x: x.replace('.npz', ''))
    index_expanded['filename_source'] = index_expanded['filename_source'] + '_'
    index_expanded['filename_source'] += index_expanded['patchnum'].astype(str) + '.npy'

    index_expanded['filename_target'] = index_expanded['filename_target'].apply(lambda x: x.replace('.npz', ''))
    index_expanded['filename_target'] = index_expanded['filename_target'] + '_' + \
                                        index_expanded['patchnum'].astype(str) + '.npy'

    index_expanded['dateobs'] = pd.to_datetime(index_expanded['dateobs'])
    index_expanded['year'] = index_expanded.dateobs.apply(lambda x: x.year)

    return index_expanded


if __name__ == '__main__':


    parser = argparse.ArgumentParser()
    parser.add_argument('--folder')
    parser.add_argument('--outname')
    parser.add_argument('--nworkers')
    parser.add_argument('--nexpansion')
    parser.add_argument('--chunksize')

    args = parser.parse_args()

    print("You are running the script with arguments: ")
    for a in args.__dict__:
        print(str(a) + ": " + str(args.__dict__[a]))

    kwargs_dict = {'nexpansion': int(args.nexpansion),
                   'nworkers': int(args.nworkers),
                   'chunksize': int(args.chunksize),
                   }

    remove_zeros_mp(args.folder,
           args.outname,
           **kwargs_dict)
