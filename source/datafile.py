import os
import pandas as pd
import numpy as np
import dask.bytes
import argparse

from concurrent.futures import ProcessPoolExecutor, wait
from google.cloud import storage
import datetime

from source.prep import map_prep
from source.utils import disable_warnings, get_logger, file_exist, upload_index

disable_warnings()
logger = get_logger(__name__)

BUCKET_MAP = {'kp512': 'fdl-mag-512',
              'GONG': 'fdl-mag-gong',
              'MDI': 'fdl-mag-mdi',
              'MWO': 'fdl-mag-mt-wilson',
              'HMI': 'fdl-mag-sdo',
              'SPMG': 'fdl-mag-spmg',
              'SOLIS': 'fdl-mag-solis',
              'SOT': 'fdl-mag-sot'}


def get_files(instrument, datafolder):

    """
    Get the file system for instrument

    Parameters
    ----------
    instrument: string
        instrument

    datafolder: basestring
        folder with the data

    Returns
    -------
    a list of dask type files object

    """

    if 'gs://' in datafolder:
        dask_str = '{}/**/'

        # Append instrument specific strings
        if instrument == 'GONG':
            dask_str = dask_str + 'mlzqa'

        dask_str = dask_str + '*.fits*'
        list_of_files = dask_str.format(datafolder)

    else:
        list_of_files = []
        for (dirpath, dirnames, filenames) in os.walk(datafolder):
            if instrument == 'GONG':
                list_of_files += [os.path.join(dirpath, file) for file in filenames
                                  if file.endswith('.fits.gz') & file.startswith('mlzqa')]
            else:
                list_of_files += [os.path.join(dirpath, file) for file in filenames if file.endswith('.fits')]

    if instrument == 'GONG':
        return dask.bytes.open_files(list_of_files, compression='gzip')
    else:
        return dask.bytes.open_files(list_of_files)


def read_files(instrument, datafolder, destination, indexfile, nworkers=4):
    """
    Read all fits for instrument, get date if opens,
    otherwise record error message

    Parameters
    ----------
    instrument: string
        instrument
    desitnation_bucket: string
        bucket to put the dataframe registering all fits files

    Keywords
    --------
    nworkers: integer
        number of concurrent workers

    Return
    ------

    """

    file_handles = get_files(instrument, datafolder)

    futures = []

    with ProcessPoolExecutor(max_workers=nworkers) as executor:
        for handle in file_handles:
            futures.append(
                executor.submit(extract_date, handle, instrument)
                )

    wait(futures)

    file_information = [f.result() for f in futures]
    data = pd.DataFrame(file_information)

    upload_index(data, destination, indexfile)
    return data


def extract_date(handle, instrument):
    """
    Read a fits file and get date and file path

    Parameters
    ----------
    handle: dask handle
    instrument: string

    Returns
    -------
    dictionary with filepath, dateobs, did_not_open flag, instrument,
    error_message (if not opened)
    """

    with handle as file:
        try:
            sun_map = map_prep(file, instrument)
            date = sun_map.date.datetime
            filepath = handle.path

            file_information = {'filepath': filepath,
                                'dateobs': date,
                                'rsun_obs': sun_map.rsun_obs.value,
                                'did_not_open': 0,
                                'instrument': instrument,
                                'error_message': ''
                                }

        except Exception as e:  # In case the file is corrupt / doesn't open
            filepath = handle.path
            file_information = {'filepath': filepath,
                                'dateobs': None,
                                'rsun_obs': None,
                                'did_not_open': 1,
                                'instrument': instrument,
                                'error_message': e
                                }
    return file_information


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--destination')
    parser.add_argument('--datafolder')
    parser.add_argument('--instrument')
    parser.add_argument('--nworkers')
    parser.add_argument('--indexfile')
    args = parser.parse_args()

    print("You are running the script with arguments: ")
    for a in args.__dict__:
        print(str(a) + ": " + str(args.__dict__[a]))

    kwargs_dict = {'nworkers': int(args.nworkers)}

    data = read_files(args.instrument, args.datafolder, args.destination, args.indexfile, **kwargs_dict)
    print(data)
